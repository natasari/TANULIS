var noPlay;
var tempPenilaian;
var dataRangkaian;
var halaman;
$(document).ready(function(){
	var idSiswaChoosen = getUrlVars()["id"];
	halaman = getUrlVars()["halaman"];
	noPlay = localStorage.getItem("noPlay");
	if(jQuery.isEmptyObject(noPlay) == true){
		nomor = 0;
		localStorage.setItem("noPlay", nomor);
	}else{
		nomor = parseInt(JSON.parse(noPlay));
	}

	dataRangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/isActive == 'Aktif']");

	dataRangkaian = dataRangkaianSatuSiswa.values();
	jumlahRangkaian = dataRangkaian.length;


	if(jumlahRangkaian != 0){
		set(nomor);
	}
	else{
		if(halaman == 'utamaSiswa'){
			alert("Aktivitas kosong atau belum dikonfigurasi");
			window.location.href = "halaman-utama-siswa.html";
			return false;
		}else{
			alert("Aktivitas kosong atau belum dikonfigurasi");
			window.location.href = "halaman-kelola-aktivitas.html?id="+idSiswaChoosen;
			return false;
		}
	}
});	

function set(nom){
	if(nom < dataRangkaian.length){
		if(dataRangkaian[nom].idAktivitas == 1 || dataRangkaian[nom].idAktivitas == 2){
			window.location.href = "aktivitas/huruf.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));
		}
		else if(dataRangkaian[nom].idAktivitas == 3){
			window.location.href = "aktivitas/angka.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
		else if(dataRangkaian[nom].idAktivitas == 4 || dataRangkaian[nom].idAktivitas == 5){
			window.location.href = "aktivitas/kata.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
		else if(dataRangkaian[nom].idAktivitas == 6){
			window.location.href = "aktivitas/hari.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
		else if(dataRangkaian[nom].idAktivitas == 7){
			window.location.href = "aktivitas/bulan.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
		else if(dataRangkaian[nom].idAktivitas == 8){
			window.location.href = "aktivitas/tekateki.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
		else if(dataRangkaian[nom].idAktivitas == 9){
			window.location.href = "aktivitas/jam.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
		else if(dataRangkaian[nom].idAktivitas == 10){
			window.location.href = "aktivitas/kalimat.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
		else if(dataRangkaian[nom].idAktivitas == 11){
			window.location.href = "aktivitas/garis.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian+"&nom="+nom+"&jml="+dataRangkaian.length;
			localStorage.setItem("noPlay", (nom+1));	
		}
	}
	else{
		//var dinilai = confirm("Apakah ingin dinilai?");
		bootbox.dialog({
            message: "Apakah ingin dinilai sekarang?",
            title: "Penilaian",
            buttons: {
              success: {
                label: "YES!",
                className: "btn-success",
                callback: function() {
                  	window.location.href = "nilai-aktivitas.html?id="+dataRangkaian[nom-1].idSiswa;
					localStorage.removeItem("noPlay");
					return false;
                }
              },
              danger: {
                label: "NO!",
                className: "btn-danger",
                callback: function() {
                  window.location.href = "halaman-utama-siswa.html";
				  localStorage.removeItem("noPlay");
				  localStorage.removeItem("tempAktivitas");
				  return false;
                }
              }	
            }
          }); 
		// if(dinilai == true){
		// }
		// else{
		// }
		// window.location.href = "hasil-mulai-aktivitas.html?id="+dataRangkaian[nom-1].idSiswa;
	}
}

 var Example = (function() {
      "use strict";

      var elem,
          hideHandler,
          that = {};

      that.init = function(options) {
          elem = $(options.selector);
      };

      that.show = function(text) {
          clearTimeout(hideHandler);

          elem.find("span").html(text);
          elem.delay(200).fadeIn().delay(4000).fadeOut();
      };

      return that;
  }());