var idSiswaChoosen ;
$(document).ready(function(){
	idSiswaChoosen = getUrlVars()["id"];
    listSampleAktivitas(idSiswaChoosen);
    dbAktivitas = SpahQL.db(tbAktivitas);

    dataSiswa = getDataSiswa(idSiswaChoosen);
    showNama(dataSiswa);
});	

function showNama(data){
	$("#namaSiswa").html("Rangkaian Aktivitas : <strong>"+data.namaSiswa+"</strong>");
}

function listSampleAktivitas(siswa){
	url="aktivitas/";	
	$("#daftarSampleAktivitas").html("");
		$("#daftarSampleAktivitas").html(
			"<thead>"+
			"	<tr>"+
			"	<th>No</th>"+
			"	<th>Jenis</th>"+
			"	<th>Nama Aktivitas</th>"+
			"	<th>Keterangan</th>"+
			"	<th>Lihat Demo</th>"+
			"	<th>Tambah</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);
		for(var i in tbAktivitas.aktivitas){
			var cli = tbAktivitas.aktivitas[i];
		  	$("#daftarSampleAktivitas tbody").append("<tr>"+
									 	 "	<td>"+cli.idAktivitas+" </td>" +
										 "	<td>"+cli.jenisAktivitas+" </td>" + 
										 "	<td>"+cli.namaAktivitas+" </td>" + 
										 "	<td>"+cli.keterangan+" </td>" + 
										 "	<td class='center-block'><button type='submit' onclick='redirectDemo(\""+url+ "\",\""+cli.pathAktivitas+ "\",\""+99+cli.idAktivitas+ "\")' class='btn btn-warning center-block'><span class='glyphicon glyphicon-play-circle' aria-hidden='true'></span> Demo</button></td>" + 
										 "	<td><button type='button' onclick='tambahAktivitaskeRangkaianSiswa(\""+siswa+ "\",\""+cli.idAktivitas+ "\")' class='btn btn-success center-block'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span> Tambah</button></td>" +
		  								 "</tr>");
		}
}

function tambahAktivitaskeRangkaianSiswa(siswa,aktivitas){
	AddtoRangkaianAktivitas(siswa,aktivitas);
}

function panelHeading(){
	url = "halaman-kelola-aktivitas.html?id="
	redirectPage(url,idSiswaChoosen);		
}

function redirectDemo(url, pathAktivitas, rangkaian){
	if(pathAktivitas==null)pathAktivitas = "";
	window.location.href=url+pathAktivitas+"?id=999&idRangkaian="+rangkaian;
	return false;
}
