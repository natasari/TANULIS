$(document).ready(function(){
	var idSiswaChoosen = getUrlVars()["id"];
    dataSiswa = getDataSiswa(idSiswaChoosen);
    showDataSiswa(dataSiswa);
    showNama(dataSiswa);
    onLoad();
});

function showNama(data){
	$("#namaSiswa").html("Data : <strong>"+data.namaSiswa+"</strong>");
	$("#nama").html("<strong>"+data.namaSiswa+"</strong>");
}

function showDataSiswa(data){
	url = 'halaman-edit-siswa.html?id=';
	//console.log(data);
			$("#tabelDataSiswa").html("");
			$("#tabelDataSiswa").html(
				"<tbody>"+
				"</tbody>"
				);
			  	$("#tabelDataSiswa tbody").append("<tr>"+
			 	 "	<td rowspan='7' class='col-xs-4 col-sm-3 col-md-3'><a href='#' class='thumbnail'><img src='"+data.pathFoto+"' alt=''></a></td>" + 
			 	 "	<td class='col-xs-1 col-sm-1 col-md-1'><label>Nama</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-7 col-sm-8 col-md-8'>"+data.namaSiswa+"</td>" + 
				 "</tr>");

			  	$("#tabelDataSiswa tbody").append("<tr>"+
			 	 "	<td class='col-xs-1 col-sm-1 col-md-1'><label>Kelas</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-7 col-sm-8 col-md-8'>"+data.kelas+"</td>" + 
				 "</tr>");

			  	$("#tabelDataSiswa tbody").append("<tr>"+
			 	 "	<td class='col-xs-1 col-sm-1 col-md-1'><label>Usia</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-7 col-sm-8 col-md-8'>"+data.usia+"</td>" + 
				 "</tr>");

			  	$("#tabelDataSiswa tbody").append("<tr>"+
			 	 "	<td class='col-xs-1 col-sm-1 col-md-1'><label>Gender</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-7 col-sm-8 col-md-8'>"+data.gender+"</td>" + 
				 "</tr>");

			  	$("#tabelDataSiswa tbody").append("<tr>"+
			 	 "	<td class='col-xs-1 col-sm-1 col-md-1'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-7 col-sm-8 col-md-8'>"+data.level+"</td>" + 
				 "</tr>");

			  	$("#tabelDataSiswa tbody").append("<tr>"+
			 	 "	<td class='col-xs-1 col-sm-1 col-md-1'><label>Keterangan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-7 col-sm-8 col-md-8'>"+data.keterangan+"</td>" + 
				 "</tr>");

			  	$("#tabelDataSiswa tbody").append("<tr>"+
			 	 "	<td colspan='4'><button type='submit' onclick='redirectPage(\""+url+ "\",\""+data.idSiswa+ "\")' class='btn btn-warning pull-right' style='margin-left:6px;'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span> Ubah Data</button>"+
			 	 "  <button type='submit' onclick='deleteSiswa(\""+data.idSiswa+ "\")' class='btn btn-danger pull-right'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Data</button></td>" +
				 "</tr>");
			
			$("#namaSiswa").html("Detail Data : <strong>"+data.namaSiswa+"</strong>");
	}

function onLoad() {
       document.addEventListener("deviceready", onDeviceReady, false);
    }

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
}

function onBackKeyDown() {
	window.location.href='halaman-utama-siswa.html';
    return false;
}