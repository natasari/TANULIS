$(document).ready(function(){
  gambar = 0;
  if(gambar == 0){
    var img = document.getElementById('largeImage');
    img.src = 'img/profil.svg';
  }
});

$(document).on('submit', "#formSiswa", function () {
  if(gambar == 0){
    var img = document.getElementById('largeImage');
    img.src = 'img/profil.svg';
    image = img.src;
  }else{
    var img = document.getElementById('largeImage');
    image = img.src;
  }

  if(AddSiswa(image)){
    window.location.href = "halaman-utama-siswa.html";
    return false;
  }
  else{
    alert("Data Tidak Berhasil Tersimpan");
  }
});

var height = $(window).height();
var wid = $(".thumbnail").width();
$("#largeImage").height(wid);

var pictureSource;   // picture source
var destinationType; // sets the format of returned value

document.addEventListener("deviceready",onDeviceReady,false);
function onDeviceReady() {
    pictureSource=navigator.camera.PictureSourceType;
    destinationType=navigator.camera.DestinationType;
}

function onPhotoDataSuccess(imageData) {
  gambar = 1;
  var smallImage = document.getElementById('largeImage');
  smallImage.style.display = 'block';
  smallImage.src = "data:image/jpeg;base64," + imageData;
}

function onPhotoURISuccess(imageURI) {
  gambar = 1;
  var largeImage = document.getElementById('largeImage');
  largeImage.style.display = 'block';
  largeImage.src = imageURI;
}

function capturePhoto() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
    destinationType: destinationType.DATA_URL });
}

function capturePhotoEdit() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
    destinationType: destinationType.DATA_URL });
}

function getPhoto(source) {
  navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
    destinationType: destinationType.FILE_URI,
    sourceType: source });
}

function onFail(message) {
  alert('Failed because: ' + message);
}

