$(document).ready(function(){
    List();
    onLoad();
    var idUserEdit;
});	

function onLoad() {
       document.addEventListener("deviceready", onDeviceReady, false);
    }

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
}

function onBackKeyDown() {
	
}


$(document).on('click', "#register", function () {
	cekUsername = 0;
	cekEmail = 0;
	cekPassword = 0;
	cekRepassword = 0;

	if($("#username").val() == ""){
		cekUsername = 1;
		$("#error_username").html("Username wajib diisi!");
	}else{
		cekUsername = 0;
		$("#error_username").html("");
	}

	if($("#email").val() == ""){
		cekEmail = 1;
		$("#error_email").html("email wajib diisi!");
	}else{
		cekEmail = 0;
		$("#error_email").html("");
	}

	if($("#password").val() == ""){
		cekPassword = 1;
		$("#error_password").html("password wajib diisi!");
	}else{
		cekPassword = 0;
		$("#error_password").html("");
	}

	if($("#repassword").val() == ""){
		cekRepassword = 0;
		$("#error_repassword").html("repassword wajib diisi!");
	}else{
		cekRepassword = 1;
		$("#error_repassword").html("");
	}

	if($("#password").val() != $("#repassword").val()){
		$("#pesanError").html("Password dan Repassword tidak sama!");
		cekPassword = 1;
		cekRepassword = 1;
	}else{
		$("#pesanError").html("");
		cekPassword = 0;
		cekRepassword = 0;
	}
	
	inputUsername = $("#username").val();
	var adaUser = db.select("/users/*[/username =='"+  inputUsername + "']").value();


	if(jQuery.isEmptyObject(adaUser) == false){
		$("#pesanError").append("Username sudah ada, gunakan username lain");	
		cekUsername = 1;
	}else{
		cekUsername = 0;
	}
	
	console.log(cekUsername);
	console.log(cekEmail);
	console.log(cekPassword);
	console.log(cekRepassword);
	if(cekUsername == 0 && cekEmail == 0 && cekPassword == 0 && cekRepassword == 0){
		if(operation == "A"){
			if(Add() == true){
				alert("Anda berhasil terdaftar");
				window.location.href='index.html';
				return false;
			}
		}
		else{
			return Edit(idUserEdit);
		}
	}
});

$(document).on('click', ".btnEdit", function () {
	operation = "E";
	selected_index = parseInt($(this).attr("alt").replace("Edit", ""));
	var cli = tbUsers.users[selected_index];
	idUserEdit = cli.idUser;
	$("#username").val(cli.username);
	$("#profesi select").val(cli.profesi);
	$("#email").val(cli.email);
	$("#password").val(cli.password);
	$("#repassword").val(cli.password);
});

$(document).on('click', ".btnDelete", function () {
	alert("delete");
	selected_index = parseInt($(this).attr("alt").replace("Delete", ""));
	Delete();
	List();
});

function List(){				
		$("#tblList").html("");
		$("#tblList").html(
			"<thead>"+
			"	<tr>"+
			"	<th></th>"+
			"	<th>id_user</th>"+
			"	<th>username</th>"+
			"	<th>profesi</th>"+
			"	<th>email</th>"+
			"	<th>password</th>"+
			"	<th>is_Login</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);
		for(var i in tbUsers.users){
			var cli = tbUsers.users[i];
		  	$("#tblList tbody").append("<tr>"+
									 	 "	<td><img src='img/edit.png' alt='Edit"+i+"' class='btnEdit'/><img src='img/delete.png' alt='Delete"+i+"' class='btnDelete'/></td>" + 
									 	 "	<td>"+cli.idUser+" </td>" +
										 "	<td>"+cli.username+" </td>" + 
										 "	<td>"+cli.profesi+" </td>" + 
										 "	<td>"+cli.email+" </td>" + 
										 "	<td>"+cli.password+" </td>" + 
										 "	<td>"+cli.isLogin+" </td>" +
		  								 "</tr>");
		}
}
