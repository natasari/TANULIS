var w = window;
requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

$(document).ready(function(){
	onLoad();

	var height = window.innerHeight;
	var w = window.innerWidth;
	document.getElementById('content').style.height = (height * 0.7) + 'px';
    document.getElementById('content').style.marginTop = (height * 0.25)/2 + 'px';
    $("[id='isi']").css({"height":(height * 0.7) + 'px'});
    
    var cekuserMasuk = db.select("/users/*[/isLogin =='1']");
    
    if(jQuery.isEmptyObject(cekuserMasuk) == false){
    	window.location.href='halaman-utama-aktivitas.html';
		return false;
    }
});	

function onLoad() {
   document.addEventListener("deviceready", onDeviceReady, false);
}

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
}

function onBackKeyDown() { 
	bootbox.dialog({
            message: "Anda yakin ingin keluar?",
            title: "Keluar",
            buttons: {
              success: {
                label: "YES!",
                className: "btn-success",
                callback: function() {
                  	navigator.app.exitApp();
                }
              },
              danger: {
                label: "NO!",
                className: "btn-danger",
                callback: function() {
                  
                }
              }	
            }
          }); 
}

function checkLogin(){
	var iusername = $("#username").val();
	var ipassword = $("#password").val();
	
	var db = SpahQL.db(tbUsers);
	
	/*cek user*/
	var userMasuk = db.select("/users/*[/username =='"+  iusername + "']");

	if(!userMasuk.value()){
		alert("Username atau Password Anda Salah");
	}
	else{
		if(userMasuk.value().username == iusername && userMasuk.value().password == ipassword){
			var a = db.select("/users/*[/username =='"+  iusername + "']/isLogin").replace("1");
			
			localStorage.setItem("tbUsers", JSON.stringify(db.value()));
			window.location.href='halaman-utama-aktivitas.html';
			return false;
		}
		else{
			alert("Username atau Password Anda Salah");
			return false;
		}
	}
}

$(document).on('submit', "#formLogin", function () {
	return checkLogin();
});

function List(){				
		$("#tblList").html("");
		$("#tblList").html(
			"<thead>"+
			"	<tr>"+
			"	<th></th>"+
			"	<th>id_user</th>"+
			"	<th>username</th>"+
			"	<th>profesi</th>"+
			"	<th>email</th>"+
			"	<th>password</th>"+
			"	<th>is_Login</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);
		for(var i in tbUsers.users){
			//var restoredSession = JSON.parse(localStorage.getItem('session'));
			var cli = tbUsers.users[i];
		  	$("#tblList tbody").append("<tr>"+
									 	 "	<td><img src='img/edit.png' alt='Edit"+i+"' class='btnEdit'/><img src='img/delete.png' alt='Delete"+i+"' class='btnDelete'/></td>" + 
									 	 "	<td>"+cli.idUser+" </td>" +
										 "	<td>"+cli.username+" </td>" + 
										 "	<td>"+cli.profesi+" </td>" + 
										 "	<td>"+cli.email+" </td>" + 
										 "	<td>"+cli.password+" </td>" + 
										 "	<td>"+cli.isLogin+" </td>" +
		  								 "</tr>");
		}
	}

