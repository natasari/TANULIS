var letter = "";
var startLetter = "";
var endLetter = "";
var fontType = "";
var fontSize = null;
var level = "";
var startASCII = null;
var endASCII = null;
var arrayLetter = new Array();
var index = 0;
var idSiswaChoosen;
var idRangkaian;
var data;
var dataRangkaianSatuSiswa;
var hasilPresentasi;
var path, n;
var tandaDraw = 0;
var h;
var w;
var height, nom, jml;

$(document).ready(function(){
	h = $(window).height()-45;
	w = $(window).width();
	$(".footer").css({ top: $(window).height()-45});
	/*dapat aktivitas yang dikirimkan*/
	idSiswaChoosen = getUrlVars()["id"];
	
	idRangkaian = getUrlVars()["idRangkaian"];
	nom = parseInt(getUrlVars()["nom"])+1;
	jml = getUrlVars()["jml"];
	dataRangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/isActive == 'Aktif'][/idRangkaian =="+parseInt(idRangkaian)+"]");
	data = dataRangkaianSatuSiswa.value();
	path = dataRangkaianSatuSiswa.path();
	console.log(dataRangkaianSatuSiswa);
	$("#namaAktivitas").html("<b>Menulis "+data.namaAktivitas+"</b>");
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	ind = path.substr(slashPosition+1, path.length);
	
	idAktivitas = data.idAktivitas;
    console.log(tbRangkaianAktivitas);

	// fontType menentukan level nya
	fontType = 'Calibri';
	document.getElementById("load").style.fontFamily = fontType;

	/*looping kata*/
	kataSimpan = data.paramAktivitas.kata;
	kata 	   = data.paramAktivitas.kata;
	pathGambar = data.paramAktivitas.path;
	settingUp();
});

var ctx, ctxHuruf, color="rgb(0, 0, 5)";

function settingUp(){
	newCanvas(kata[index], pathGambar[index]);
	ctx.strokeStyle = '#000';
	ctx.globalCompositeOperation = 'source-over';
	$(".palette").click(function(){
		ctx.globalCompositeOperation = 'source-over';
		$(".palette").css("border-color", "#777");
		$(".palette").css("border-style", "solid");
		$(this).css("border-color", "#fff");
		$(this).css("border-style", "dashed");
		color = $(this).css("background-color");
		ctx.beginPath();
		ctx.strokeStyle = color;
	});
	$("#hps").click(function(){
		ctx.globalCompositeOperation = 'destination-out';
	});
	$("#reload").click(function(){
		newCanvas(arrayLetter[index]);
	});
	$("#urutanAktivitas").html("Kata : <b>"+kata[index]+"</b>/"+kata[(kata.length-1)]+"");
	$("#urutanPerAktivitas").html("&nbsp&nbsp&nbspAktivitas ke : <b>"+nom+"</b>/"+jml+"");
	if(idSiswaChoosen == "999"){
		$("#next").remove();	
		$("#urutanPerAktivitas").remove();	
	}
	else{
		$("#back").remove();	
	}
}

function newCanvas(letter, pathPic){
	height = h/5;
	$("#headera").height(height);
	
	$("#headera").css({
		'font-size': '10vw',
		'line-height': height + 'px',
		'font-family': 'Century Gothic'
	})
	if(jQuery.isEmptyObject(pathPic) == false){
		$("#headera").html("<img class='img-responsive' style='margin-right:4%;width:"+$("#headera").height()*0.65+"px;display:inline' src='"+pathPic.path+"' />");
	}else{
		$("#headera").html("<img class='img-responsive' style='margin-right:4%;width:"+$("#headera").height()*0.65+"px;display:inline' src='../img/buku.jpg' />");	
	}

	var canvas = document.getElementById("canvas");
	ctx=canvas.getContext("2d");
	canvas.height = h-height;
	canvas.width = w;
	$("#canvas").css({
		'margin-top': (height) + 'px'
	});


	ctx.strokeStyle = color;
	ctx.lineWidth = 10;
	ctx.lineCap = 'round';

	window.onload = function start() {
		canvasHuruf = document.getElementById("canvasHuruf");
		$("#canvasHuruf").css({
			'margin-top': (height) + 'px'
		});

		ctxHuruf = canvasHuruf.getContext('2d');
        canvasHuruf.height = h - height;
        canvasHuruf.width = w;
        tandaDraw = 1;
        text_handler();
	}
  	
  	function text_handler() {
  		fontSize = canvasHuruf.width;
        ctxHuruf.font="normal " + '20vw' + " " + fontType;//font size and then font family.
        ctxHuruf.fillStyle = 'rgb(0, 150, 136)'; //color to be seen on a black canvas, default is black text
        ctxHuruf.textBaseline = 'middle';
	        if(level != "Sulit"){
	    		drawletter(letter);    
	        }
    }
  	
  	if(tandaDraw == 1 && level != "Sulit"){
    	ctxHuruf.clearRect(0, 0, canvasHuruf.width, canvasHuruf.height);
	    drawletter(letter);
    }

	//setup for drawing
	$("#canvas").drawTouch();
	$("#canvas").drawPointer();
	$("#canvas").drawMouse();
}

function drawletter(letter) {
  var minus = ctxHuruf.measureText(letter).width / 2;
  var centerx = ($(window).width()/ 2)-minus;
  var centery = canvasHuruf.height/2;
  var everIndeks = 999;

  var kali = 0;
  if(data.paramAktivitas.level == "Mudah"){
  	while(kali < 2){
  		ran = Math.floor((Math.random() * letter.length-1) + 1);
	  	if(everIndeks != ind){
	  		letter = letter.replace(letter.charAt(ran), '_');
  			kali++;
  			everIndeks = ran;
	  	}
  	}

  }else if(data.paramAktivitas.level == "Sedang"){
  	for(var i = 1; i<letter.length; i++){
  		letter = letter.replace(letter.charAt(i), '_')
  	}
  }
  else{
  	for(var i = 0; i<letter.length; i++){
  		letter = letter.replace(letter.charAt(i), '_')
  	}
  }
  ctxHuruf.fillText(letter, centerx, centery);
};

$.fn.drawTouch = function(){
	var start = function(e){
		e = e.originalEvent;
		ctx.beginPath();
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY-height;
		ctx.moveTo(x,y);
	};
	var move= function(e){
		e.preventDefault();
		e = e.originalEvent;
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY-height;
		ctx.lineTo(x,y);
		ctx.stroke();
	};
	var end=function(e){
		//pixelthreshold();
	}
	$(this).on("touchstart",start);
	$(this).on("touchmove",move);
	$(this).on("touchend",end);
};

$.fn.drawMouse = function() {
	var clicked = 0;
	var start = function(e) {
		clicked = 1;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY-height;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		if(clicked){
			x = e.pageX;
			y = e.pageY-height;
			ctx.lineTo(x,y);
			ctx.stroke();
		}
	};
	var stop = function(e) {
		clicked = 0;
		//pixelthreshold();
	};
	$(this).on("mousedown", start);
	$(this).on("mousemove", move);
	$(this).on("mouseup", stop);
};

// prototype to	start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
$.fn.drawPointer = function() {
	var start = function(e) {
        e = e.originalEvent;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY-height;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		e.preventDefault();
        e = e.originalEvent;
		x = e.pageX;
		y = e.pageY-height;
		ctx.lineTo(x,y);
		ctx.stroke();
    };
	$(this).on("MSPointerDown", start);
	$(this).on("MSPointerMove", move);
};  

function simpanGambar(id){
	var pathHuruf = canvas.toDataURL();
	var pathTulis = canvasHuruf.toDataURL();
	localStorage.removeItem("tempAktivitas");
	
	index++;
	if(index == kata.length){
		var tempData = ({
			idSiswa : parseInt(idSiswaChoosen),
			idAktivitas  : parseInt(idAktivitas),
			idRangkaian    : idRangkaian,
			indeks : ind,
			letter : kata[index-1],
			pathHuruf : pathHuruf,
			pathTulis : pathTulis,
			level : data.paramAktivitas.level
		});
		tempAktivitas.temp.push(tempData);
		localStorage.setItem("tempAktivitas", JSON.stringify(tempAktivitas));
		window.location.href = "../halaman-mulai-aktivitas.html?id=" + idSiswaChoosen;
	}else{
		var tempData = ({
			idSiswa : parseInt(idSiswaChoosen),
			idAktivitas  : parseInt(idAktivitas),
			idRangkaian    : idRangkaian,
			indeks : ind,
			letter : kata[index-1],
			pathHuruf : pathHuruf,
			pathTulis : pathTulis,
			level : data.paramAktivitas.level
		});
		console.log(tempAktivitas);
		tempAktivitas.temp.push(tempData);
		localStorage.setItem("tempAktivitas", JSON.stringify(tempAktivitas));
		settingUp();
	}
}


$(document).on('click', "#next", function () {
	simpanGambar();
});