// localStorage.removeItem("tbRangkaianAktivitas");
var tbRangkaianAktivitas = localStorage.getItem("tbRangkaianAktivitas");

if(jQuery.isEmptyObject(tbRangkaianAktivitas) == true){
	var tbRangkaianAktivitas = {
	  'rangkaian': [],
	  'state' :  true
	};
}else{
	tbRangkaianAktivitas = JSON.parse(tbRangkaianAktivitas);
}
var dbRangkaianAktivitas = SpahQL.db(tbRangkaianAktivitas);


    var tempAktivitas = localStorage.getItem("tempAktivitas");
	if(jQuery.isEmptyObject(tempAktivitas) == true){
		var tempAktivitas = {
		  'temp': [],
		  'state' :  true
		};
	}else{
		tempAktivitas = JSON.parse(tempAktivitas);
	}
console.log(tbRangkaianAktivitas);

function AddtoRangkaianAktivitas(siswa, aktivitas){
	var dataSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(siswa)+"]");
	var dataAktivitas = dbAktivitas.select("/aktivitas/*[/idAktivitas =="+parseInt(aktivitas)+"]");
	var jumlahRangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"]");
	
	if(jQuery.isEmptyObject(jumlahRangkaian) == true){
		id_rangkaian = 1;
	}
	else{
		rangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"]").values();
	    idRangkaianTerakhir = rangkaianSatuSiswa[rangkaianSatuSiswa.length-1].idRangkaian;
		id_rangkaian = parseInt(idRangkaianTerakhir) + 1; 
	}
	// alert("id rangkaian "+ id_rangkaian);
	var aktivitaskeRangkaian = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : id_rangkaian,
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataAktivitas.value().namaAktivitas,
			paramAktivitas : dataAktivitas.value().paramAktivitas,
			paramResult : dataAktivitas.value().paramResult,
			jenisAktivitas : dataAktivitas.value().jenisAktivitas,
			isActive : "Belum dikonfigurasi",
			isPass : "Belum"
	}); 
	tbRangkaianAktivitas.rangkaian.push(aktivitaskeRangkaian);
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	alert("Aktivitas berhasil ditambahkan");
	//return false;
}

function editResultAktivitas(siswa, aktivitas, rangkaian, index, kemiripan, parameterNilai, level){
	// alert(siswa + " " + aktivitas + " " + rangkaian + " " + index + " " + kemiripan + " " + dataRangkaian + " " + parameterNilai + " " + level);
	var data = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"][/isActive == 'Aktif'][/idRangkaian =="+parseInt(rangkaian)+"]").value();

	var res = data.paramResult;	
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	if(dd<10) { dd='0'+dd } 
	if(mm<10) { mm='0'+mm } 
	today = mm+'/'+dd+'/'+yyyy;

	var Result = ({
		        parameterYangDinilai : parameterNilai,
				nilai : kemiripan,
				waktu : "0",
				level : level,
				tanggal : today
			});

	if(jQuery.isEmptyObject(res) == false){
		var nHasil = res.length;		
			if(aktivitas == '3'){
				dataNilai = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa == "+parseInt(siswa)+"][/idRangkaian == "+parseInt(rangkaian)+"][/paramResult/*[/parameterYangDinilai=="+parameterNilai+"]]/paramResult/*");
			}else{
				dataNilai = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa == "+parseInt(siswa)+"][/idRangkaian == "+parseInt(rangkaian)+"][/paramResult/*[/parameterYangDinilai=='"+parameterNilai+"']]/paramResult/*");
			}
			pathNilai = dataNilai.path();
			
			if(jQuery.isEmptyObject(pathNilai) == false){
				panjangPathNilai = pathNilai.length;
				indeksUbah = pathNilai[panjangPathNilai-1];
				if(res[indeksUbah].parameterYangDinilai == parameterNilai && res[indeksUbah].tanggal == today){
					// alert(res[indeksUbah].tanggal + " : " + today);
					res.splice(indeksUbah, 1);
				}
			}		
	}
	res.push(Result);
		
	var pas;
	if(parseInt(kemiripan) > 50){
		pass = "Sudah";
	}
	else{
		pass = "Belum";
	}
	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
		idSiswa : parseInt(siswa),
		idRangkaian    : parseInt(rangkaian),
		idAktivitas  : parseInt(aktivitas),
		namaAktivitas : data.namaAktivitas,
		paramAktivitas : data.paramAktivitas,
		paramResult : res,
		jenisAktivitas : data.jenisAktivitas,
		isActive : data.isActive,
		isPass : pass
	});
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
}

function editResultAktivitasGaris(siswa, aktivitas, rangkaian, index, kemiripan){
	var data = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"][/isActive == 'Aktif'][/idRangkaian =="+parseInt(rangkaian)+"]").value();
	res = data.paramResult;
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	if(dd<10) { dd='0'+dd } 
	if(mm<10) { mm='0'+mm } 
	today = mm+'/'+dd+'/'+yyyy;

	Result = ({
		        "parameterYangDinilai" : data.paramAktivitas.path,
				"nilai" : kemiripan,
				"waktu" : "0",
				"tanggal" : today
			});
	res.push(Result);
	
	var pas;
	if(parseInt(kemiripan) > 50){
		pass = "Sudah";
	}
	else{
		pass = "Belum";
	}
	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
		idSiswa : parseInt(siswa),
		idRangkaian    : parseInt(rangkaian),
		idAktivitas  : parseInt(aktivitas),
		namaAktivitas : data.namaAktivitas,
		paramAktivitas : data.paramAktivitas,
		paramResult : res,
		jenisAktivitas : data.jenisAktivitas,
		isActive : data.isActive,
		isPass : pass
	});
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
}
function deleteRangkaian(siswa, index){
	tbRangkaianAktivitas.rangkaian.splice(index, 1);
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	if(jQuery.isEmptyObject(tbRangkaianAktivitas.rangkaian) == true){
		localStorage.removeItem("tbRangkaianAktivitas");
		id_rangkaian = 1;
	}
	alert("Data berhasil dihapus");
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return true;
}

function deleteRangkaianFromSiswa(siswa, index){
	tbRangkaianAktivitas.rangkaian.splice(index, 1);
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	if(jQuery.isEmptyObject(tbRangkaianAktivitas.rangkaian) == true){
		localStorage.removeItem("tbRangkaianAktivitas");
		id_rangkaian = 1;
	}
	return true;
}

function editRangkaianAktivitasHurufBesar(siswa, aktivitas, rangkaian, index, dataRangkaian){
	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : parseInt(rangkaian), //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataRangkaian.namaAktivitas,
			paramAktivitas : ({
				ukuranTulisan : $("#ukuranTulisan").val(),
				mulaiHuruf : $("#mulaiHuruf").val(),
				akhirHuruf : $("#akhirHuruf").val(),
				jenisTulisan : $("#jenisTulisan").val(),
				level : $("#level").val()
			}),
			paramResult : dataRangkaian.paramResult,
			jenisAktivitas : dataRangkaian.jenisAktivitas,
			isActive : $("#isActive").val(),
			isPass : dataRangkaian.isPass
		});
		localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
		dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
		console.log(dataLengkap.value());
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return false;
}

function editRangkaianAktivitasSukuKata(siswa, aktivitas, rangkaian, index, dataRangkaian, path){
	var values = $("input[name='kata[]']").map(function(){return $(this).val();}).get();

	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : parseInt(rangkaian), //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataRangkaian.namaAktivitas,
			paramAktivitas : ({
				kata : values,
				jenisTulisan : $("#jenisTulisan").val(),
				level : $("#level").val(),
				path : path
			}),
			paramResult : dataRangkaian.paramResult,
			jenisAktivitas : dataRangkaian.jenisAktivitas,
			isActive : $("#isActive").val(),
			isPass : dataRangkaian.isPass
		});
	
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa; //"halaman-kelola-aktivitas.html?id="+siswa; 
	return true;	
}

function editRangkaianAktivitasHari(siswa, aktivitas, rangkaian, index, dataRangkaian){
	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : parseInt(rangkaian), //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataRangkaian.namaAktivitas,
			paramAktivitas : ({
				ukuranTulisan : $("#ukuranTulisan").val(),
				mulai : $("#mulaiHuruf").val(),
				akhir : $("#akhirHuruf").val(),
				pilihanStyle : $("#pilihanStyle").val(),
				level : $("#level").val()
			}),
			paramResult : dataRangkaian.paramResult,
			jenisAktivitas : dataRangkaian.jenisAktivitas,
			isActive : $("#isActive").val(),
			isPass : dataRangkaian.isPass
		});
		localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
		dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
		console.log(dataLengkap.value());
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return false;
}

function editRangkaianAktivitasBulan(siswa, aktivitas, rangkaian, index, dataRangkaian){
	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : parseInt(rangkaian), //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataRangkaian.namaAktivitas,
			paramAktivitas : ({
				ukuranTulisan : $("#ukuranTulisan").val(),
				mulai : $("#mulaiHuruf").val(),
				akhir : $("#akhirHuruf").val(),
				pilihanStyle : $("#pilihanStyle").val(),
				level : $("#level").val()
			}),
			paramResult : dataRangkaian.paramResult,
			jenisAktivitas : dataRangkaian.jenisAktivitas,
			isActive : $("#isActive").val(),
			isPass : dataRangkaian.isPass
		});
		localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
		dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
		console.log(dataLengkap.value());
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return false;
}

function editRangkaianAktivitasGaris(siswa, aktivitas, rangkaian, index, dataRangkaian, bentuk, image){
	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : parseInt(rangkaian), //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataRangkaian.namaAktivitas,
			paramAktivitas : ({
				kata : bentuk,
				path : image
			}),
			paramResult : dataRangkaian.paramResult,
			jenisAktivitas : dataRangkaian.jenisAktivitas,
			isActive : $("#isActive").val(),
			isPass : dataRangkaian.isPass
		});
		localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
		dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return false;
}