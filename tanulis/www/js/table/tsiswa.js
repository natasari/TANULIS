// localStorage.removeItem("tbSiswa");
var operation = "A";
var selected_index = -1; 
var tbSiswa = localStorage.getItem("tbSiswa");
var id_siswa = 0;
if(jQuery.isEmptyObject(tbSiswa) == true){
	var tbSiswa = {
	  'siswa': [],
	  'state' :  true
	};
	id_siswa = 1;
}else{
	tbSiswa = JSON.parse(tbSiswa);
	id_siswa= tbSiswa.siswa[(tbSiswa.siswa.length)-1].idSiswa + 1; //get idUser yang terakhir ditambah 1
}
dbSiswa = SpahQL.db(tbSiswa);
console.log(tbSiswa);
		
function AddSiswa(img){
		var userMasuk = db.select("/users/*[/isLogin =='1']/idUser").value();
		var siswaa = ({
			idSiswa : id_siswa,
			idUser : userMasuk,
			namaSiswa : $("#nama").val(),
			kelas  : $("#kelas").val(),
			usia : $("#usia").val(),
			gender : $("#gender").val(),
			level : $("#level").val(),
			keterangan : $("#keterangan").val(),
			pathFoto : img
		}); 
		tbSiswa.siswa.push(siswaa);
		localStorage.setItem("tbSiswa", JSON.stringify(tbSiswa));
		alert("Data berhasil disimpan");
		return true;
	}

function deleteSiswa(siswa){
	var index = getIndexSiswa(siswa);

	var r = confirm("Yakin data siswa dihapus?");
	if (r == true) {
		tbSiswa.siswa.splice(index, 1);
	 	localStorage.setItem("tbSiswa", JSON.stringify(tbSiswa));

		if(jQuery.isEmptyObject(tbSiswa.siswa) == true){
			localStorage.removeItem("tbSiswa");
			id_siswa= 1;
		}

		//hapus semua rangkaian aktivitas murid ini
		
		hapusRangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+  siswa + "]").values();

		for(k in hapusRangkaian){
				dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(hapusRangkaian[k].idRangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
				data = dataLengkap.value();

				var n = dataLengkap.path().length;
				var path = dataLengkap.path();
				for(var i=0; i<path.length;i++) {
				    if (path[i] === "/") slashPosition = i;
				}
				var ind = path.substr(slashPosition+1, path.length);
				// console.log(hapusRangkaian[k].idRangkaian);
				console.log(path);
				console.log(ind);
				deleteRangkaianFromSiswa(siswa, ind);
		}
		alert("Data siswa berhasil dihapus");
		window.location.href = "halaman-utama-siswa.html";
		return false;
	} 
}

function updateSiswa(siswa, index, img){
	var userMasuk = db.select("/users/*[/isLogin =='1']/idUser").value();
	tbSiswa.siswa[parseInt(index)] = ({
		idSiswa : parseInt($("#id").val()),
		idUser : userMasuk,
		namaSiswa : $("#nama").val(),
		kelas  : $("#kelas").val(),
		usia : $("#usia").val(),
		gender : $("#gender").val(),
		jenisTulisan : $("#font").val(),
		level : $("#level").val(),
		keterangan : $("#keterangan").val(), 
		pathFoto : img
	});
	localStorage.setItem("tbSiswa", JSON.stringify(tbSiswa));		
	return true;
}

function getDataSiswa(siswa){
	var dataSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(siswa)+"]");
	return dataSiswa.value();
}

function getIndexSiswa(siswa){
	var dataSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(siswa)+"]");
	var path = dataSiswa.path();
	var n = dataSiswa.path().length;
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);
	return ind;
}