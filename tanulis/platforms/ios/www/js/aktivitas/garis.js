var letter = "";
var startLetter = "";
var endLetter = "";
var fontType = "";
var fontSize = null;
var level = "";
var startASCII = null;
var endASCII = null;
var arrayLetter = new Array();
var index = 0;
var idSiswaChoosen;
var idRangkaian;
var data;
var dataRangkaianSatuSiswa;
var hasilPresentasi;
var path, n;
var tandaDraw = 0;
var h;
var w;
var height, nom, jml;

$(document).ready(function(){
	h = $(window).height()-45;
	w = $(window).width();
	$(".footer").css({ top: $(window).height()-45});
	idSiswaChoosen  = getUrlVars()["id"];
	idRangkaian 	= getUrlVars()["idRangkaian"];
	nom = parseInt(getUrlVars()["nom"])+1;
	jml = getUrlVars()["jml"];

	dataRangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/isActive == 'Aktif'][/idRangkaian =="+parseInt(idRangkaian)+"]");
	data = dataRangkaianSatuSiswa.value();
	path = dataRangkaianSatuSiswa.path();

	$("#namaAktivitas").html("<b>"+data.namaAktivitas+"</b>");

	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	ind = path.substr(slashPosition+1, path.length);

	idAktivitas = data.idAktivitas;
	settingUp(data.paramAktivitas.path);

});

var ctx, ctxHuruf, color="rgb(0, 0, 5)";

function settingUp(path){
	newCanvas(path);
	ctx.strokeStyle = '#000';
	ctx.globalCompositeOperation = 'source-over';
	$(".palette").click(function(){
		ctx.globalCompositeOperation = 'source-over';
		$(".palette").css("border-color", "#777");
		$(".palette").css("border-style", "solid");
		$(this).css("border-color", "#fff");
		$(this).css("border-style", "dashed");
		color = $(this).css("background-color");
		ctx.beginPath();
		ctx.strokeStyle = color;
	});
	$("#hps").click(function(){
		ctx.globalCompositeOperation = 'destination-out';
		$(".palette").css("border-color", "#777");
		$(".palette").css("border-style", "solid");
		$(this).css("border-color", "#fff");
		$(this).css("border-style", "dashed");
	});
	$("#reload").click(function(){
		location.reload();
	});
	$("#urutanAktivitas").html("Garis : <b>1</b>/1");
	$("#urutanPerAktivitas").html("Aktivitas ke : <b>"+nom+"</b>/"+jml+"");
	
	if(idSiswaChoosen == "999"){
		$("#next").remove();
		$("#urutanPerAktivitas").remove();
	}
	else{
		$("#back").remove();
	}
}

function newCanvas(path){
	height = h/5;
	var canvas = document.getElementById("canvas");
	ctx=canvas.getContext("2d");
	canvas.height = h;
	canvas.width = w;

	ctx.strokeStyle = color;
	ctx.lineWidth = 10;
	ctx.lineCap = 'round';

	window.onload = function start() {
            canvasHuruf = document.getElementById('canvasHuruf');
            ctxHuruf = canvasHuruf.getContext('2d');
            canvasHuruf.height = h;
            canvasHuruf.width = w;
            tandaDraw = 1;
            text_handler();
            var img = new Image();
			img.onload = function() {
			  ctxHuruf.drawImage(this, 0, 0, canvasHuruf.width, canvasHuruf.height);
			}
			img.src = path;
        }

        function text_handler() {        
        ctxHuruf.fillStyle = 'rgb(0, 150, 136)'; //color to be seen on a black canvas, default is black text
        }

    if(tandaDraw == 1 && level != "Sulit"){
    	ctxHuruf.clearRect(0, 0, canvasHuruf.width, canvasHuruf.height);
	    
    }
	//setup for drawing
	$("#canvas").drawTouch();
	$("#canvas").drawPointer();
	$("#canvas").drawMouse();
}


$.fn.drawTouch = function(){
	var start = function(e){
		e = e.originalEvent;
		ctx.beginPath();
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.moveTo(x,y);
	};
	var move= function(e){
		e.preventDefault();
		e = e.originalEvent;
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
	};
	var end=function(e){
		//pixelthreshold();
	}
	$(this).on("touchstart",start);
	$(this).on("touchmove",move);
	$(this).on("touchend",end);
};

$.fn.drawMouse = function() {
	var clicked = 0;
	var start = function(e) {
		clicked = 1;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		if(clicked){
			x = e.pageX;
			y = e.pageY;
			ctx.lineTo(x,y);
			ctx.stroke();
		}
	};
	var stop = function(e) {
		clicked = 0;
	};
	$(this).on("mousedown", start);
	$(this).on("mousemove", move);
	$(this).on("mouseup", stop);
};

// prototype to	start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
$.fn.drawPointer = function() {
	var start = function(e) {
        e = e.originalEvent;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		e.preventDefault();
        e = e.originalEvent;
		x = e.pageX;
		y = e.pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
    };
	$(this).on("MSPointerDown", start);
	$(this).on("MSPointerMove", move);
};  

function nilai(id){
	var score;
	var flagNan = 0;
	var flagCancel = 0;
	if(id == 1){
		score = 0;
	}
	else{
		while(flagNan == 0){
			score = prompt("Masukkan Nilai", "");
			if(isNaN(parseInt(score)) == true){
				if(score === null){
					flagCancel = 1;
					flagNan = 1;
				}else{
					alert('Nilai yang diinputkan bukan angka');
					flagNan = 0;
				}
			}else{
				flagNan = 1;
			}
		}	
	}
	if(flagCancel == 0){
		index++;
			editResultAktivitasGaris(idSiswaChoosen, idAktivitas, idRangkaian, ind, score, dataRangkaianSatuSiswa);
	  		window.location.href = "../halaman-mulai-aktivitas.html?id=" + idSiswaChoosen;
	}
}

function simpanGambar(id){
	var pathHuruf = canvas.toDataURL();
	var pathTulis = canvasHuruf.toDataURL();
	localStorage.removeItem("tempAktivitas");
	
	index++;
	
	var tempData = ({
		idSiswa : parseInt(idSiswaChoosen),
		idAktivitas  : parseInt(idAktivitas),
		idRangkaian    : idRangkaian,
		indeks : ind,
		letter : arrayLetter[index-1],
		pathHuruf : pathHuruf,
		pathTulis : pathTulis,
		level : data.paramAktivitas.level
	});
	tempAktivitas.temp.push(tempData);
	localStorage.setItem("tempAktivitas", JSON.stringify(tempAktivitas));
	window.location.href = "../halaman-mulai-aktivitas.html?id=" + idSiswaChoosen;
	
}

$(document).on('click', "#next", function () {
	simpanGambar();
});