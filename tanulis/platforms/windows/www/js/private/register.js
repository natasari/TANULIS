$(document).ready(function(){
    //List();
});	
	
$(document).on('submit', "#formRegister", function () {
	if(operation == "A"){
		if(Add() == true){
			window.location.href='index.html';
			return false;
		}
	}
	else{
		return Edit();
	}
});

$(document).on('click', ".btnEdit", function () {
	operation = "E";
	selected_index = parseInt($(this).attr("alt").replace("Edit", ""));
	
	var cli = tbUsers.users[selected_index];
	$("#username").val(cli.username);
	$("#profesi select").val(cli.profesi);
	$("#email").val(cli.email);
	$("#password").val(cli.password);
});

$(document).on('click', ".btnDelete", function () {
	alert("delete");
	selected_index = parseInt($(this).attr("alt").replace("Delete", ""));
	Delete();
	List();
});

function List(){				
		$("#tblList").html("");
		$("#tblList").html(
			"<thead>"+
			"	<tr>"+
			"	<th></th>"+
			"	<th>id_user</th>"+
			"	<th>username</th>"+
			"	<th>profesi</th>"+
			"	<th>email</th>"+
			"	<th>password</th>"+
			"	<th>is_Login</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);
		for(var i in tbUsers.users){
			//var restoredSession = JSON.parse(localStorage.getItem('session'));
			var cli = tbUsers.users[i];
		  	$("#tblList tbody").append("<tr>"+
									 	 "	<td><img src='img/edit.png' alt='Edit"+i+"' class='btnEdit'/><img src='img/delete.png' alt='Delete"+i+"' class='btnDelete'/></td>" + 
									 	 "	<td>"+cli.idUser+" </td>" +
										 "	<td>"+cli.username+" </td>" + 
										 "	<td>"+cli.profesi+" </td>" + 
										 "	<td>"+cli.email+" </td>" + 
										 "	<td>"+cli.password+" </td>" + 
										 "	<td>"+cli.isLogin+" </td>" +
		  								 "</tr>");
		}
}
