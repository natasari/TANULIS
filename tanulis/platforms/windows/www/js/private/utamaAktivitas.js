//localStorage.removeItem("tbRangkaianAktivitas");
$(document).ready(function(){
    listSiswaAktivitas();
});

function listSiswaAktivitas(){				
			$("#dataSiswaAktivitas").html("");
			$("#dataSiswaAktivitas").html(
				"<thead>"+
				"	<tr>"+
				"	<th >No</th>"+
				"	<th >Nama</th>"+
				"	<th class='text-center'>Kelola Aktivitas</th>"+
				"	</tr>"+
				"</thead>"+
				"<tfoot>"+
				"	<tr>"+
				"	<th >No</th>"+
				"	<th >Nama</th>"+
				"	<th class='text-center'>Kelola Aktivitas</th>"+
				"	</tr>"+
				"</tfoot>"+
				"<tbody>"+
				"</tbody>"
				);
			var numberSelected = 1;
			for(i=0; i<tbSiswa.siswa.length; i++){
				
				//var restoredSession = JSON.parse(localStorage.getItem('session'));
				var url = 'halaman-kelola-aktivitas.html?id=';
				var cli = tbSiswa.siswa[i];
			  	$("#dataSiswaAktivitas tbody").append("<tr>"+
										 	 "	<td>"+ numberSelected++ +" </td>" +
											 "	<td>"+cli.namaSiswa+" </td>" + 
											 "	<td class='center-block'> <a class='btn btn-primary center-block' onclick='redirectPage(\""+url+ "\",\""+cli.idSiswa+ "\")'><span class='glyphicon glyphicon-tasks' aria-hidden='true'></span> Kelola Rangkaian Aktivitas</a></td>" + 
			  								 "</tr>");
			  	
			}
}
