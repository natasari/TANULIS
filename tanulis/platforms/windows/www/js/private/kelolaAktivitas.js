$(document).ready(function(){
	localStorage.removeItem("noPlay");
    var idSiswaChoosen = getUrlVars()["id"];
	showTombol(idSiswaChoosen);
	showTombolPlay(idSiswaChoosen);

    dataSiswa = getDataSiswa(idSiswaChoosen);
    showNama(dataSiswa);

    showRangkaianSiswa(idSiswaChoosen);
});	

function showNama(data){
	$("#namaSiswa").html("Rangkaian Aktivitas : <strong>"+data.namaSiswa+"</strong>");
}

function showTombol(siswa){
	url = "tambah-aktivitas.html?id="
	$("#buttonTambahAktivitas").html("<button onclick='redirectPage(\""+url+ "\",\""+siswa+ "\")' type='button' class='btn btn-default' style='float:left;'>" +
                               "<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>Tambah Sample Aktivitas</a>");
}

function showTombolPlay(siswa){
	url = "halaman-mulai-aktivitas.html?id=";
	$("#buttonPlay").html("<button onclick='redirectPage(\""+url+ "\",\""+siswa+ "\")' type='button' class='btn btn-success' style='float:left;margin-left:5px;'>" +
                               "<span class='glyphicon glyphicon-triangle-right' aria-hidden='true'></span>Mulai Aktivitas</a>");
}

function showRangkaianSiswa(siswa){
	url = "form-konfigurasi.html"
	/*ambil data rangkaian yang cuma punya id=siswa*/
	var rangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"]").values();
	console.log(rangkaian);
	$("#tabelRangkaianAktivitas").html("");
		$("#tabelRangkaianAktivitas").html(
			"<thead>"+
			"	<tr>"+
			"	<th>No</th>"+
			"	<th>Jenis</th>"+
			"	<th>Nama Aktivitas</th>"+
			"	<th>Status</th>"+
			"	<th>Kelola</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);
		for(var i in rangkaian){
			cli = rangkaian[i];
			//var restoredSession = JSON.parse(localStorage.getItem('session'));
		  	$("#tabelRangkaianAktivitas tbody").append("<tr>"+
									 	 "	<td>"+ (i*1+1) +" </td>" +
										 "	<td>"+cli.jenisAktivitas+" </td>" + 
										 "	<td>"+cli.namaAktivitas+" </td>" + 
										 "	<td>"+cli.isActive+" </td>" + 
										 "	<td class='center-block'> <a class='btn btn-primary center-block' onclick='redirectPageKonfigurasi(\""+url+ "\",\""+cli.idSiswa+ "\",\""+cli.idRangkaian+ "\",\""+cli.idAktivitas+ "\")'><span class='glyphicon glyphicon-cog' aria-hidden='true'></span> Konfigurasi</a></td>" + 
		  								 "</tr>");
		}
}

function redirectPageKonfigurasi(url, siswa, rangkaian, aktivitas){
	window.location.href=url + "?id=" + siswa + "&rangkaian=" + rangkaian + "&aktivitas=" + aktivitas;
	return false;
}