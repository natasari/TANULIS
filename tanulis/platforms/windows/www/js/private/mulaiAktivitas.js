//localStorage.removeItem("noPlay");
var noPlay;
var dataRangkaian;

$(document).ready(function(){
	//console.log(dbRangkaianAktivitas);
	/*dapet id siswa*/
	var idSiswaChoosen = getUrlVars()["id"];
	/*ambil rangkaian dengan id siswa ini aja dan yang aktif*/
	
	noPlay = localStorage.getItem("noPlay");

	if(jQuery.isEmptyObject(noPlay) == true){
		nomor = 0;
		localStorage.setItem("noPlay", nomor);
	}else{
		nomor = parseInt(JSON.parse(noPlay));
	}

	dataRangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/isActive == 'Aktif']");
	console.log(dataRangkaianSatuSiswa.values());

	dataRangkaian = dataRangkaianSatuSiswa.values();
	jumlahRangkaian = dataRangkaian.length;
	if(jumlahRangkaian != 0){
		set(nomor);
	}
	else{
		alert("Aktivitas Kosong atau Belum dikonfigurasu");
		window.location.href = "halaman-kelola-aktivitas.html?id="+idSiswaChoosen;
		return false;
	}

});	

function set(nom){
	if(nom < dataRangkaian.length){
		if(dataRangkaian[nom].idAktivitas == 1 || dataRangkaian[nom].idAktivitas == 2 || dataRangkaian[nom].idAktivitas == 3){
			if(dataRangkaian[nom].paramAktivitas.level == 'Mudah'){
				window.location.href = "aktivitas/huruf-kecil.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian;
			}
			else if(dataRangkaian[nom].paramAktivitas.level == 'Sedang'){
				window.location.href = "aktivitas/shuruf-kecil.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian;
			}
			else{
				window.location.href = "aktivitas/lhuruf-kecil.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian;
			}
			localStorage.setItem("noPlay", (nom+1));
		}
		else if(dataRangkaian[nom].idAktivitas == 4 || dataRangkaian[nom].idAktivitas == 5){
			if(dataRangkaian[nom].paramAktivitas.level == 'Mudah'){
				window.location.href = "aktivitas/suku-kata.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian;
			}
			else if(dataRangkaian[nom].paramAktivitas.level == 'Sedang'){
				window.location.href = "aktivitas/ssuku-kata.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian;	
			}
			else{
				window.location.href = "aktivitas/suku-kata.html?id="+dataRangkaian[nom].idSiswa+"&idRangkaian="+dataRangkaian[nom].idRangkaian;	
			}
			localStorage.setItem("noPlay", (nom+1));	
		}
	}
	else{
		window.location.href = "hasil-mulai-aktivitas.html?id="+dataRangkaian[nom-1].idSiswa;
		localStorage.removeItem("noPlay");
		return false;
	}
}
	