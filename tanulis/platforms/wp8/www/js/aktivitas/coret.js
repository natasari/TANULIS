var ctx, ctxHuruf, color="rgb(0, 0, 5)";
var pixels = null;
var letterpixels = null;
var tandaDraw = 0;
var fontSize = '235px';
var fontType = "calibri";
var level = 'Mudah';
$(document).ready(function(){
	$(".footer").css({ top: $(window).height()-45});
	settingUp();
});

function settingUp(){
	newCanvas('a');
	$(".palette").click(function(){
		ctx.globalCompositeOperation = 'source-over';
		$(".palette").css("border-color", "#777");
		$(".palette").css("border-style", "solid");
		$(this).css("border-color", "#fff");
		$(this).css("border-style", "dashed");
		color = $(this).css("background-color");
		ctx.beginPath();
		ctx.strokeStyle = color;
	});
	$("#hps").click(function(){
		ctx.globalCompositeOperation = 'destination-out';
	});
}

function newCanvas(letter){
	//setup canvas
	var height = $(window).height()/5;
	$("#headera").height(height);
	$("#headera").html(letter);
	$("#headera").css({
		'font-size': (height) + 'px',
		'line-height': height + 'px',
		'font-family': 'Century Gothic'
	})

	var canvas = document.getElementById("canvas");
	ctx=canvas.getContext("2d");
	canvas.height = $(window).height()-45;
	canvas.width = $(window).width();

	ctx.strokeStyle = color;
	ctx.lineWidth = 10;
	ctx.lineCap = 'round';

	window.onload = function start() {
            canvasHuruf = document.getElementById('canvasHuruf');
            $("#canvasHuruf").css({
				'margin-top': (height) + 'px'
			});
            ctxHuruf = canvasHuruf.getContext('2d');
            canvasHuruf.width = $(window).width();
            canvasHuruf.height = $(window).height()-45- height;
            C_Width = canvasHuruf.width;
            C_Height = canvasHuruf.height;
            tandaDraw = 1;
            text_handler();
        }

        function text_handler() {
        ctxHuruf.font="normal " + fontSize + " " + fontType;//font size and then font family.
        ctxHuruf.fillStyle = 'rgb(0, 150, 136)'; //color to be seen on a black canvas, default is black text
	        if(level != "Sulit"){
	    		drawletter(letter);    
	        }
        }
    if(tandaDraw == 1 && level != "Sulit"){
    	ctxHuruf.clearRect(0, 0, canvasHuruf.width, canvasHuruf.height);
	    drawletter(letter);
    }
	//setup for drawing
	$("#canvas").drawTouch();
	$("#canvas").drawPointer();
	$("#canvas").drawMouse();
}

function drawletter(letter) {
  var centerx = ($(window).width()/2) - (ctx.measureText(letter).width*7);
  var centery = canvasHuruf.height/2 +  $("#headera").height();
  ctxHuruf.fillText(letter, centerx, centery);
};

$.fn.drawTouch = function(){
	var start = function(e){
		e = e.originalEvent;
		ctx.beginPath();
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.moveTo(x,y);
	};
	var move= function(e){
		e.preventDefault();
		e = e.originalEvent;
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
	};
	var end=function(e){
		//pixelthreshold();
	}
	$(this).on("touchstart",start);
	$(this).on("touchmove",move);
	$(this).on("touchend",end);
};

$.fn.drawMouse = function() {
	var clicked = 0;
	var start = function(e) {
		clicked = 1;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		if(clicked){
			x = e.pageX;
			y = e.pageY;
			ctx.lineTo(x,y);
			ctx.stroke();
		}
	};
	var stop = function(e) {
		clicked = 0;
	};
	$(this).on("mousedown", start);
	$(this).on("mousemove", move);
	$(this).on("mouseup", stop);
};

// prototype to	start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
$.fn.drawPointer = function() {
	var start = function(e) {
        e = e.originalEvent;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		e.preventDefault();
        e = e.originalEvent;
		x = e.pageX;
		y = e.pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
    };
	$(this).on("MSPointerDown", start);
	$(this).on("MSPointerMove", move);
};  


function fillCircle(x, y, radius, fillColor) {
    ctx.fillStyle = fillColor;
    ctx.beginPath();
    ctx.moveTo(x, y);
    ctx.arc(x, y, radius, 0, Math.PI * 2, false);
    ctx.fill();
};

