$(document).ready(function(){
    listSiswaEvaluasi();
	$('.nav-toggle').click(function(){
		var collapse_content_selector = $(this).attr('href');
		var toggle_switch = $(this);
		$(collapse_content_selector).toggle(function(){
			if($(this).css('display')=='none'){
				toggle_switch.html('<b>Bahasa</b> : Show');//change the button label to be 'Show'
			}else{
				toggle_switch.html('<b>Bahasa</b> : Hide');//change the button label to be 'Hide'
			}
		});
	});
	$('.nav-toggle2').click(function(){
		var collapse_content_selector = $(this).attr('href');
		var toggle_switch = $(this);
		$(collapse_content_selector).toggle(function(){
			if($(this).css('display')=='none'){
				toggle_switch.html('<b>Matematika</b> : Show');//change the button label to be 'Show'
			}else{
				toggle_switch.html('<b>Matematika</b> : Hide');//change the button label to be 'Hide'
			}
		});
	});
	onLoad();
});


function listSiswaEvaluasi(){
			//mencari semua rangkaian yang ada di rangkaian aktivitas
			$("#dataSiswaEvaluasi").html("");
			$("#dataSiswaEvaluasi").html(
				"<thead>"+
				"	<tr>"+
				"	<th >No</th>"+
				"	<th >Nama</th>"+
				"	<th class='text-center'>Aktivitas</th>"+
				"	<th class='text-center'>Grafik</th>"+
				"	</tr>"+
				"</thead>"+
				"<tfoot>"+
				"	<tr>"+
				"	<th >No</th>"+
				"	<th >Nama</th>"+
				"	<th class='text-center'>Aktivitas</th>"+
				"   <th class='text-center'>Grafik</th>"+
				"	</tr>"+
				"</tfoot>"+
				"<tbody>"+
				"</tbody>"
				);
			var numberSelected = 1;
			var url = 'evaluasi-per-tanggal.html?id=';
			var urlAktivitas = 'evaluasi-per-aktivitas.html?id=';
			iduserMasuk = userMasuk();
			arraySiswa = dbSiswa.select("/siswa/*[/idUser =="+parseInt(iduserMasuk)+"]").values();
			for(i=0; i<arraySiswa.length; i++){
				var cli = arraySiswa[i];
			  	$("#dataSiswaEvaluasi tbody").append("<tr>"+
				 	 "	<td>"+ numberSelected++ +" </td>" +
					 "	<td>"+ cli.namaSiswa+" </td>" + 
					 "	<td><button style='width:100%;' href='#bahasa"+i+"' class='nav-toggle btn'><b>Bahasa</b> : Show</button>"+
					 "  <div id='bahasa"+i+"' style='display:none'><p id='listBahasa"+i+"' style='margin-top:4%;'></p></div>"+

					 "  <button style='width:100%;margin-top:2%;' href='#matematika"+i+"' class='nav-toggle2 btn'><b>Matematika</b> : Show</button>"+
					 "  <div id='matematika"+i+"' style='display:none'><p id='listMatematika"+i+"' style='margin-top:4%;'></p></div></td>"+
					 "	<td class='center-block'> <a class='btn btn-primary center-block' onclick='redirectPage(\""+url+ "\",\""+cli.idSiswa+ "\",\"&halaman=utamaEvaluasi\")'><span class='glyphicon glyphicon-tasks' aria-hidden='true'></span> Evaluasi per Tanggal</a> " + 
					 "  <a class='btn btn-primary center-block' style='margin-top:4%;' onclick='redirectPage(\""+urlAktivitas+ "\",\""+cli.idSiswa+ "\",\"&halaman=utamaEvaluasi\")'><span class='glyphicon glyphicon-tasks' aria-hidden='true'></span> Evaluasi per Aktivitas</a></td>" + 
						 "</tr>");
				dataRangkaianSatuSiswa(tbSiswa.siswa[i].idSiswa, i);
			}
}

function dataRangkaianSatuSiswa(siswa, i){
	var rangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"]").values();
	var bahasa = [];
	var mat = [];

	for(j=0; j<rangkaian.length; j++){
		if(rangkaian[j].jenisAktivitas == 'Bahasa'){
			var a = jQuery.inArray( rangkaian[j].namaAktivitas+"<br>", bahasa);
			if( a < 0){
				bahasa.push(rangkaian[j].namaAktivitas+"<br>");
			}
		}
		else{
			mat.push(rangkaian[j].namaAktivitas+"<br>");
		}
	}
	$("#listBahasa"+i).append(bahasa);
	$("#listMatematika"+i).append(mat);
}

function onLoad() {
       document.addEventListener("deviceready", onDeviceReady, false);
    }

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
}

function onBackKeyDown() {
	
}