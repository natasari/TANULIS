$(document).ready(function(){
	idSiswaChoosen = getUrlVars()["id"];
    dataSiswa = getDataSiswa(idSiswaChoosen);
    idAktivitas = getUrlVars()["aktivitas"];
    aktivitas = dbAktivitas.select("/aktivitas/*[/idAktivitas =="+parseInt(idAktivitas)+"]").values();
    namaAktivitas = aktivitas[0].namaAktivitas;
    showNama(dataSiswa, idSiswaChoosen, namaAktivitas);
});

function showNama(data, siswa, namaAktivitas){
	$("#namaSiswa").html("<a href='evaluasi-per-aktivitas.html?id="+siswa+"' >Evaluasi Aktivitas : <strong>"+data.namaSiswa+"</strong></a>");
	$("#namaAktivitas").html("Aktivitas " + namaAktivitas);
}

$(function () {
	var rangkaianSatuJenisAktivitas = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/namaAktivitas == '"+namaAktivitas+"']/paramResult").values();
	//ambiltanggal
	tanggalSemua = [];
	nilai=[];
	for(i in rangkaianSatuJenisAktivitas){
		temp = rangkaianSatuJenisAktivitas[i];
		for(j in temp){
			if(jQuery.inArray( temp[j].tanggal, tanggalSemua ) < 0){
					tanggalSemua.push(temp[j].tanggal);
			}
			nilai.push({
				"tanggal":temp[j].tanggal,
				"nilai": temp[j].nilai
				});
		}
	}

    console.log(tanggalSemua.sort());
	nilaiTanggal = [];
	nilaiAja = [];
	for(k in tanggalSemua){
		var count = 0;
		var sum = 0;
		for(l in nilai){
			if(tanggalSemua[k] == nilai[l].tanggal){
				sum = sum + parseInt(nilai[l].nilai);
				count = count + 1;
			}
		}
		nilaiAja.push(sum/count);
		nilaiTanggal.push({
			"name" : tanggalSemua[k],
			"data" : (sum/count)
		});
	}
		haha = [{
            name: 'Tokyo',
            data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        }]
        console.log(haha);
        console.log(nilaiTanggal);

    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: namaAktivitas
        },
        subtitle: {
            text: 'Perkembangan Kemampuan Siswa'
        },
        xAxis: {
            categories: tanggalSemua
        },
        yAxis: {
            title: {
                text: 'Nilai'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: namaAktivitas,
            data: nilaiAja
        }]
    });
});