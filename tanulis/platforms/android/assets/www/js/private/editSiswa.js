var n = 0;
var path = null;

$(document).ready(function(){
	var idSiswaChoosen = getUrlVars()["id"];
	data = getDataSiswa(idSiswaChoosen);
  	showNama(data);
  	$("#id").val(data.idSiswa);
	$("#nama").val(data.namaSiswa);
	$("#kelas").val(data.kelas);
	$("#usia").val(data.usia);
	$("#gender").val(data.gender);
	$("#level").val(data.level);
	$("#keterangan").val(data.keterangan);
	
	var image = document.getElementById('foto');
	image.style.display = 'block';
    image.src = data.pathFoto;

	var dataSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(idSiswaChoosen)+"]");
	var n = dataSiswa.path().length;
	var path = dataSiswa.path();

	for(var i=0; i<path.length;i++) {
		 if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);

	$(document).on('click', "#updateSiswa", function () {	
		var img = document.getElementById('foto');
		updateSiswa(idSiswaChoosen, ind, img.src);
		window.location.href = "halaman-data-siswa.html?id="+idSiswaChoosen;
		return false;
	});
});

function showNama(data){
	$("#namaSiswa").html("<a href='halaman-data-siswa.html?id="+data.idSiswa+"' > Data : <strong>"+data.namaSiswa+"</strong></a>");
	$("#editSiswa").html(" Edit data : <strong>"+data.namaSiswa+"</strong></a>");
}

var height = $(window).height();
var wid = $(".thumbnail").width();
$("#largeImage").height(wid);

var pictureSource;   // picture source
var destinationType; // sets the format of returned value

document.addEventListener("deviceready",onDeviceReady,false);
function onDeviceReady() {
    pictureSource=navigator.camera.PictureSourceType;
    destinationType=navigator.camera.DestinationType;
}

function onPhotoDataSuccess(imageData) {
  var smallImage = document.getElementById('foto');
  smallImage.style.display = 'block';
  smallImage.src = "data:image/jpeg;base64," + imageData;
}

function onPhotoURISuccess(imageURI) {
  var largeImage = document.getElementById('foto');
  largeImage.style.display = 'block';
  largeImage.src = imageURI;
}

function capturePhoto() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
    destinationType: destinationType.DATA_URL });
}

function capturePhotoEdit() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
    destinationType: destinationType.DATA_URL });
}

function getPhoto(source) {
  navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
    destinationType: destinationType.FILE_URI,
    sourceType: source });
}

function onFail(message) {
  alert('Failed because: ' + message);
}