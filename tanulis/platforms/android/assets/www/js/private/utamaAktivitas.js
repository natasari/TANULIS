$(document).ready(function(){
    listSiswaAktivitas();
    onLoad();
});

function listSiswaAktivitas(){				
			$("#dataSiswaAktivitas").html("");
			$("#dataSiswaAktivitas").html(
				"<thead>"+
				"	<tr>"+
				"	<th >No</th>"+
				"	<th >Nama</th>"+
				"	<th class='text-center'>Kelola Aktivitas</th>"+
				"	</tr>"+
				"</thead>"+
				"<tfoot>"+
				"	<tr>"+
				"	<th >No</th>"+
				"	<th >Nama</th>"+
				"	<th class='text-center'>Kelola Aktivitas</th>"+
				"	</tr>"+
				"</tfoot>"+
				"<tbody>"+
				"</tbody>"
				);
			var numberSelected = 1;
			iduserMasuk = userMasuk();
			arraySiswa = dbSiswa.select("/siswa/*[/idUser =="+parseInt(iduserMasuk)+"]").values();
			for(i=0; i<arraySiswa.length; i++){
				var url = 'halaman-kelola-aktivitas.html?id=';
				var cli = arraySiswa[i];
				var halaman = "&halaman=utamaAktivitas";
			  	$("#dataSiswaAktivitas tbody").append("<tr>"+
										 	 "	<td>"+ numberSelected++ +" </td>" +
											 "	<td>"+ cli.namaSiswa+" </td>" + 
											 "	<td class='center-block'> <a class='btn btn-primary center-block' onclick='redirectPage(\""+url+ "\",\""+cli.idSiswa+ "\",\""+halaman+ "\")'><span class='glyphicon glyphicon-tasks' aria-hidden='true'></span> Kelola Rangkaian Aktivitas</a></td>" + 
			  								 "</tr>");
			  	
			}
}

function onLoad() {
   document.addEventListener("deviceready", onDeviceReady, false);
}

function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
}

function onBackKeyDown() { 
  	window.location.href = "halaman-utama-aktivitas.html";
	return false;
}
