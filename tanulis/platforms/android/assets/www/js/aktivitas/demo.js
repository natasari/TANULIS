buildDemoAktivitas();
function buildDemoAktivitas(){

	var dataSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+999+"]");
	
	if(jQuery.isEmptyObject(dataSiswa.value()) == true){
		var aktivitasHurufKecil = ({
				idSiswa : 999,
				idRangkaian : 991,
				idAktivitas : 1,
				namaAktivitas    : "Huruf Kecil",
				pathAktivitas  : "huruf.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					mulaiHuruf : "a",
					akhirHuruf : "a",
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Bahasa",
				isActive : "Aktif",
				isPass : "Belum"
		}); 
		tbRangkaianAktivitas.rangkaian.push(aktivitasHurufKecil);

		var aktivitasHurufBesar = ({
				idSiswa : 999,
				idRangkaian : 992,
				idAktivitas : 2,
				namaAktivitas    : "Huruf Besar",
				pathAktivitas  : "huruf.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					mulaiHuruf : "A",
					akhirHuruf : "A",
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Bahasa",
				isActive : "Aktif",
				isPass : "Belum"
		});
		tbRangkaianAktivitas.rangkaian.push(aktivitasHurufBesar); 


		var aktivitasAngka = ({
				idSiswa : 999,
				idRangkaian : 993,
				idAktivitas : 3,
				namaAktivitas    : "Angka",
				pathAktivitas  : "huruf.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					mulaiHuruf : "0",
					akhirHuruf : "0",
					//jenisTulisan : "Arial",
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Matematika",
				isActive : "Aktif",
				isPass : "Belum"
		});
		tbRangkaianAktivitas.rangkaian.push(aktivitasAngka); 

		var aktivitasSukuKata = ({
				idSiswa : 999,
				idRangkaian : 994,
				idAktivitas : 4,
				namaAktivitas    : "Suku Kata",
				pathAktivitas  : "kata.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					kata 	   : ["bu-ku"],
					path 	   : [],
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Bahasa",
				isActive : "Aktif",
				isPass : "Belum"
		});
		tbRangkaianAktivitas.rangkaian.push(aktivitasSukuKata); 

		var aktivitasKata = ({
				idSiswa : 999,
				idRangkaian : 995,
				idAktivitas : 5,
				namaAktivitas    : "Kata",
				pathAktivitas  : "kata.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					kata 	   : ["buku"],
					path 	   : [],
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Bahasa",
				isActive : "Aktif",
				isPass : "Belum"
		});
		tbRangkaianAktivitas.rangkaian.push(aktivitasKata); 

		var aktivitasHari = ({
				idSiswa : 999,
				idRangkaian : 996,
				idAktivitas : 6,
				namaAktivitas    : "Hari",
				pathAktivitas  : "hari.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					mulai : "senin",
					akhir : "selasa",
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Matematika",
				isActive : "Aktif",
				isPass : "Belum"
		}); 
		tbRangkaianAktivitas.rangkaian.push(aktivitasHari);

		var aktivitasBulan = ({
				idSiswa : 999,
				idRangkaian : 997,
				idAktivitas : 7,
				namaAktivitas    : "Bulan",
				pathAktivitas  : "bulan.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					mulai : "januari",
					akhir : "januari",
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Matematika",
				isActive : "Aktif",
				isPass : "Belum"
		}); 
		tbRangkaianAktivitas.rangkaian.push(aktivitasBulan);

		var aktivitasTekaTeki = ({
				idSiswa : 999,
				idRangkaian : 998,
				idAktivitas : 8,
				namaAktivitas    : "Teka-teki",
				pathAktivitas  : "tekateki.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					kata 	   : ["buku"],
					path 	   : [],
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Bahasa",
				isActive : "Aktif",
				isPass : "Belum"
		}); 
		tbRangkaianAktivitas.rangkaian.push(aktivitasTekaTeki);

		var aktivitasJam = ({
				idSiswa : 999,
				idRangkaian : 999,
				idAktivitas : 9,
				namaAktivitas    : "Jam",
				pathAktivitas  : "jam.html",
				paramAktivitas : ({
					ukuranTulisan : "Sedang",
					kata 	   : ["04:15"],
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Matematika",
				isActive : "Aktif",
				isPass : "Belum"
		}); 
		tbRangkaianAktivitas.rangkaian.push(aktivitasJam);

		var aktivitasKalimat = ({
				idSiswa : 999,
				idRangkaian : 9910,
				idAktivitas : 10,
				namaAktivitas    : "Kalimat",
				pathAktivitas  : "kalimat.html",
				paramAktivitas : ({
					kata 	   : ["ibu memasak"],
					level : "Mudah"
				}),
				paramResult : [],
				jenisAktivitas : "Bahasa",
				isActive : "Aktif",
				isPass : "Belum"
		}); 
		tbRangkaianAktivitas.rangkaian.push(aktivitasKalimat);

		var aktivitasGaris = ({
				idSiswa : 999,
				idRangkaian : 9911,
				idAktivitas : 11,
				namaAktivitas    : "Garis dan Bentuk",
				pathAktivitas  : "garis.html",
				paramAktivitas : ({
					kata 	   : "",
					path	   : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApIAAAFJCAYAAAAomc0XAAAgAElEQVR4Xu3dX3rURpfA4XOkPJPchawgZgUxK8CsALMCzApi7rqTSMgSX7rvMCvAWQFmBZgVYFaAs4LgO3smUs1Topuv6f/ddLeqVD/fzExoqeq8pzx9XFJVqfCDAAIIIIAAAggggMAaArrGNVyCAAIIIIAAAggggIBQSDIIEEAAAQQQQAABBNYSoJBci42LEEAAAQQQQAABBCgkGQMIIIAAAggggAACawlQSK7FxkUIIIAAAggggAACFJKMAQQQQAABBBBAAIG1BCgk12LjIgQQQAABBBBAAAEKScYAAggggAACCCCAwFoCFJJrsXERAggggAACCCCAAIUkYwABBBBAAAEEEEBgLQEKybXYuAgBBBBAAAEEEECAQpIxgAACCCCAAAIIILCWAIXkWmxchAACCCCAAAIIIEAhyRhAAAEEEEAAAQQQWEuAQnItNi5CAAEEEEAAAQQQoJBkDCCAAAIIIIAAAgisJUAhuRYbFyGAAAIIIIAAAghQSDIGEEAAAQQQQAABBNYSoJBci42LEEAAAQQQQAABBCgkGQMIIIAAAggggAACawlQSK7FxkUIIIAAAggggAACFJKMAQQQQAABBBBAAIG1BCgk12LjIgQQQAABBBBAAAEKScYAAggggAACCCCAwFoCFJJrsXERAggggAACCCCAAIUkYwABBBBAAAEEEEBgLQEKybXYuAgBBBBAAAEEEECAQpIxgAACCCCAAAIIILCWAIXkWmxchAACCCCAAAIIIEAhyRhAAAEEEEAAAQQQWEuAQnItNi5CAAEEEEAAAQQQoJBkDCCAAAIIIIAAAgisJUAhuRYbFyGAAAIIIIAAAghQSDIGEEAAAQQQQAABBNYSoJBci42LEEAAAQQQQAABBCgkGQMIIIAAAggggAACawlQSK7FxkUIIIAAAggggAACFJKMAQQQQAABBBBAAIG1BCgk12LjIgQQQAABBBBAAAEKScYAAggggAACCCCAwFoCFJJrsXERAggggAACCCCAAIUkYwABBBBAAAEEEEBgLQEKybXYuAgBBBBAAAEEEECAQpIxgAACCCCAAAIIILCWAIXkWmxchAACCCCAAAIIIEAhyRhAAAEEEEAAAQQQWEuAQnItNi5CAAEEEEAAAQQQoJBkDCCAAAIIIIAAAgisJUAhuRYbFyGAAAIIIIAAAghQSDIGEEAAAQQQQAABBNYSoJBci42LEECgzQJ/pun9afH9lufv2hw3sSGAAAKrClBIrirG5xFAwCuBLMvu/E9V/RKJ3DHG7A87r3H8VyfLruz/bT/zfVW9FmMOFgWnIp9E5GmnKM6G1/5Qlo9F9cAYc2n/m6peViKf/jeKPmRZZj/PDwIIINBKAQrJVqaVoBAIU6CfZXtSlg+Nqi0Y9+YVhrYgvInju7bQ6yfJmRF5vIqaxvFdW4j20vTtwgJU9UJErkT1onty8tcq7fBZBBBAwGUBCkmXs0PfEEBgaYH+H38ciOprI3Jn6YtUn3bz/LSfJBdGZOrj7Fn3UmMeyHffXZmy/Lh0e5+nK1928/x49Bo7I8rM5UqKfBgBBBwRoJB0JBF0AwEE5gv8J8v2o7J8aB8hqzH7RvW8iqKXv2dZ/Ti5lyTnIvJwFUdVPenkebZ2ISkiRvXtKm3az3aLov7/vTamuCxfGZHPj9ztzKUxF0b1gvcxV1Xl8wgg0IQAhWQT6rSJAAILBer3Fo15qMYc2kfHU2ca7aPiPH9gb7ZOMSjDGck0zYwxzxZ2avgB1evbKNr7wb53ueqM5EghOe+xuH30botlW1zeqr5hxnLp7PBBBBDYoQCF5A6xaQoBBBYL9J49eyzGHC1873BwqyqO79lZyf7qxeCH2yg6sAWaLVp/qKpjI2KL1l9UZPrqbNU9u6CmiuNsOBO6arv23p2iOLDvc65UhKqejc7ALpbkEwgggMD2BSgkt29MCwggsKRAL01PxZhfl/x4/TH7rmLn+fOLwcrrKzHmx/Hrh4WhUb2sV11X1YW9ZpV25n3WFoVVWe5Hnxf5SD2DqnrHFqVfXad6rVV1aNsePKp/v2IfrrpFcXfFa/g4AgggsDUBCsmt0XJjBBBYVaCXJGbVa4arp4fX9ZPkSFT3bLFoF8MMt/hZ9b6b+ny9kvzff/fs/caL116aXk4UmwsaHs7ADj9m97w0UXQ9nCHdVL+5DwIIILCMAIXkMkp8BgEENiLw5b3Hz4+R7erqs25RnAxv3kvTT9NmFKc1rqp/V8Yc/1YUdpGNlz/1I/WyPDWq9pH6xEzqtKBu4/inL4/jy/LjyLujVxJFGe9TejkU6DQC3gpQSHqbOjqOgD8Cg/0d7SPro/FFM8OV0zaaJfZzfGMXn1RRdNG2Gbj6UXdVHQzeDZ26+lxF/uoUxdE8q8GG6WcSxy+bno31Z4TSUwQQWFeAQnJdOa5DAIGFAvWCkqp6Vi+emfFjC59OUfxk/3mw6OXSGPOz/b/trKPdDqcSOfd55nEh1JQP/Jkkh5Fd/PP5xBzr8eY2jo+Gq7eXeg1A9Uyj6ISCcp0McA0CCCwjQCG5jBKfQQCBlQQGs2svll15PXxcO2zEXm//97bNOq6EuODDSxWSg3uo6vlNFD1hC6FNZoB7IYBA/Qc/DAgggMAmBexiFyPyatl72lnHTp7Xi1H4WV5g1RXuKnLZKYp7y7fAJxFAAIHFAhSSi434BAIIrCDQT5J/lj2m0G7LU8bxMTOPKwCPfHSwQt1upl6/CrDoZ3yF+6LP8+8IIIDAIgEKyUVC/DsCCKwksMwjV7toRIw52+Rejit1smUfHpwzbmeCH88MTfW6m+fLn0PeMiPCQQCB7QhQSG7Hlbsi0HoBW7wY1ftVHL8ZnVGceea16rXd7kej6JTFH9sZHoPV8dnU7YRUX3bz/Ni2PNi8/fXgHdZ626Duyclf2+kVd0UAgTYLUEi2ObvEhsAWBKYtpFGRJ52iOLPNDVZq2422P++LaE9zETm9iaJTFntsISFTbjk88rHebmmw4rtbFIfDj/aT5L0R+XwKz/Dn87ZKT3nNYDc5ohUE2iJAIdmWTBIHAlsWqGexytLuBZlNa2p05fWgmDxUYz7dxPE5BeSWk7PC7Zc4mjG7jeOX5GwFVD6KQMACFJIBJ5/QEVhWYPAY267Enr26WvVpN89Pl70nn2tGYJDLtwtav1JjnvAOazM5olUEfBKgkPQpW/QVgR0LDI7we2ZE6nfr5v2MPt5e9Fn+vTmBwfuRV8scyWhfSegUxdPmekvLCCDgugCFpOsZon8INCRgH4HGZflq4l26Kf0ZPbqvoe7S7AoCg1cPzsWYXxZdxv6Ti4T4dwTCFqCQDDv/RI/AVIHBTOTHhftBfl6JnfFI28+B1EtTO9OcLZqdZLbZz/zSawR2IUAhuQtl2kDAMwF7zrOKvF7Q7a/OfvYsRLo7EBgsorIr7h/OROH9V8YLAgjMEKCQZGgggMCEwNyVvarXxpij34riHLr2CNR/PKieTcxOql7fRtEeq7jbk2siQWCTAhSSm9TkXgi0SKCfJBdG5P5YSMxCtijH46EMXmk4HT0hZ/yxdj9NHxpj9sc3om8xC6EhgMAcAQpJhgcCgQv0nj17LMYcqTGXN3F8Mpx5Gn3kqap/V8YcMwsZxmAZLLTalzi+GD2FqJemr+xYGSqwqjuM8UCUCMwToJBkfCAQqMDYMXm1gl2hexPHD3iMGeigmBP2rPdmVfX8JoqeMGYYMwiEKUAhGWbeiTpwgcEjzLdTt/YZOZM5cCbCHxHoJ8nZ6CPvURz+AGGoIBCuAIVkuLkn8kAFBo8tbRF5ZxqBirzrFMVBoDyEPUOgn6aZMebZLCAV+VTG8QPO6mYIIRCWAIVkWPkm2sAFFhWR9eNt1ZNOnk89TztwvqDDH7wKcTFvE3OKyaCHCMEHKkAhGWjiCTs8AfuOWyRiT6qZOhNZF5Eif3WK4stiivCUiHiewOCViPMpq/m/XGaLSTHmEed0M5YQCEOAQjKMPBNl4ALLbDBOERn4IFkh/HnvSw7+IPkkcXxvdMX3Crfnowgg4JEAhaRHyaKrCKwjYM9VlrJ8P/e4Q04uWYc26Gvq4xWNeTETgTEV9Pgg+HAEKCTDyTWRBiqwaDaSc5QDHRgbCLufJEdG5NW0WzGuNgDMLRDwQIBC0oMk0UUEvkVg5nGHHHX4LaxcOxCoi0nV06+OVlT90M3zfZAQQKD9AhSS7c8xESIgvTS1X/S/fqFQva6i6ICtWhgcmxAY7AZgj1a8b9+1vYnjYzYo34Qs90DAfQEKSfdzRA8R2IjAYNX2sYhcSRxnLITYCCs3WSDQS5JnonpQr+aOoqeMO4YMAu0SoJBsVz6JJnCBenuWqnpljDm0R9fxxR34gGg4/H6avrZjcdgN9plsOCE0j8AWBCgkt4DKLRFoQmDasYf1LBDbsDSRjuDbnHk2N+e5Bz82AGiXAIVku/JJNAEL9JPEbvEzucCBs7MDHhXNhT5veyB7NnenKO411ztaRgCBTQlQSG5Kkvsg0KBAL01fiTFTT6Rho/EGExNw03b/UlNVl1+t5h71UD3r5vmTgIkIHYFWCFBItiKNBBGyQD9NM2PMs1kGaswDjqsLeYQ0F/u8fSbrXrFpeXPJoWUENiRAIbkhSG6DQBMC/T/+ODCqb2cWkaonnTzPmugbbSJgBSa2nhpj4Q8dxgkCfgtQSPqdP3ofsMCiow95pB3w4HAs9Hlnc7MgzLFk0R0EVhSgkFwRjI8j4IrAzMU1nx8ZfriNogM2hXYlW2H3w+4o8H1VXYgxv0yT4I+esMcH0fstQCHpd/7ofaACc989U72+jaI9ishAB4ejYc9bfKMi7zpFceBo1+kWAgjMEaCQZHgg4KHAvAU2VRzf4+hDD5MaQJdnnfuuvMsbQPYJsa0CFJJtzSxxtVpg5iIbVsG2Ou9tCG58Np3H2m3IKjGELEAhGXL2id1rgfoLWfV0uE8fX8hepzOoztuZybiqDqWqLtiaKqjUE2wLBSgkW5hUQgpHYLBy+6CM40seZ4eT9zZGOjji87GJok+3qm94x7eNWSamNgpQSLYxq8TUSoHes2ePpar21Jh3zOK0MsXBBlXPUJblWyNyxyLYIxRv4vgBxWSwQ4LAPRKgkPQoWXQ1XIGJIxA5Xi7cwdCyyAczkR+HReSX8FQvunn+oGXhEg4CrROgkGxdSgmobQJ/Jsmhirwej0tFnnSK4qxt8RJPWALzTmcyIo9+K4rzsESIFgG/BCgk/coXvQ1MYOZsTb3nOMcfBjYcWhnuYLPyq+GisdEg7ak3N3F8l0fcrUw9QbVEgEKyJYkkjHYKzDunmBnJduY8xKjm7Ysqqi+7eX4cogsxI+CDAIWkD1mij0EKzNq8ucZQ/dDN8/0gYQi6lQK9NL2cdYQim+y3MuUE1RIBCsmWJJIw2ifQS9O3YszUY+PUmAes3G5fzkOOaN67ksLCm5CHBrE7LkAh6XiC6F6YArMW2NSTkSJ/dYriKEwZom6zQD9JzozI42kxsvCmzZknNp8FKCR9zh59b61AL0k+isjeRICq17dRtMfig9amPujAFiy8uewUxb2ggQgeAQcFKCQdTApdCltg3mykcJZ22IMjgOjnLbzpFgXfWQGMAUL0S4BfSr/yRW8DEJj1Raqqf3fyfHKWMgATQgxLoJ+mV8aYn0ejVpF3naKY+s5wWDpEi4BbAhSSbuWD3iAgs2YkeUeMwRGKQL1jQVVdfNlbUvVDFUVHnCcfygggTp8EKCR9yhZ9DUagl6bHYsyLLwGzl14wuSfQzwL9LNszVXVojLnidBtGBQLuClBIupsbeha4gP0ilbI8KOP4kpmYwAcD4SOAAAKOClBIOpoYuoUAAggg8F+BXpI8E5HP2159nqE/xQcBBJoXoJBsPgf0AIH6MZ6U5WsjYk+ruZIoyronJ39BgwACItM252dTfkYGAm4IUEi6kQd6EbjA1FNs2Oon8FFB+FZgzok3V92iuIsSAgg0K0Ah2aw/rSMwc5U2250wOBAYLLopS7tB/8QPs5KMEASaF6CQbD4H9CBwgV6SnIvIwykMb7pFcRg4D+EjINP2lRyw8DvC+ECgYQEKyYYTQPNhC9RbnMyabRF50imKs7CFiB4BkX6SHBmRV1NnJeP4bifLrnBCAIFmBCgkm3GnVQRqgV6anooxv04+s9Prbp7fgQkBBETmncE9WMF9jBMCCDQjQCHZjDutIlAL9JPkHyMyWTCyATkjBIGvBGb+0SXCohvGCgINClBINohP02ELzDoK0aooj+vCHhxEPyEw7zUQjg9lwCDQnACFZHP2tBy4AItsAh8AhL+yQD9JLozI/SkXsuhmZU0uQGAzAhSSm3HkLgisLNBLEjPtImWRzcqWXBCGwKxFN2yVFUb+idJNAQpJN/NCr1ouMHOTZWWRTctTT3jfKDBjKyBmJL/RlcsRWFeAQnJdOa5D4BsFemn6SYz5cfQ2qnrSyfPsG2/N5Qi0VmD8jzA7G3kTx4dZln1qbdAEhoDDAhSSDieHrrVb4D9Zth9V1ZkY84uNlMdz7c430W1OwG4H9MO//+6X33336fcsu9zcnbkTAgisKkAhuaoYn0dgwwK2oLS35Atxw7DcDgEEEEBg6wIUklsnpgEEEEAAgW0I1FsCVdUzEdlTYy4ljl9yys02pLknArMFKCQZHQjsWKCXpseD02z2VPW8jKITZiN3nASa817AzuTHZfn2qw39VS+6ef7A++AIAAGPBCgkPUoWXfVfYNqmyirySeL4HjMp/ueXCHYnMOukGzXmQef584vd9YSWEAhbgEIy7PwT/Y4F+klyZkQeTzSr+rSb56c77g7NIeCtQD9NM2OMfaz99Q/Hi3qbUzrupwCFpJ95o9eeCvSS5KN9n2viu49ZFE8zSrebEpi1Oblw9nZTKaHdQAUoJANNPGHvXmDeWcG3cfwT++DtPie06K/AvN8nzqr3N6/03D8BCkn/ckaPPRWYOYOi+qGb5/UWQPwggMDyAr00vRzuwzp6FceMLm/IJxH4VgEKyW8V5HoElhSY837ky26eHy95Gz6GAAIDgZkLbkT+6hTFEVAIILB9AQrJ7RvTAgK1QD9J3huRiZlHI/Lot6I4hwkBBFYT+DNJDlXk9fhVKnLZKYp7q92NTyOAwDoCFJLrqHENAisK2CPdvi/Lf6ZdxvuRK2LycQQGAvxeMRQQaF6AQrL5HNCDAAT6f/xxYFTfTsycqP7dyfOJVdwBkBAiAhsR6KfplTHm54nfLXZC2IgvN0FgkQCF5CIh/h2BDQgMTrN5MfFlx7tcG9DlFiELzNpPsorje5wYFfLIIPZdCVBI7kqadoIWmPkuF7MmQY8Lgv92gfrxdlVdiTE/Du+m/IH27bDcAYElBSgkl4TiYwh8q8D4qm0VedcpioNvvS/XIxC6QL2nZFXZhTd3KmMuWbwW+ogg/l0KUEjuUpu2ghewX3hSlgdlHF/y2C344QAAAggg4L0AhaT3KSQABBBAAIH6EbcxD6Wq9iSKrm5V33BaFOMCge0LUEhu35gWELB7SP4qIkd2H0lVPS+j6IQZSQYGApsTGN+nVUU+dYrip821wJ0QQGCaAIUk4wKBLQv8J8v2o7J8P9qM/ZK7ieO7zJhsGZ/bByEwc3stkSedojgLAoEgEWhIgEKyIXiaDUdg1tGIyortcAYBkW5VYFYhKSJvukVxuNXGuTkCgQtQSAY+AAh/+wL9JLkwIvfHW6KQ3L49LYQhMGdGkp0RwhgCRNmgAIVkg/g0HYZAP0n+MSJ3JgrJOL7bybKrMBSIEoHtCUx7fcS2xnuS2zPnzggMBSgkGQsIbFmglyRmWhPdouD3b8v23D4cAX7Pwsk1kbolwBeZW/mgNy0TqDdKLsuPE2GpXnfzfGKWsmXhEw4COxOYVUjexvFPLGrbWRpoKEABCskAk07IuxPg3a3dWdNS2AK8ixx2/om+OQEKyebsaTkAAQrJAJJMiE4IUEg6kQY6EaAAhWSASSfk3Qn0k8RuQv5qSotsS7K7NNBSAAK9JDkXkYfjoRqRR5y9HcAAIMTGBCgkG6On4RAE+mmaGWOejceqqiedPM9CMCBGBHYhMPN3jf1ad8FPGwELUEgGnHxC374AJ25s35gWELACUxe2qV7fRtEei20YIwhsT4BCcnu23BmBWmDikZvqh26e78ODAAKbFRj84XasIneM6qVG0Sl7tW7WmLshMPGEDRIEENi+gN0wWctyLzLmU+f584vtt0gLCCCAAAIIbF+AGcntG9MCAvJnmtZHJJoouv49yy4hQQCBzQvYx9tSVS+GJ0kZY16y0GbzztwRgVEBCknGAwJbFMiy7M4PZfnWiHx5lM1Cmy2Cc+tgBezv2vdl+V5E9r76kmOxTbBjgsB3I0AhuRtnWglUoJemx2LMi/Hwqzi+x8xkoIOCsLciMHNhGzskbMWbmyIwFKCQZCwgsEWBWXvbKbMkW1Tn1iEKsPl/iFknZhcEKCRdyAJ9aK0Ap220NrUE5pgAhaRjCaE7wQhQSAaTagJtQoBCsgl12gxR4M8kOVSR11Ni5xSpEAcEMe9MgEJyZ9Q0FKJAL01PxZhfJ2JXfdrN89MQTYgZgW0IcIrUNlS5JwKLBSgkFxvxCQTWFuDLbW06LkRgJQF+11bi4sMIbEyAQnJjlNwIgUkBvtwYFQjsRoDftd040woC4wIUkowJBLYowJfbFnG5NQIjAvyuMRwQaEaAQrIZd1oNRIAvt0ASTZiNC/C71ngK6ECgAhSSgSaesHcjMGslKafb7MafVsIRmFVIGpFHHJMYzjgg0t0LUEju3pwWAxKwZ/+aqroUY34cDZsNyQMaBIS6E4H6d60sP37VmOr1bRTtZVn2aSedoBEEAhSgkAww6YS8W4F6VlI1E2N+EdUPInLG1j+7zQGthSFgNyUX1SN73rYR+VTFccZRpGHkniibE6CQbM6elhFAAAEEEEAAAa8FKCS9Th+d90ngzzS9HxmzL3H8ppNlVz71nb4i4LqAfbQtZfnQqB6KyJVW1V+d588vXO83/UPAdwEKSd8zSP+dF8iy7M4PZfnWiOx/6azqy26eHzvfeTqIgCcCvTR9K8YcjHaXd5E9SR7d9FqAQtLr9NF5HwR6aXosxrwY7+ttHP/EIgAfMkgfXReYutDmc6c5Z9v15NE/7wUoJL1PIQG4LjBrWxLhvG3XU0f/PBGY9ccahaQnCaSbXgtQSHqdPjrvgwAbJfuQJfroswC/Yz5nj777LkAh6XsG6b/zAv0kOTIir8Y7qiLvOkXx1TtdzgdDBxFwUKCfJBdG5P5419iM3MFk0aXWCVBIti6lBOSagN3bzqi+ndKvq25R3HWtv/QHAd8EekliNyLfm/hjzZgHrNz2LZv01zcBCknfMkZ/vRToJYmZ1vFuUfA76GVG6bQrAnZXhO/L8h9+v1zJCP0ITYAvsdAyTryNCPTT9MoY8zMzJo3w02iLBWbN+Kvq3508n5ilbDEFoSHQiACFZCPsNBqaQC9JzkXk4UTcrNwObSgQ74YFWLG9YVBuh8CKAhSSK4LxcQTWEZizBRAbk68DyjUIDARYsc1QQKBZAQrJZv1pPRCBOQtu2DA5kDFAmNsRmFVIVnF87/csu9xOq9wVAQSGAhSSjAUEdiQw7T1JFXnSKYqzHXWBZhBonUB9qk1VXYoxP458sbG1VusyTUCuClBIupoZ+tU6gcGspD1f+6GofjDGZL8VhX13kh8EEPgGgUExeajG2PPsL27i+JzjR78BlEsRWEGAQnIFLD6KAAIIIIAAAggg8F8BCklGAwINCdhZlE6WXTXUPM0i4L2A/R2SsrQz/HbT//Nb1TfMRHqfVgLwTIBC0rOE0V3/Bf5MkkMVeWFP4lCRT6L6spPnmf+REQECuxXopelbMebLMaMqcnkTxw8oJnebB1oLW4BCMuz8E/2OBf6TZftRWb4fb5YVpjtOBM15LzD4g+z1RCDszep9bgnALwEKSb/yRW89F+gnyZEReTUehor81SmKI8/Do/sI7EygnyRnRuTxxO+S6gkz/DtLAw0hIBSSDAIEdigw8zg3kU+dovhph12hKQS8FugnyT9G5M54EEbkEbsheJ1aOu+ZAIWkZwmju34LZFl25/uy/GdaFGrMg87z5xd+R0jvEdi+wJwN/qVbFHyvbT8FtIDAFwF+4RgMCOxYYM652xyXuONc0JyfAr00PRVjfp3Se06K8jOl9NpjAQpJj5NH1/0UmPWepIhcdYvirp9R0WsEdicw67E2J0XtLge0hMBQgEKSsYDAjgXqUzjK8uO0Znm8veNk0Jx3AjNXa4vIbRz/xNY/3qWUDnsuQCHpeQLpvp8C/SS5MCL3x3vP6m0/80mvdycwa7W2iPBYe3dpoCUEvghQSDIYEGhAgMfbDaDTZCsE+kny3ojYM7W/+uGxdivSSxAeClBIepg0uuy/wLzV26w69T+/RLA9gV6aXooxv3xdRer1bRTt8Vh7e+7cGYFZAhSSjA0EGhKY+ohO9UM3zydmWxrqIs0i4JzAtHcklU3IncsTHQpHgEIynFwTqWMCdlbyh6q6NMb8bLumIu/KOD7+PcsuHesq3UHAKYF6wVpVHasx9o+us05RnDnVQTqDQEACFJIBJZtQ3RSw52/H9mSbLLtys4f0CgEEEEAAgekCFJKMDAQQQAAB5wXsH1xRWT5UkU8Sx2/4w8v5lNHBQAQoJANJNGG6L2CPfRPVF3ZFqqqe30TRExYPuJ83erh9gUER+X7YUl1Mijzlkfb27WkBgUUCFJKLhPh3BHYgMHU7INWLbp4/2EHzNIGA0wLTtvyxxWSnKH5yuuN0DoEABCgkA0gyIbovMHODcmMedJ4/v3A/AnqIwHYE5p1kw1ZZ2zHnrgisIkAhuYoWn0VgSwK9JDkXkYcTt2dWckvi3NYXgV6avhVjDqb8brBVltGnGcUAABELSURBVC9JpJ+tFqCQbHV6Cc4XgTkn3Qjnb/uSRfq5aQH73rBRfTvtvpxks2lt7ofAegIUkuu5cRUCGxfop+nVcE/Jr27OrOTGrbmhHwKzZiNV9e9Onu/5EQW9RKDdAhSS7c4v0Xkk0EvTYzHmxdTZF96V9CiTdHUTAsxGbkKReyCwfQEKye0b0wICSwnU529X1ZUY8+PEBcxKLmXIh9ojMOfdSM7Vbk+aiaQFAhSSLUgiIbRHoJ+mmTHm2bSIjMij34rCLsrhB4FWC8ybjRTVp908P201AMEh4JEAhaRHyaKr7RcYP397LOI33aI4bL8CEYYuMOsPKt6NDH1kEL+LAhSSLmaFPgUtMGsFt4q86xTF5DYoQWsRfBsF5vwOPOE0mzZmnJh8FqCQ9Dl79L21Ar00vRRjfhkNUFVPOnmetTZoAkNgRKCfJGdG5PHIf2JGnhGCgIMCFJIOJoUuIdDPsj2pqot6OyDVaxHJeC+McRGaQH3+fBQdiDFXN3F8ztnzoY0A4vVBgELShyzRx2AFbEHZybKrYAEIPCgB+44wxWJQKSfYFghQSLYgiYQQjsBgNev9Ko7f/J5ll+FETqRtF+gnyQsjcmzjVJHTmzg+oahse9aJrw0CFJJtyCIxtF5gsJr7lTHmy6ptjohrfdqDCXDqKm32Tg0m/wTqtwCFpN/5o/eBCExZeFBHXsXxPWYmAxkELQ3zP1m2H5Xl+2nhMb5bmnTCapUAhWSr0kkwbRXoJ8mFEbk/Hp+KXHaK4l5b4yau9gv0k+S9EdmfFqnG8V3eEW7/GCBCvwUoJP3OH70PRGDWjKQNn22BAhkELQxz3klOIsJ2Py3MOSG1T4BCsn05JaIWCtjV26aq7N6Sk+dw84i7hRlvf0gLjkG81ijaZzay/eOACP0XoJD0P4dEEIhAL02PxZgXM8K9uo3je6xyDWQweB6mXTz2/ef3IvemhsJ52p5nmO6HJEAhGVK2idV7gV6SnIvIw2mBqOp5J88feR8kAbReoJ+mr0d3IBgLmEfarR8BBNgmAQrJNmWTWFovUM/kVNXVrEfcbAnU+iHgfYCLHmnfRtEeM+vep5kAAhKgkAwo2YTaDoE/k+RQRV7PesTdLYq77YiUKNooMG+BjRrzoPP8+UUb4yYmBNoqQCHZ1swSV6sFeml6Ksb8OhGk6nU3z++0OniC81pg5h9Cqi+7eV6fbMMPAgj4I0Ah6U+u6CkCXwn00tSu4v5l9D+yFRCDxAeBKdtZ8V6kD4mjjwhMEaCQZFgg4KnA4H3Ji2ExSRHpaSID7bZ9V1Ki6ECMueoUxVmgDISNgPcCFJLep5AAQhewR8z9n8jV+AIF+985PjH00dF8/PZRdqT62Ih8qqLoJWOy+ZzQAwQ2KUAhuUlN7oWAAwK2gIzL8pU9dk7tl7fIk9+Kwm4bxA8COxXoJ8mREXk1bNSOxzKOH1BM7jQNNIbAVgUoJLfKy80R2K2APQFHytKeXfzVghu2BdptHmhNZLyIHDG5YmcBRggC7RGgkGxPLokEAWFrFQaBCwJz94oUkW5R8N3jQqLoAwIbEOCXeQOI3AIBVwTmFpI8VnQlTa3ux+DVirfjs+Ijj7ffdYrioNUIBIdAQAIUkgElm1DbL7DEyTe8o9b+YdBYhIuKSFG91ija72TZVWOdpGEEENioAIXkRjm5GQLNC9gv86iq7LZAP07rDQsems9RG3uwTBFZRdEBC23amH1iClmAQjLk7BN7awXqYrIs388KkGKytalvJLBZi7y+dEb1miKykdTQKAJbF6CQ3DoxDSDQjMCcVbPDDrF6tpnUtK7VmUd2DiJl14DWpZyAEPjv34lYIIBAewUWFZNqzIPO8+cX7RUgsl0IzCskKSJ3kQHaQKA5AWYkm7OnZQR2IjCzmFS97ub5V/tN7qRDNNI6gVnv5VJEti7VBITAhACFJIMCgQAEphWTfMkHkPgdhmiPQlTVs3qRl+q1MeaIE5V2mACaQqAhAQrJhuBpFoFdCwwW4GS2XTXmdPSRtl0sYcrysV2E0ymKl7vuG+35ITDYXuqFGHMkIlcqctIpirPR3tvNyHldwo980ksENiFAIbkJRe6BgMcCdiYpErFnc9ePuVXksozjJ2zT4nFSt9D10TPcR2/Pe7ZbwOaWCHgkQCHpUbLoKgKbFrAzTD+U5ccpZ3N/EpGn47NNm26f+/khYF+NEJEX006rURFOqvEjjfQSga0IUEhuhZWbIuCHwKIzkVXktFMUT/2Ihl5uQ6CfJLaAPJ51b1X9u5Pne9tom3sigID7AhSS7ueIHiKwNYHBu5Ef5zVgH3VLHD/iWLutpcHJGw82GX9tRPbnjg/Vk06e1+/e8oMAAuEJUEiGl3MiRuArgUV7TdoP20U4POoOZ+CMvzc7M3LVl908nzlbGY4YkSIQrgCFZLi5J3IEvgjUxaTq6azzuYcfZMug9g+aRa871AKq12rMMe/Qtn88ECECiwQoJBcJ8e8IBCJQr8qtqnNjzM/zQtY4vstj7vYOin6aZsaYZ7MitO9EllF0yKr+9o4BIkNgFQEKyVW0+CwCLReo9wksS7sv4MNZoVZxfI8ior0DoZemx2LMixkRvrmN46Msy+yrDvwggAAC9tUnfhBAAIGvBWYVE2z10q6RMpiF/vkmit4Ni8PBpuNXE685qD7t5vlpuwSIBgEEvlWAQvJbBbkegZYKDM5Ptkfe/VKHqHpdRdHBcDbyqw2qVS+0qk440cSPwWDfgxRVu61PvSK7XkwVx/eGryyM5t7+8VDG8TGz0H7kll4isGsBCsldi9MeAp4JDFbw3pE4vhgWGjM3Mlc9lyh6yjuUbia53tKnql4YYw4neqh60c3zB272nF4hgICrAhSSrmaGfiHgsMCC9+hsz7PbOH7Ju3RuJHFQ+D+bt7G47Snvv7qRL3qBgE8CFJI+ZYu+IuCIwKKVvcPHpUbklIKyuaQNFk/9qiLH0443HO8ZK/KbyxUtI+CrAIWkr5mj3wg0KLDUXoOD/tn37yqRJ78VxXmDXQ6u6fo9x7J8LSLLHV/I5uLBjRECRmATAhSSm1DkHggEKGDfnVRVuxjnx2XC57HpMkqb+0wvSWzhPnMbpy8tqV4bY44o9Ddnz50QCEmAQjKkbBMrAhsWqN+9q6rjeRtYD5tUzmTesP782/WT5MKI3J/3KZuTmyg65V3WnaaGxhBolQCFZKvSSTAINCNQrwYuy8yIPJ7VAyPyaDjrVa8EV31cv7enenar+oZiZrXcDRbQWO+jehsf1Ysqip4Ot+nppak98vLXaXdVkb8kjjNW169mzqcRQGBSgEKSUYEAAhsTsAWlKUu7afVXj1RHNzIfvLv3fqJR1TMVOe/k+ZuNdaiFN+qn6UMjcijGHI2HpyKXnaK4Z//7YLb4cuzIyzcax8cUkC0cGISEQEMCFJINwdMsAm0WGGx4fSSqB2LMxU0cHw9nHBe9u2cX5xjV8yqKXrIJ9udRMtgg/Fc15nDR6ms15sFwY/i6sK+qQxW5U0bROZ5t/q0jNgSaEaCQbMadVhEIVqCfpldjs2TzLK6MyNNQF4IMCsFXYszB0gOGowyXpuKDCCDw7QIUkt9uyB0QQGAFgUUzktNuFeqK75WtVK81ivZ5dL3CgOSjCCDwTQIUkt/Ex8UIILCqwGCRyPmiFcWj9x1d8d1Pkl/tCS32ca1dYGKMuTSqF7/l+btV+9Lk5/9M0/tqzIGq7tsZR/vIWseOmOyl6adlt1cS1Q9aVcecd95kVmkbgfAEKCTDyzkRI+CEwPA9SqNqF47M3Yty+N7foo3Q7WITo3opxlxVcfzGlXcCB6vaHxrVfTVmv15lPePHFpOdPH9k/3nhjKSdgTTmvIzjU1didWJw0QkEENiZAIXkzqhpCAEEpgkMZijtIpLDqRtoq37o5nldeC0srMYasIXlTRw/GC70GaxkrvdWrEQ+2f/5v1H0Yd2th+z9/qeqfrH3iUTuGFU7q/hu9NHyMsdJjrsMjyqcucJd5I1d4X4Tx+fr9p3RiAACCGxCgEJyE4rcAwEENiIwLCoHeyPagu9NFcfZcLZtmU22JzoyWHxii7K4LN8uXPX8edX4pVbVyejqZ6mqF8YYW9AuPHJQRZ50iuJssB3Sx1VxRlde1ycIidRb/VA8rirJ5xFAYNsCFJLbFub+CCCwMYF+kpzN2/R8WkPD9ytXnc2sC7c4vmtnF3tJYovBhQXkSPtX3aK4u+hR/FQY1etunt/ZGBo3QgABBLYoQCG5RVxujQACmxWwM5bfV1VmZywXvVc5bPnL7OASRwaO99Zea9+3NKpvV42kWxT1/39dZbsje+IM7zuuKs3nEUCgSQEKySb1aRsBBNYWqGf7oqhe8WxXPk/bm1JV/+7keT2TuO5jca0qu4DnWwrJbNpZ5LZvdsW5XXlu22C19dpDgQsRQKBBAQrJBvFpGgEENidQv1/577/7EkWft9Ixxi60+bIYZfCu4etVWhzuX7nSNjy2AdWX3Tw/HrZVv59ZVXYxkUhVXdx8990li2RWyQSfRQABVwUoJF3NDP1CAIGNC3wp6EZOipm6n6XqtT2uupvn9txwqYtQ1bNpj9PtOeJfOqp6YQtFZhc3njpuiAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgpQSDqaGLqFAAIIIIAAAgi4LkAh6XqG6B8CCCCAAAIIIOCoAIWko4mhWwgggAACCCCAgOsCFJKuZ4j+IYAAAggggAACjgr8PxUFrCs/UynNAAAAAElFTkSuQmCC"
				}),
				paramResult : [],
				jenisAktivitas : "Bahasa",
				isActive : "Aktif",
				isPass : "Belum"
		}); 
		tbRangkaianAktivitas.rangkaian.push(aktivitasGaris);
		localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	}
}