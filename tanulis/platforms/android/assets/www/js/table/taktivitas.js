localStorage.removeItem("tbAktivitas");
var tbAktivitas;
tbAktivitas = localStorage.getItem("tbAktivitas");//Retrieve the stored data
 //Converts string to object

if(jQuery.isEmptyObject(tbAktivitas) == true){
	tbAktivitas = {
	  'aktivitas': [],
	  'state' :  true
	};
	buildTableAktivitas();
}else{
	tbAktivitas = JSON.parse(tbAktivitas);
}

dbAktivitas = SpahQL.db(tbAktivitas);

function buildTableAktivitas(){
	var aktivitasHurufKecil = ({
			idAktivitas : 1,
			namaAktivitas    : "Huruf Kecil",
			pathAktivitas  : "huruf.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulaiHuruf : "a",
				akhirHuruf : "z",
				//jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis huruf kecil dari a - z"
	}); 
	tbAktivitas.aktivitas.push(aktivitasHurufKecil);

	var aktivitasHurufBesar = ({
			idAktivitas : 2,
			namaAktivitas    : "Huruf Besar",
			pathAktivitas  : "huruf.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulaiHuruf : "A",
				akhirHuruf : "Z",
				//jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis huruf besar dari A - Z"
	});
	tbAktivitas.aktivitas.push(aktivitasHurufBesar); 


	var aktivitasAngka = ({
			idAktivitas : 3,
			namaAktivitas    : "Angka",
			pathAktivitas  : "huruf.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulaiHuruf : "0",
				akhirHuruf : "9",
				//jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Matematika",
			keterangan : "Menulis angka 0 - 9"
	});
	tbAktivitas.aktivitas.push(aktivitasAngka); 

	var aktivitasSukuKata = ({
			idAktivitas : 4,
			namaAktivitas    : "Suku Kata",
			pathAktivitas  : "kata.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				kata 	   : [""],
				path 	   : [],
				//jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis suku-kata"
	});
	tbAktivitas.aktivitas.push(aktivitasSukuKata); 

	var aktivitasKata = ({
			idAktivitas : 5,
			namaAktivitas    : "Kata",
			pathAktivitas  : "kata.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				kata 	   : [""],
				path 	   : [],
				//jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis kata"
	});
	tbAktivitas.aktivitas.push(aktivitasKata); 

	var aktivitasHari = ({
			idAktivitas : 6,
			namaAktivitas    : "Hari",
			pathAktivitas  : "hari.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulai : "senin",
				akhir : "minggu",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Matematika",
			keterangan : "Menulis hari dari senin - minggu"
	}); 
	tbAktivitas.aktivitas.push(aktivitasHari);

	var aktivitasBulan = ({
			idAktivitas : 7,
			namaAktivitas    : "Bulan",
			pathAktivitas  : "bulan.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulai : "januari",
				akhir : "desember",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Matematika",
			keterangan : "Menulis bulan dari januari - desember"
	}); 
	tbAktivitas.aktivitas.push(aktivitasBulan);

	var aktivitasTekaTeki = ({
			idAktivitas : 8,
			namaAktivitas    : "Teka-teki",
			pathAktivitas  : "tekateki.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				kata 	   : [""],
				path 	   : [],
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Mengisi huruf yang kosong"
	}); 
	tbAktivitas.aktivitas.push(aktivitasTekaTeki);

	var aktivitasTekaTeki = ({
			idAktivitas : 9,
			namaAktivitas    : "Jam",
			pathAktivitas  : "jam.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				kata 	   : [""],
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Matematika",
			keterangan : "Menebali waktu"
	}); 
	tbAktivitas.aktivitas.push(aktivitasTekaTeki);

	var aktivitasKalimat = ({
			idAktivitas : 10,
			namaAktivitas    : "Kalimat",
			pathAktivitas  : "kalimat.html",
			paramAktivitas : ({
				kata 	   : [""],
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis kalimat sederhana"
	}); 
	tbAktivitas.aktivitas.push(aktivitasKalimat);

	localStorage.setItem("tbAktivitas", JSON.stringify(tbAktivitas));

	var aktivitasGaris = ({
			idAktivitas : 11,
			namaAktivitas    : "Garis dan Bentuk",
			pathAktivitas  : "garis.html",
			paramAktivitas : ({
				kata 	   : "",
				path	   : ""
			}),
			paramResult : [],
			jenisAktivitas : "Matematika",
			keterangan : "Membuat garis dan bentuk"
	}); 
	tbAktivitas.aktivitas.push(aktivitasGaris);

	localStorage.setItem("tbAktivitas", JSON.stringify(tbAktivitas));

	console.log(tbAktivitas);
}

function redirectPage(url,id) {
		if(id==null)id = "";
		window.location.href=url+id;
		return false;
}
