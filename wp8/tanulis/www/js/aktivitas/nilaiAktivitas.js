$(document).ready(function(){
	allHasil = [];
	idSiswaChoosen = getUrlVars()["id"];
	console.log(tempAktivitas);
	$("#nilai").html("");
		$("#nilai").html(
			"<thead>"+
			"	<tr>"+
			"	<th></th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);

		tempA = tempAktivitas.temp;
		width = document.getElementById("nilai").offsetWidth;
		
		
		 $("#nilai tbody").append("<tr>"+
			 	 "  <td>" +loopCanvas(tempA.length) +
				 "  </td>" +
					 "</tr>");

		$("#nilai tbody").append("<button type='submit' onclick='simpan()' class='btn btn-primary pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan</button>");

		for(i=0; i<tempA.length; i++){
			loopShow(i, tempA[i].letter, tempA[i].pathHuruf, tempA[i].pathTulis);
		}

		var namaSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(idSiswaChoosen)+"]/namaSiswa");
		$("#namaSiswa").html("Nilai Aktivitas : <strong>"+namaSiswa+"</strong>");
		localStorage.removeItem("tempAktivitas");
});

function loopCanvas(length){
	tempCanvas = [];
	for(j = 0; j<length; j++){
		h = width/2;
		w = width;
		tempCanvas = tempCanvas + "<div style='width:"+w+"px; height:"+h+"px; float:left;'><canvas id=canvasb"+j+" height="+h+"px width="+w+"px style='position: absolute; z-index: 1;'></canvas>"+
								  "<canvas id=canvas"+j+" height="+h+"px width="+w+"px style='border:solid 1px;position: absolute; z-index: 2;'></canvas>"+
								  "</div><input id='input"+j+"'class='form-control' type='number' name='number' min='0' max='100' placeholder='Masukkan Nilai'>"	
	}

	return tempCanvas;
}

function loopShow(i, letter, pathHuruf, pathTulis){
	var canvas2 = document.getElementById("canvasb"+i);
	var ctx2=canvas2.getContext("2d");

	var img2 = new Image();
	img2.onload = function() {
	  ctx2.drawImage(this, 0, 0, canvas2.width, canvas2.height);
	}
	img2.src = pathTulis;
	var canvas = document.getElementById("canvas"+i);
	var ctx=canvas.getContext("2d");

	var img = new Image();
	img.onload = function() {
	  ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
	}
	img.src = pathHuruf;
}

function simpan(){
	var length = tempA.length;
	for(k = 0; k<length; k++){
		var score = $('#input'+k).val();
		if(jQuery.isEmptyObject(score) == true){
			score = 0;
		}

		if(tempA[k].idAktivitas == '11'){
			editResultAktivitasGaris(idSiswaChoosen, tempA[k].idAktivitas, tempA[k].idRangkaian, tempA[k].indeks, score);
		}else{
			editResultAktivitas(idSiswaChoosen, tempA[k].idAktivitas, tempA[k].idRangkaian, tempA[k].indeks, score, tempA[k].letter, tempA[k].level);
		}
	}
		window.location.href = "hasil-mulai-aktivitas.html?id="+idSiswaChoosen;
}