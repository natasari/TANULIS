function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[#&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
	vars[key] = value;
	});
	return vars;
}

$(document).on('click', "#logout", function () {
	var idUserLogin = userLogin();
	if(logout(idUserLogin)){
		window.location.href = "index.html";
	    return false;
	}
});