$(document).ready(function(){
	var idSiswaChoosen = getUrlVars()["id"];
	showTombolPlay(idSiswaChoosen);

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	if(dd<10) { dd='0'+dd } 
	if(mm<10) { mm='0'+mm } 
	today = mm+'/'+dd+'/'+yyyy;

	hasilData = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+ parseInt(idSiswaChoosen) + "][/isActive == 'Aktif']*[/paramResult/*/tanggal == '"+today+"']");
	hasil = hasilData.values();
	console.log(dbRangkaianAktivitas.value());
	$("#hasil").html("");
		$("#hasil").html(
			"<thead>"+
			"	<tr>"+
			"	<th>No</th>"+
			"	<th>Nama Aktivitas</th>"+
			"	<th>Hasil</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);

		for(i=0; i<hasilData.length; i++){
			n = hasil[i].paramResult.length;
			a = hasil[i].paramResult;
		  	$("#hasil tbody").append("<tr>"+
		  		 "	<td>"+(i+1)+" </td>" +
			 	 "	<td>"+hasil[i].namaAktivitas+" </td>" +
			 	 "  <td>"+looping(a, today, hasil[i].namaAktivitas)+
				 "  </td>" + 
					 "</tr>");
		}
		/*ambil nama dengan id siswa x*/
		var namaSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(idSiswaChoosen)+"]/namaSiswa");
		$("#namaSiswa").html("Hasil Aktivitas : <strong>"+namaSiswa+"</strong>");

});

function looping(a, today, namaAktivitas){
	var allHasil="";
	var ind = 0;
	for(j=0; j<a.length; j++){
		if(a[j].tanggal == today){
			if(ind == 0 ){
				allHasil = allHasil + a[j].tanggal + "<br>";
				ind = 1;
			}
			z= a[j];
			for (var property in z){
				if(property == 'parameterYangDinilai'){
					if(namaAktivitas == 'Garis dan Bentuk'){
						allHasil = allHasil + ("<b>Soal" +": <img src='" + z[property] + "'/></b> " );
					}else{
						allHasil = allHasil + ("<b>Soal" +": " + z[property] + "</b> " );
					}
				}
				else if(property != 'tanggal'){
					allHasil = allHasil + (property +": " + z[property] + " " );
				}
			}
			allHasil = allHasil + "<br>";
		}
	}
	return allHasil;
}

function showTombolPlay(siswa){
	url = "halaman-mulai-aktivitas.html?id=";
	$("#buttonPlay").html("<button onclick='redirectPage(\""+url+ "\",\""+siswa+ "\")' type='button' class='btn btn-success' style='float:right;margin-left:5px;'>" +
                               "<span class='glyphicon glyphicon-triangle-right' aria-hidden='true'></span>Ulangi Aktivitas</a>");
	urlPengelolaan = "halaman-kelola-aktivitas.html?id=";
	$("#pengelolaan").html("<button onclick='redirectPage(\""+urlPengelolaan+ "\",\""+siswa+ "\")' type='button' class='btn btn-primary' style='float:right;margin-left:5px;'>" +
                               "<span class='glyphicon glyphicon-tasks' aria-hidden='true'></span> Kelola Rangkaian Aktivitas</a>");
}