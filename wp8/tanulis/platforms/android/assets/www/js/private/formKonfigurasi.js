var max_fields      = 5; //maximum input boxes allowed
var paths = new Array();
var indexPic = new Array();
var dataForImage;
var nForImage;
var pathForImage;

$(document).ready(function(){
	var idSiswaChoosen = getUrlVars()["id"];
	var idRangkaian = getUrlVars()["rangkaian"];
	var idAktivitas = getUrlVars()["aktivitas"];

    // KE ENAM BELAS AKTIVITAS CUY //
	if(idAktivitas == "1"){
		showHuruf(idSiswaChoosen,idRangkaian,idAktivitas);
	}
	else if(idAktivitas == "2"){
		showHurufBesar(idSiswaChoosen,idRangkaian,idAktivitas);
	}
	else if(idAktivitas == "3"){
		showAngka(idSiswaChoosen,idRangkaian,idAktivitas);
	}
	else if(idAktivitas == "4" || idAktivitas == "5" || idAktivitas== "8"){
		showSukuKata(idSiswaChoosen,idRangkaian,idAktivitas);
	    var add_button      = $("#add_field_button"); 
		var x = 1; 

	    $(add_button).click(function(e){
	        e.preventDefault();
	        if(x < max_fields){ 
	            x++; 
	            if(jQuery.isEmptyObject(indexPic) == true){
	            	if(jQuery.isEmptyObject(paths) == true){
	            		pushValue  = 1;	
	            	}
	            	else{
	            		pushValue = parseInt(paths[paths.length-1].indeks) + 1; 	
	            	}
	            }
	            else{
	            	lastValue = indexPic.length;
		            pushValue = indexPic[indexPic.length - 1] + 1;	
	            }
	            indexPic.push(pushValue);
	            $("#input_fields_wrap").append('<div id='+pushValue+' style="display:flex;"><input class="form-control" style="width:50%;" name="kata[]"/><a href="#" style="height:34px;" class="remove_field btn btn-default"><i class="glyphicon glyphicon-remove"></i></a><button type="submit" class="btn btn-default" style="height:34px;margin-left:10px; margin-right:10px;" onclick="getPhoto('+pushValue+',pictureSource.PHOTOLIBRARY);">Pilih Foto</button><br><img style="width:80px;height:60px;" id="largeImage'+pushValue+'" src="" /></div>'); //add input box
	        }
	    });
	    
	    $("#input_fields_wrap").on("click",".remove_field", function(e){ 
	    	isi = $(this).parent('div').attr('id');
	    	indexRemove =  indexPic.indexOf(parseInt(isi));
	    	indexPic.splice(indexRemove, 1);

	    	dbPaths = SpahQL.db(paths);
			getPath = dbPaths.select("/*[/indeks == "+parseInt(isi)+"]");
			if(jQuery.isEmptyObject(getPath) == false){
				lPath = getPath.path();
		    	indexRemovePath =  parseInt(lPath[getPath.path().length-1]);
		    	paths.splice(indexRemovePath,1);
			}
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    });
	}
	else if(idAktivitas == "6"){
		showHari(idSiswaChoosen,idRangkaian,idAktivitas);
	}
	else if(idAktivitas == "7"){
		showBulan(idSiswaChoosen,idRangkaian,idAktivitas);
	}
	else if(idAktivitas== "9"){
		showJam(idSiswaChoosen, idRangkaian, idAktivitas);
	    var add_button      = $("#add_field_button"); 
		var x = 1; 

	    $(add_button).click(function(e){
	        e.preventDefault();
	        if(x < max_fields){ 
	            x++; 
	             if(jQuery.isEmptyObject(indexPic) == true){
	            	pushValue  = 1;	
	            }
	            else{
	            	lastValue = indexPic.length;
		            pushValue = indexPic[indexPic.length - 1] + 1;	
	            }
	            indexPic.push(pushValue);
	            $("#input_fields_wrap").append('<div style="display:flex;"><input class="form-control" style="width:50%;" name="kata[]"/><a href="#" style="height:34px;" class="remove_field btn btn-default"><i class="glyphicon glyphicon-remove"></i></a><br></div>'); //add input box
	        }
	    });
	    
	    $("#input_fields_wrap").on("click",".remove_field", function(e){ 
	    	isi = $(this).parent('div').attr('id');
	    	indexRemove =  indexPic.indexOf(parseInt(isi));
	    	indexPic.splice(indexRemove, 1);
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    });
	}
	else if(idAktivitas == "10"){
		showKalimat(idSiswaChoosen,idRangkaian,idAktivitas);
		var add_button      = $("#add_field_button"); 
		var x = 1; 

	    $(add_button).click(function(e){
	        e.preventDefault();
	        if(x < max_fields){ 
	            x++; 
	             if(jQuery.isEmptyObject(indexPic) == true){
	            	pushValue  = 1;	
	            }
	            else{
	            	lastValue = indexPic.length;
		            pushValue = indexPic[indexPic.length - 1] + 1;	
	            }
	            indexPic.push(pushValue);
	            $("#input_fields_wrap").append('<div style="display:flex;"><input class="form-control" style="width:50%;" name="kata[]"/><a href="#" style="height:34px;" class="remove_field btn btn-default"><i class="glyphicon glyphicon-remove"></i></a><br></div>'); //add input box
	        }
	    });
	    
	    $("#input_fields_wrap").on("click",".remove_field", function(e){ 
	    	isi = $(this).parent('div').attr('id');
	    	indexRemove =  indexPic.indexOf(parseInt(isi));
	    	indexPic.splice(indexRemove, 1);
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    });
	}
	else if(idAktivitas == "11"){
		showGaris(idSiswaChoosen,idRangkaian,idAktivitas);
	}
});

function showHuruf(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();

	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Huruf</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'> " +
				 		 "<select class='form-control' id='mulaiHuruf'>"+
                            "<option value='a'>a</option>"+
                            "<option value='b'>b</option>"+
                            "<option value='c'>c</option>"+
                            "<option value='d'>d</option>"+
                            "<option value='e'>e</option>"+
                            "<option value='f'>f</option>"+
                            "<option value='g'>g</option>"+
                            "<option value='h'>h</option>"+
                            "<option value='i'>i</option>"+
                            "<option value='j'>j</option>"+
                            "<option value='k'>k</option>"+
                            "<option value='l'>l</option>"+
                            "<option value='m'>m</option>"+
                            "<option value='n'>n</option>"+
                            "<option value='o'>o</option>"+
                            "<option value='p'>p</option>"+
                            "<option value='q'>q</option>"+
                            "<option value='r'>r</option>"+
                            "<option value='s'>s</option>"+
                            "<option value='t'>t</option>"+
                            "<option value='u'>u</option>"+
                            "<option value='v'>v</option>"+
                            "<option value='w'>w</option>"+
                            "<option value='x'>x</option>"+
                            "<option value='y'>y</option>"+
                            "<option value='z'>z</option>"+
                         "</select></td>"+
				 "</tr>");
				document.getElementById('mulaiHuruf').value = data.paramAktivitas.mulaiHuruf;

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Akhir Huruf</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'> " +
				 		 "<select class='form-control' id='akhirHuruf'>"+
                            "<option value='a'>a</option>"+
                            "<option value='b'>b</option>"+
                            "<option value='c'>c</option>"+
                            "<option value='d'>d</option>"+
                            "<option value='e'>e</option>"+
                            "<option value='f'>f</option>"+
                            "<option value='g'>g</option>"+
                            "<option value='h'>h</option>"+
                            "<option value='i'>i</option>"+
                            "<option value='j'>j</option>"+
                            "<option value='k'>k</option>"+
                            "<option value='l'>l</option>"+
                            "<option value='m'>m</option>"+
                            "<option value='n'>n</option>"+
                            "<option value='o'>o</option>"+
                            "<option value='p'>p</option>"+
                            "<option value='q'>q</option>"+
                            "<option value='r'>r</option>"+
                            "<option value='s'>s</option>"+
                            "<option value='t'>t</option>"+
                            "<option value='u'>u</option>"+
                            "<option value='v'>v</option>"+
                            "<option value='w'>w</option>"+
                            "<option value='x'>x</option>"+
                            "<option value='y'>y</option>"+
                            "<option value='z'>z</option>"+
                         "</select>"+
				 "</tr>");
				document.getElementById('akhirHuruf').value = data.paramAktivitas.akhirHuruf;

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasHurufBesar(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+ind+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+ind+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");

			  	$("#judulAktivitas").html(data.namaAktivitas);
}

function showHurufBesar(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();

	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Huruf</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'> " +
				 		 "<select class='form-control' id='mulaiHuruf'>"+
                            "<option value='A'>A</option>"+
                            "<option value='B'>B</option>"+
                            "<option value='C'>C</option>"+
                            "<option value='D'>D</option>"+
                            "<option value='E'>E</option>"+
                            "<option value='F'>F</option>"+
                            "<option value='G'>G</option>"+
                            "<option value='H'>H</option>"+
                            "<option value='I'>I</option>"+
                            "<option value='J'>J</option>"+
                            "<option value='K'>K</option>"+
                            "<option value='L'>L</option>"+
                            "<option value='M'>M</option>"+
                            "<option value='N'>N</option>"+
                            "<option value='O'>O</option>"+
                            "<option value='P'>P</option>"+
                            "<option value='Q'>Q</option>"+
                            "<option value='R'>R</option>"+
                            "<option value='S'>S</option>"+
                            "<option value='T'>T</option>"+
                            "<option value='U'>U</option>"+
                            "<option value='V'>V</option>"+
                            "<option value='W'>W</option>"+
                            "<option value='X'>X</option>"+
                            "<option value='Y'>Y</option>"+
                            "<option value='Z'>Z</option>"+
                         "</select></td>"+
				 "</tr>");
				document.getElementById('mulaiHuruf').value = data.paramAktivitas.mulaiHuruf;

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Huruf</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'> " +
				 		 "<select class='form-control' id='akhirHuruf'>"+
                            "<option value='A'>A</option>"+
                            "<option value='B'>B</option>"+
                            "<option value='C'>C</option>"+
                            "<option value='D'>D</option>"+
                            "<option value='E'>E</option>"+
                            "<option value='F'>F</option>"+
                            "<option value='G'>G</option>"+
                            "<option value='H'>H</option>"+
                            "<option value='I'>I</option>"+
                            "<option value='J'>J</option>"+
                            "<option value='K'>K</option>"+
                            "<option value='L'>L</option>"+
                            "<option value='M'>M</option>"+
                            "<option value='N'>N</option>"+
                            "<option value='O'>O</option>"+
                            "<option value='P'>P</option>"+
                            "<option value='Q'>Q</option>"+
                            "<option value='R'>R</option>"+
                            "<option value='S'>S</option>"+
                            "<option value='T'>T</option>"+
                            "<option value='U'>U</option>"+
                            "<option value='V'>V</option>"+
                            "<option value='W'>W</option>"+
                            "<option value='X'>X</option>"+
                            "<option value='Y'>Y</option>"+
                            "<option value='Z'>Z</option>"+
                         "</select>"+
				 "</tr>");
				document.getElementById('akhirHuruf').value = data.paramAktivitas.akhirHuruf;

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasHurufBesar(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+ind+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+ind+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");

			  	$("#judulAktivitas").html(data.namaAktivitas);
}

function showAngka(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();

	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Angka</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'><input type='number' class='form-control' id='mulaiHuruf' value="+data.paramAktivitas.mulaiHuruf+"></td>" + 
				 "</tr>");

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Akhir Angka</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'><input type='number' class='form-control' id='akhirHuruf' value="+data.paramAktivitas.akhirHuruf+"></td>" + 
				 "</tr>");

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasHurufBesar(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+ind+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+ind+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");

			  	$("#judulAktivitas").html(data.namaAktivitas);
}

var nomor;
var pictureSource;   
var destinationType; 
var dbPathsOfImage;

document.addEventListener("deviceready",onDeviceReady,false);

function onDeviceReady() {
    pictureSource=navigator.camera.PictureSourceType;
    destinationType=navigator.camera.DestinationType;
}

function onPhotoDataSuccess(imageData) {
  var smallImage = document.getElementById('smallImage');

  smallImage.style.display = 'block';
  smallImage.src = "data:image/jpeg;base64," + imageData;
}

function onPhotoURISuccess(imageURI) {
  var largeImage = document.getElementById('largeImage'+nomor);

  largeImage.style.display = 'block';
  largeImage.src = imageURI;
  //alamatnya ada di imageURI, jadi yang disimpan adalah imageURI
  var dataGambar = ({
      indeks : nomor,
      path : imageURI
  });
  //cek jika ada nomor itu disini ga di push melainkan direplace
  if(jQuery.isEmptyObject(paths) == false){ //jika path ga kosong? cari ada ga yg sama
  	dbPathsOfImage = SpahQL.db(paths);
  	findSame = dbPathsOfImage.select("/*[/indeks == "+nomor+"]/path");
  	if(jQuery.isEmptyObject(findSame.value()) == false){
  		findSame = dbPathsOfImage.select("/*[/indeks == "+nomor+"]/path").replace(imageURI);
  		paths = dbPathsOfImage.value();
  	}
  	else{paths.push(dataGambar);}
  }
  else{paths.push(dataGambar);}  
  //dan ini untuk remove soalnya harus direfres gitu tombolnya
  $('.tombol').remove();
  alert("show" + dataForImage +  " " + ind);
  show(dataForImage, ind);
}

function capturePhoto() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
    destinationType: destinationType.DATA_URL });
}

function capturePhotoEdit() {
  navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
    destinationType: destinationType.DATA_URL });
}

function getPhoto(i,source) {
  nomor = i;
  navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
    destinationType: destinationType.FILE_URI,
    sourceType: source });
}
function onFail(message) {
  alert('Failed because: ' + message);
}

