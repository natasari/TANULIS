$(document).ready(function(){
	idSiswaChoosen = getUrlVars()["id"];
    daftarEvaluasiSiswa(idSiswaChoosen);

    dataSiswa = getDataSiswa(idSiswaChoosen);
    showNama(dataSiswa);
    var kalimat = "";
});

function showNama(data){
	$("#namaSiswa").html("Evaluasi per Tanggal : <strong>"+data.namaSiswa+"</strong>");
}

function daftarEvaluasiSiswa(siswa){
	var hasilRangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"]/paramResult").values();
	$("#dataSatuSiswaEvaluasi").html("");
	$("#dataSatuSiswaEvaluasi").html(
		"<thead>"+
		"	<tr>"+
		"	<th class='col-xs-1 col-sm-1 col-md-1'>No</th>"+
		"	<th >Tanggal</th>"+
		"	<th >Nilai Aktivitas</th>"+
		"	<th class='text-center'>Rata-rata</th>"+
		"	<th class='text-center'>Grafik</th>"+
		"	</tr>"+
		"</thead>"+
		"<tfoot>"+
		"	<tr>"+
		"	<th >No</th>"+
		"	<th >Tanggal</th>"+
		"	<th >Nilai Aktivitas</th>"+
		"	<th class='text-center'>Rata-rata</th>"+
		"	<th class='text-center'>Grafik</th>"+
		"	</tr>"+
		"</tfoot>"+
		"<tbody>"+
		"</tbody>"
		);
	hasilSemua = [];
	tanggalSemua = [];
	for(i=0; i<hasilRangkaian.length; i++){
		temp = hasilRangkaian[i];
		for(j=0; j<hasilRangkaian[i].length; j++){
			tempHasil = temp[j];
			hasilSemua.push(tempHasil);
			if(jQuery.inArray( tempHasil.tanggal, tanggalSemua ) < 0){
				tanggalSemua.push(tempHasil.tanggal);
			}
		}
	}
	
	var url = 'grafik-tanggal.html?id='+siswa+'&tanggal='
	tanggalSemua.sort();
	for(k in tanggalSemua){
		var tempT = (tanggalSemua[k]);
		$("#dataSatuSiswaEvaluasi tbody").append("<tr>"+
		 	 "	<td style='text-align:center;'>"+ (parseInt(k)+1) +" </td>" +
			 "	<td>"+ tanggalSemua[k] +" </td>" +
			 "  <td><a onclick='hai(\""+tanggalSemua[k]+ "\")' class='btn btn-primary pull-right'><span class='glyphicon glyphicon-edit'></span></a>"+showNamaAktivitas(tanggalSemua[k])+"" +
			 "  </td>"+
			 "  <td>"+ nilaiTanggal(tanggalSemua[k]) + "</td>"+
			 "	<td class='center-block'> <a class='btn btn-primary center-block' onclick='redirectPage(\""+url+ "\",\""+tempT+ "\",\"&halaman=evaluasiTanggal\")'><span class='glyphicon glyphicon-signal' aria-hidden='true'></span> Lihat Grafik</a></td>" + 
			 "  </tr>");
		showAktivitas(tanggalSemua[k], tanggalSemua, siswa, k);
	}	
}

function showAktivitas(tanggal, tanggalSemua, siswa, k){
	var bahasa = "";
	var mat = "";
	var aktivitasPerTanggal = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"][/paramResult/*[/tanggal == '"+tanggal+"']]").values();
	for(j in aktivitasPerTanggal){
		if( aktivitasPerTanggal[j].jenisAktivitas == 'Bahasa'){
			bahasa = bahasa + (aktivitasPerTanggal[j].namaAktivitas + " : "+aktivitasPerTanggal[j].paramAktivitas.level+ " <br>");
		}
		else if(aktivitasPerTanggal[j].jenisAktivitas == 'Matematika'){
			if(aktivitasPerTanggal[j].namaAktivitas = 'Garis dan Bentuk'){
				mat = mat + (aktivitasPerTanggal[j].namaAktivitas + " : -" );
			}else{
				mat = mat + (aktivitasPerTanggal[j].namaAktivitas + " : "+aktivitasPerTanggal[j].paramAktivitas.level+ " <br>");
			}
		}
	}
	$("#listBahasa"+k).append(bahasa);
	$("#listMatematika"+k).append(mat);
}
function nilaiTanggal(tanggal){
	
	nilaiJenis = [];
	nilaiJenisB = [];
    nilaiJenis = rataRata(tanggal);   
    
    prevTanggal = tanggalSebelum(tanggal);
   
    if(prevTanggal == 0){
        nilaiJenisB = [0, 0];
    }
    else{
        nilaiJenisB = rataRata(prevTanggal);
      
        dataShow = [{
            name: tanggal,
            data: nilaiJenis
        }, {
            name: prevTanggal,
            data: nilaiJenisB
        }];
        tgl = [prevTanggal, tanggal];
    }
   
   	//  var kembalian = 'Bahasa : ' + nilaiJenisB[0] + " " + nilaiJenis[0];
    // kembalian = kembalian + ' <br> Matematika : ' + nilaiJenisB[1] + ' ' + nilaiJenis[1];

    var kembalian = 'Bahasa : ' + nilaiJenis[0];
    if(nilaiJenisB[0] < nilaiJenis[0] ){
    	kembalian = kembalian + " <i class='fa fa-chevron-circle-up' style='font-size:1.5em;color:green'></i>";
    }else if(nilaiJenisB[0] > nilaiJenis[0] ){
    	kembalian = kembalian + " <i class='fa fa-chevron-circle-down' style='font-size:1.5em;color:red'></i>";
    }

    kembalian = kembalian + ' <br> Matematika : ' + nilaiJenis[1];
    if(nilaiJenisB[1] < nilaiJenis[1] ){
    	kembalian = kembalian + " <i class='fa fa-chevron-circle-up' style='font-size:1.5em;color:green'></i>";
    }else if(nilaiJenisB[1] > nilaiJenis[1] ){
    	kembalian = kembalian + " <i class='fa fa-chevron-circle-down' style='font-size:1.5em;color:red'></i>";
    }
    // console.log(kembalian);
    return kembalian;
}

function tanggalSebelum(tanggalS){
    var hasilRangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"]/paramResult").values();

    hasilSemua = [];
    tanggalSemua = [];
    for(i=0; i<hasilRangkaian.length; i++){
        temp = hasilRangkaian[i];
        for(j=0; j<hasilRangkaian[i].length; j++){
            tempHasil = temp[j];
            hasilSemua.push(tempHasil);
            if(jQuery.inArray( tempHasil.tanggal, tanggalSemua ) < 0){
                tanggalSemua.push(tempHasil.tanggal);
            }
        }
    }
    tanggalSemua.sort();
    var iTglSekarang = tanggalSemua.indexOf(tanggalS);
    indeksTanggalSebelum = iTglSekarang-1;
    
    if(indeksTanggalSebelum < 0){
        return 0;
    }else{
        return tanggalSemua[indeksTanggalSebelum];    
    }
}

function rataRata(tgl){
    var rangkaianSatuTanggalBahasa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/jenisAktivitas == 'Bahasa'][/paramResult/*[/tanggal == '"+tgl+"']]/paramResult").values();
    var rangkaianSatuTanggalMatematika = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/jenisAktivitas == 'Matematika'][/paramResult/*[/tanggal == '"+tgl+"']]/paramResult").values();

    //rata2 bahasa
    if(jQuery.isEmptyObject(rangkaianSatuTanggalBahasa) == false){
        pembagiBahasa = 0;
        rataBahasa = 0;
        for(k in rangkaianSatuTanggalBahasa){
            tempBahasa = rangkaianSatuTanggalBahasa[k];
            for(j in tempBahasa){
                if(tempBahasa[j].tanggal == tgl){
                    pembagiBahasa++;
                    rataBahasa = rataBahasa + parseInt(tempBahasa[j].nilai);
                }
            }
        }
    rataBahasa = rataBahasa / pembagiBahasa;
    }else{
        rataBahasa = 0;
    }

    //rata2 matematika
    if(jQuery.isEmptyObject(rangkaianSatuTanggalMatematika) == false){
        pembagiMatematika = 0;
        rataMatematika = 0;
        for(k in rangkaianSatuTanggalMatematika){
            tempMatematika = rangkaianSatuTanggalMatematika[k];
            for(j in tempMatematika){
                if(tempMatematika[j].tanggal == tgl){
                	// console.log(tempMatematika[j].nilai);
                    pembagiMatematika++;
                    rataMatematika = rataMatematika + parseInt(tempMatematika[j].nilai);
                }
            }
        }       
        // console.log(rataMatematika);
        // console.log(pembagiMatematika);
        rataMatematika = rataMatematika/pembagiMatematika;

    }
    else{
        rataMatematika = 0;
    }
    return ([rataBahasa, rataMatematika]);
}


function nilaiSetiapAktivitas(tanggalIni){
	var rangkaianSatuTanggalMat = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/paramResult/*[/tanggal == '"+tanggalIni+"']]").values();

	hasilKalimat = "";
	for(i in rangkaianSatuTanggalMat){
		console.log(rangkaianSatuTanggalMat);
		a = rangkaianSatuTanggalMat[i].paramResult;
		hasilKalimat = hasilKalimat + rangkaianSatuTanggalMat[i].namaAktivitas + "<br> ";
		hasilKalimat = hasilKalimat + (looping(a, tanggalIni, rangkaianSatuTanggalMat[i].namaAktivitas));
	}
	return hasilKalimat;
}

function showNamaAktivitas(tanggalIni){
	var rangkaianSatuTanggalMat = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/paramResult/*[/tanggal == '"+tanggalIni+"']]").values();
	hasilKalimat = "";
	for(i in rangkaianSatuTanggalMat){
		console.log(rangkaianSatuTanggalMat);
		hasilKalimat = hasilKalimat + rangkaianSatuTanggalMat[i].namaAktivitas + "<br> ";
	}
	return hasilKalimat;
}

function looping(a, today, namaAktivitas){
	var allHasil="";
	var ind = 0;
	for(j=0; j<a.length; j++){
		if(a[j].tanggal == today){
			if(ind == 0 ){
				ind = 1;
			}
			z= a[j];
			for (var property in z){
				if(property == 'parameterYangDinilai'){
					if(namaAktivitas == 'Garis dan Bentuk'){
						allHasil = allHasil + ("<b>Soal" +": <img src='" + z[property] + "'/></b> " );
					}else{
						allHasil = allHasil + ("<b>Soal" +": " + z[property] + "</b> " );
					}
				}
				else if(property != 'tanggal'){
					allHasil = allHasil + (property +": " + z[property] + " " );
				}
			}
			allHasil = allHasil + "<br>";
		}
	}
	return allHasil;
}

function hai(tgl){
	var kembalian = nilaiSetiapAktivitas(tgl);
	console.log(tgl);
	bootbox.dialog({
            message: kembalian,
            title: "Detail",
            buttons: {
              success: {
                label: "OK",
                className: "btn-success",
                callback: function() {
                  	
                }
              }
            }
          }); 
}