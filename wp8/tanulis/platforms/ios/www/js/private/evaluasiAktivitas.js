$(document).ready(function(){
	idSiswaChoosen = getUrlVars()["id"];
	daftarEvaluasiSiswa(idSiswaChoosen);

    dataSiswa = getDataSiswa(idSiswaChoosen);
    showNama(dataSiswa, idSiswaChoosen);
    console.log(tbRangkaianAktivitas);
});

function showNama(data, siswa){
	$("#namaSiswa").html("Evaluasi per Aktivitas: <strong>"+data.namaSiswa+"</strong>");
}

function daftarEvaluasiSiswa(siswa){
	var hasilRangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"]").values();
	console.log(hasilRangkaian);
	$("#dataSatuSiswaEvaluasi").html("");
	$("#dataSatuSiswaEvaluasi").html(
		"<thead>"+
		"	<tr>"+
		"	<th class='text-center col-xs-1 col-sm-1 col-md-1'>No</th>"+
		"	<th >Aktivitas</th>"+
		"	<th >Nilai</th>"+
		"	<th class='text-center col-xs-2 col-sm-2 col-md-2'>Grafik</th>"+
		"	</tr>"+
		"</thead>"+
		"<tfoot>"+
		"	<tr>"+
		"	<th >No</th>"+
		"	<th >Aktivitas</th>"+
		"	<th >Nilai</th>"+
		"	<th class='text-center'>Grafik</th>"+
		"	</tr>"+
		"</tfoot>"+
		"<tbody>"+
		"</tbody>"
		);
	
	for(k in hasilRangkaian){
	namaAk = hasilRangkaian[k].namaAktivitas;
	idAk = hasilRangkaian[k].idAktivitas;
	console.log(idAk);
	var url = 'grafik-aktivitas.html?id='+siswa+'&rangkaian='+hasilRangkaian[k].idRangkaian+'&aktivitas=';
		$("#dataSatuSiswaEvaluasi tbody").append("<tr>"+
		 	 "	<td>"+ (parseInt(k)+1) +" </td>" +
			 "	<td>"+ hasilRangkaian[k].namaAktivitas +" </td>" + 
			 "	<td>"+ ratarata(namaAk) +"</td>" + 
			 "	<td class='center-block'> <a class='btn btn-primary center-block' onclick='redirectPage(\""+url+ "\",\""+idAk+ "\",\"&halaman=evaluasiAktivitas\")'><span class='glyphicon glyphicon-signal' aria-hidden='true'></span> Lihat Grafik</a></td>" + 
			 "</tr>");
	}
}

function ratarata(namaAktivitas){
	var rangkaianSatuJenisAktivitas = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/namaAktivitas == '"+namaAktivitas+"']/paramResult").values();
	//ambiltanggal
	tanggalSemua = [];
	nilai=[];
	for(i in rangkaianSatuJenisAktivitas){
		temp = rangkaianSatuJenisAktivitas[i];
		for(j in temp){
			if(jQuery.inArray( temp[j].tanggal, tanggalSemua ) < 0){
					tanggalSemua.push(temp[j].tanggal);
			}
			nilai.push({
				"tanggal":temp[j].tanggal,
				"nilai": temp[j].nilai
				});
		}
	}

    console.log(tanggalSemua.sort());
	nilaiTanggal = [];
	nilaiAja = [];
	for(k in tanggalSemua){
		var count = 0;
		var sum = 0;
		for(l in nilai){
			if(tanggalSemua[k] == nilai[l].tanggal){
				sum = sum + parseInt(nilai[l].nilai);
				count = count + 1;
			}
		}
		nilaiAja.push(sum/count);
		rata = (sum/count);
	}
	return rata;
}
