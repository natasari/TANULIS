$(document).ready(function(){
	idSiswaChoosen = getUrlVars()["id"];
    dataSiswa = getDataSiswa(idSiswaChoosen);
    tanggal = getUrlVars()["tanggal"];
    showNama(dataSiswa, idSiswaChoosen);
});

function showNama(data, siswa, namaAktivitas){
	$("#namaSiswa").html("<a href='evaluasi-per-tanggal.html?id="+siswa+"' >Evaluasi Aktivitas : <strong>"+data.namaSiswa+"</strong></a>");
}

$(function () {
    nilaiJenis = [];
    nilaiJenis = rataRata(tanggal);   
    
    prevTanggal = tanggalSebelum();

    if(prevTanggal == 0){
        dataShow = [{
            name: tanggal,
            data: nilaiJenis
        }];
        tgl = [tanggal];
    }
    else{
        nilaiJenisB = [];
        nilaiJenisB = rataRata(prevTanggal);
      
        dataShow = [{
            name: tanggal,
            data: nilaiJenis
        }, {
            name: prevTanggal,
            data: nilaiJenisB
        }];
        tgl = [prevTanggal, tanggal];
    }

    jenisSemua = ["Bahasa","Matematika"];
     $('#container').highcharts({
        title: {
            text: 'Perkembangan Kemampuan Siswa'
        },
        subtitle: {
            text: 'Menurut Jenis Aktivitas'
        },
        xAxis: {
            categories: ['Bahasa', 'Matematika']
        },
        yAxis: {
            title: {
                text: 'Nilai'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: dataShow
    });
});

function tanggalSebelum(){
    var hasilRangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"]/paramResult").values();

    hasilSemua = [];
    tanggalSemua = [];
    for(i=0; i<hasilRangkaian.length; i++){
        temp = hasilRangkaian[i];
        for(j=0; j<hasilRangkaian[i].length; j++){
            tempHasil = temp[j];
            hasilSemua.push(tempHasil);
            if(jQuery.inArray( tempHasil.tanggal, tanggalSemua ) < 0){
                tanggalSemua.push(tempHasil.tanggal);
            }
        }
    }
    tanggalSemua.sort();
    var iTglSekarang = tanggalSemua.indexOf(tanggal);
    indeksTanggalSebelum = iTglSekarang-1;
    
    if(indeksTanggalSebelum < 0){
        return 0;
    }else{
        return tanggalSemua[indeksTanggalSebelum];    
    }
}

function rataRata(tgl){
    var rangkaianSatuTanggalBahasa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/jenisAktivitas == 'Bahasa'][/paramResult/*[/tanggal == '"+tgl+"']]/paramResult").values();
    var rangkaianSatuTanggalMatematika = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/jenisAktivitas == 'Matematika'][/paramResult/*[/tanggal == '"+tgl+"']]/paramResult").values();

    //rata2 bahasa
    
    if(jQuery.isEmptyObject(rangkaianSatuTanggalBahasa) == false){
        pembagiBahasa = 0;
        rataBahasa = 0;
        for(k in rangkaianSatuTanggalBahasa){
            tempBahasa = rangkaianSatuTanggalBahasa[k];
            for(j in tempBahasa){
                if(tempBahasa[j].tanggal == tgl){
                    pembagiBahasa++;
                    rataBahasa = rataBahasa + parseInt(tempBahasa[j].nilai);
                }
            }
        }
    rataBahasa = rataBahasa / pembagiBahasa;
    }else{
        rataBahasa = 0;
    }

    //rata2 matematika
    if(jQuery.isEmptyObject(rangkaianSatuTanggalMatematika) == false){
        pembagiMatematika = 0;
        rataMatematika = 0;
        for(k in rangkaianSatuTanggalMatematika){
            tempMatematika = rangkaianSatuTanggalMatematika[k];
            for(j in tempMatematika){
                if(tempMatematika[j].tanggal == tgl){
                    pembagiMatematika++;
                    rataMatematika = rataMatematika + parseInt(tempMatematika[j].nilai);
                }
            }
        }       
        rataMatematika = rataMatematika/pembagiMatematika;
    }
    else{
        rataMatematika = 0;
    }

    return ([rataBahasa, rataMatematika]);
}