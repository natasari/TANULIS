var letter = "";
var startLetter = "";
var endLetter = "";
var fontType = "";
var fontSize = null;
var level = "";
var startASCII = null;
var endASCII = null;
var arrayLetter = new Array();
var index = 0;
var idSiswaChoosen;
var idRangkaian;
var data;
var dataRangkaianSatuSiswa;
var hasilPresentasi;
var path, n;
var tandaDraw = 0;
var h;
var w;
var height, nom, jml;

$(document).ready(function(){
  h = $(window).height()-45;
  w = $(window).width();
  $(".footer").css({ top: $(window).height()-45});
  idSiswaChoosen = getUrlVars()["id"];
  
  idRangkaian = getUrlVars()["idRangkaian"];
  nom = parseInt(getUrlVars()["nom"])+1;
  jml = getUrlVars()["jml"];
  dataRangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/isActive == 'Aktif'][/idRangkaian =="+parseInt(idRangkaian)+"]");
  data = dataRangkaianSatuSiswa.value();
  path = dataRangkaianSatuSiswa.path();
  $("#namaAktivitas").html("<b>Menulis "+data.namaAktivitas+"</b>");
  for(var i=0; i<path.length;i++) {
      if (path[i] === "/") slashPosition = i;
  }
  ind = path.substr(slashPosition+1, path.length);
  
  idAktivitas = data.idAktivitas;

  // fontType menentukan level nya
  if(data.paramAktivitas.level == "Mudah"){
    fontType = "easyFont";
  }
  else if(data.paramAktivitas.level == "Sedang"){
    fontType = "mediumFont";
  }
  else{
    level = "Sulit";
    fontType = "calibri";
  }
  document.getElementById("load").style.fontFamily = fontType;

  /*looping kata*/
  kata = data.paramAktivitas.kata;
  pathGambar = data.paramAktivitas.path;
  settingUp();
});

var ctx, ctxHuruf, color="rgb(0, 0, 5)";

function settingUp(){
  newCanvas(kata[index]);
  ctx.strokeStyle = '#000';
  $(".palette").click(function(){
    ctx.globalCompositeOperation = 'source-over';
    $(".palette").css("border-color", "#777");
    $(".palette").css("border-style", "solid");
    $(this).css("border-color", "#fff");
    $(this).css("border-style", "dashed");
    color = $(this).css("background-color");
    ctx.beginPath();
    ctx.strokeStyle = color;
  });
  $("#hps").click(function(){
    ctx.globalCompositeOperation = 'destination-out';
  });
  $("#reload").click(function(){
    newCanvas(kata[index]);
  });
  $("#urutanAktivitas").html("Huruf : <b>"+kata[index]+"</b>/"+kata[(kata.length-1)]+"");
  $("#urutanPerAktivitas").html("&nbsp&nbsp&nbspAktivitas ke : <b>"+nom+"</b>/"+jml+"");

  if(idSiswaChoosen == "999"){
    $("#next").remove();
    $("#urutanPerAktivitas").remove();
  }
  else{
    $("#back").remove();
  }
}

function newCanvas(letter){
  height = h/5;
  $("#headera").height(height);

  var canvas = document.getElementById("canvas");
  ctx=canvas.getContext("2d");
  canvas.height = h-height;
  canvas.width = w;

  $("#canvas").css({
    'margin-top': (height) + 'px'
  });

  ctx.strokeStyle = color;
  ctx.lineWidth = 10;
  ctx.lineCap = 'round';

  window.onload = function start() {
    canvasHuruf = document.getElementById("canvasHuruf");
    $("#canvasHuruf").css({
      'margin-top': (height) + 'px'
    });

    ctxHuruf = canvasHuruf.getContext('2d');
        canvasHuruf.height = h - height;
        canvasHuruf.width = w;
        tandaDraw = 1;
        text_handler();
  }
    
    function text_handler() {
      fontSize = canvasHuruf.width;
        ctxHuruf.font="normal " + '20vw' + " " + fontType;//font size and then font family.
        ctxHuruf.fillStyle = 'rgb(0, 150, 136)'; //color to be seen on a black canvas, default is black text
        ctxHuruf.textBaseline = 'middle';
          if(level != "Sulit"){
          drawletter(letter);    
          }
    }
    
    if(tandaDraw == 1 && level != "Sulit"){
      ctxHuruf.clearRect(0, 0, canvasHuruf.width, canvasHuruf.height);
      drawletter(letter);
    }

  //setup for drawing
  $("#canvas").drawTouch();
  $("#canvas").drawPointer();
  $("#canvas").drawMouse();
}

function drawletter(letter) {
  var minus = ctxHuruf.measureText(letter).width / 2;
  var centerx = ($(window).width()/ 2)-minus;
  var centery = canvasHuruf.height/2;
  ctxHuruf.fillText(letter, centerx, centery);
  drawClock(letter);
};

$.fn.drawTouch = function(){
  var start = function(e){
    e = e.originalEvent;
    ctx.beginPath();
    x = e.changedTouches[0].pageX;
    y = e.changedTouches[0].pageY-height;
    ctx.moveTo(x,y);
  };
  var move= function(e){
    e.preventDefault();
    e = e.originalEvent;
    x = e.changedTouches[0].pageX;
    y = e.changedTouches[0].pageY-height;
    ctx.lineTo(x,y);
    ctx.stroke();
  };
  var end=function(e){
  }
  $(this).on("touchstart",start);
  $(this).on("touchmove",move);
  $(this).on("touchend",end);
};

$.fn.drawMouse = function() {
  var clicked = 0;
  var start = function(e) {
    clicked = 1;
    ctx.beginPath();
    x = e.pageX;
    y = e.pageY-height;
    ctx.moveTo(x,y);
  };
  var move = function(e) {
    if(clicked){
      x = e.pageX;
      y = e.pageY-height;
      ctx.lineTo(x,y);
      ctx.stroke();
    }
  };
  var stop = function(e) {
    clicked = 0;
  };
  $(this).on("mousedown", start);
  $(this).on("mousemove", move);
  $(this).on("mouseup", stop);
};

// prototype to start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
$.fn.drawPointer = function() {
  var start = function(e) {
        e = e.originalEvent;
    ctx.beginPath();
    x = e.pageX;
    y = e.pageY-height;
    ctx.moveTo(x,y);
  };
  var move = function(e) {
    e.preventDefault();
        e = e.originalEvent;
    x = e.pageX;
    y = e.pageY-height;
    ctx.lineTo(x,y);
    ctx.stroke();
    };
  $(this).on("MSPointerDown", start);
  $(this).on("MSPointerMove", move);
};  

function simpanGambar(id){
  var pathHuruf = canvas.toDataURL();
  var pathTulis = canvasHuruf.toDataURL();
  localStorage.removeItem("tempAktivitas");
  
  index++;
  if(index == kata.length){
    var tempData = ({
      idSiswa : parseInt(idSiswaChoosen),
      idAktivitas  : parseInt(idAktivitas),
      idRangkaian    : idRangkaian,
      indeks : ind,
      letter : kata[index-1],
      pathHuruf : pathHuruf,
      pathTulis : pathTulis,
      level : data.paramAktivitas.level
    });
    tempAktivitas.temp.push(tempData);
    localStorage.setItem("tempAktivitas", JSON.stringify(tempAktivitas));
    window.location.href = "../halaman-mulai-aktivitas.html?id=" + idSiswaChoosen;
  }else{
    var tempData = ({
      idSiswa : parseInt(idSiswaChoosen),
      idAktivitas  : parseInt(idAktivitas),
      idRangkaian    : idRangkaian,
      indeks : ind,
      letter : kata[index-1],
      pathHuruf : pathHuruf,
      pathTulis : pathTulis,
      level : data.paramAktivitas.level
    });
    console.log(tempAktivitas);
    tempAktivitas.temp.push(tempData);
    localStorage.setItem("tempAktivitas", JSON.stringify(tempAktivitas));
    settingUp();
  }
}


$(document).on('click', "#next", function () {
  simpanGambar();
});
var canvasJam = document.getElementById("jam");
var ctxJam = canvasJam.getContext("2d");
var radius = canvasJam.height / 2;
canvasJam.height = $(window).height()/5;;
canvasJam.width = $(window).height()/5;
ctxJam.translate(radius, radius);
radius = radius * 0.7;

function drawClock(letter) {
  drawFace(ctxJam, radius);
  drawNumbers(ctxJam, radius);
  drawTime(ctxJam, radius, letter);
}

function drawFace(ctxJam, radius) {
  var grad;
  ctxJam.beginPath();
  ctxJam.arc(0, 0, radius, 0, 2*Math.PI);
  ctxJam.fillStyle = 'white';
  ctxJam.fill();
  grad = ctxJam.createRadialGradient(0,0,radius*0.95, 0,0,radius*1.05);
  grad.addColorStop(0, '#333');
  grad.addColorStop(0.5, 'white');
  grad.addColorStop(1, '#333');
  ctxJam.strokeStyle = grad;
  ctxJam.lineWidth = radius*0.1;
  ctxJam.stroke();
  ctxJam.beginPath();
  ctxJam.arc(0, 0, radius*0.1, 0, 2*Math.PI);
  ctxJam.fillStyle = '#333';
  ctxJam.fill();
}

function drawNumbers(ctxJam, radius) {
  var ang;
  var num;
  ctxJam.font = radius*0.15 + "px arial";
  ctxJam.textBaseline="middle";
  ctxJam.textAlign="center";
  for(num = 1; num < 13; num++){
    ang = num * Math.PI / 6;
    ctxJam.rotate(ang);
    ctxJam.translate(0, -radius*0.85);
    ctxJam.rotate(-ang);
    ctxJam.fillText(num.toString(), 0, 0);
    ctxJam.rotate(ang);
    ctxJam.translate(0, radius*0.85);
    ctxJam.rotate(-ang);
  }
}

function drawTime(ctxJam, radius, letter){
    var now = new Date();
    console.log(letter);
    var hour = letter.substr(1, 1);
    var minute = letter.substr(3, 4);
    var second = now.getSeconds();
    //hour
    hour=hour%12;
    hour=(hour*Math.PI/6)+
    (minute*Math.PI/(6*60))+
    (second*Math.PI/(360*60));
    drawHand(ctxJam, hour, radius*0.5, radius*0.07);
    //minute
    minute=(minute*Math.PI/30)+(second*Math.PI/(30*60));
    drawHand(ctxJam, minute, radius*0.8, radius*0.07);
}

function drawHand(ctxJam, pos, length, width) {
    ctxJam.beginPath();
    ctxJam.lineWidth = width;
    ctxJam.lineCap = "round";
    ctxJam.moveTo(0,0);
    ctxJam.rotate(pos);
    ctxJam.lineTo(0, -length);
    ctxJam.stroke();
    ctxJam.rotate(-pos);
}