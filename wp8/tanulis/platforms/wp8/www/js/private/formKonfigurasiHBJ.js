function showHari(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();
	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Hari</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 	"<select class='form-control' id='mulaiHuruf'>"+
                            "<option value='senin'>senin</option>"+
                            "<option value='selasa'>selasa</option>"+
                            "<option value='rabu'>rabu</option>"+
                            "<option value='kamis'>kamis</option>"+
                            "<option value='jumat'>jumat</option>"+
                            "<option value='sabtu'>sabtu</option>"+
                            "<option value='minggu'>minggu</option>"+
                         "</select></td>"+
				 "</tr>");
			  	document.getElementById('mulaiHuruf').value = data.paramAktivitas.mulai;
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Akhir Hari</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' id='akhirHuruf'>"+
                            "<option value='senin'>senin</option>"+
                            "<option value='selasa'>selasa</option>"+
                            "<option value='rabu'>rabu</option>"+
                            "<option value='kamis'>kamis</option>"+
                            "<option value='jumat'>jumat</option>"+
                            "<option value='sabtu'>sabtu</option>"+
                            "<option value='minggu'>minggu</option>"+
                         "</select></td>"+
				 "</tr>");
			  	document.getElementById('akhirHuruf').value = data.paramAktivitas.akhir;

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}


			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasHari(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+ind+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+ind+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");

			  	$("#judulAktivitas").html(data.namaAktivitas);
}

function showBulan(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();
	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);	
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Hari</label></td>" +
				 "	<td>:</td>" + 
				  "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 	"<select class='form-control' id='mulaiHuruf'>"+
                            "<option value='januari'>januari</option>"+
                            "<option value='februari'>februari</option>"+
                            "<option value='maret'>maret</option>"+
                            "<option value='april'>april</option>"+
                            "<option value='mei'>mei</option>"+
                            "<option value='juni'>juni</option>"+
                            "<option value='juli'>juli</option>"+
                            "<option value='agustus'>agustus</option>"+
                            "<option value='september'>september</option>"+
                            "<option value='oktober'>oktober</option>"+
                            "<option value='november'>november</option>"+
                            "<option value='desember'>desember</option>"+
                         "</select></td>"+
				 "</tr>");
			  	document.getElementById('mulaiHuruf').value = data.paramAktivitas.mulai;
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Akhir Hari</label></td>" +
				 "	<td>:</td>" + 
				  "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 	"<select class='form-control' id='akhirHuruf'>"+
                            "<option value='januari'>januari</option>"+
                            "<option value='februari'>februari</option>"+
                            "<option value='maret'>maret</option>"+
                            "<option value='april'>april</option>"+
                            "<option value='mei'>mei</option>"+
                            "<option value='juni'>juni</option>"+
                            "<option value='juli'>juli</option>"+
                            "<option value='agustus'>agustus</option>"+
                            "<option value='september'>september</option>"+
                            "<option value='oktober'>oktober</option>"+
                            "<option value='november'>november</option>"+
                            "<option value='desember'>desember</option>"+
                         "</select></td>"+
				 "</tr>");
			  	document.getElementById('akhirHuruf').value = data.paramAktivitas.akhir;
			  	
			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}


			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasHari(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+ind+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+ind+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");

			  	$("#judulAktivitas").html(data.namaAktivitas);
}

function showJam(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();
	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	for(var i=0; i<path.length;i++) {
	    if (path[i] === "/") slashPosition = i;
	}
	var ind = path.substr(slashPosition+1, path.length);
	dataForImage = data;
	nForImage = n;
	pathForImage = path;

	paths = data.paramAktivitas.path;
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Kata</label></td>" +
				 "	<td>:</td>" + 				 		
				 "  <td></td>" +
				 "	</tr>"+
				 "	<tr>"+
				 "	<td colspan='3'>*Masukkan jam : Contoh <strong> 00:50</td>"+
				 "</tr>");

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "	<td colspan='4' id='input_fields_wrap'>"+
				 		"<div id="+0+" style='display:flex;'>" +
				 		"<input placeholder='tulis jam' class='form-control' name='kata[]' id='kata' style='width:50%;' value='"+data.paramAktivitas.kata[0]+"'>"+
				 		"<button type='submit' style='height:34px;' class='btn btn-default addButton' id='add_field_button'>"+"<i class='glyphicon glyphicon-plus'></i></button>"+
				 		"</div>"+
				 "</td>" + 
				 "</tr>");
			  	
			  	//showing tombol
			  	show(data, ind);
			  	//jika data tersimpan
			  	if(jQuery.isEmptyObject(data.paramAktivitas.path) == false){
			  		document.getElementById('largeImage0').src = data.paramAktivitas.path[0].path;
			  	}

			  	nData = data.paramAktivitas.kata.length;
			  	max_fields  = 6 - nData;
			  
			  	for(i=1; i<nData; i++){
					$("#input_fields_wrap").append('<div style="display:flex;"><input class="form-control" style="width:50%;" name="kata[]" value='+data.paramAktivitas.kata[i]+'><a style="height:34px;" href="#" class="remove_field btn btn-default">'+
					'<i class="glyphicon glyphicon-remove"></i></a>'); //add input box
			  	}

			  	$("#judulAktivitas").html(data.namaAktivitas);
}