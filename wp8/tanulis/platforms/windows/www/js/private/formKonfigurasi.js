var max_fields      = 1; //maximum input boxes allowed
$(document).ready(function(){
	var str = "My bologna has a first name";

	var shuffled = str.split('').sort(function(){return 0.5-Math.random()}).join('');
	console.log(shuffled);

	var idSiswaChoosen = getUrlVars()["id"];
	var idRangkaian = getUrlVars()["rangkaian"];
	var idAktivitas = getUrlVars()["aktivitas"];
	
	if(idAktivitas == "1" || idAktivitas == "2"){
		showHuruf(idSiswaChoosen,idRangkaian,idAktivitas);
	}
	else if(idAktivitas == "3"){
		showAngka(idSiswaChoosen,idRangkaian,idAktivitas);
	}
	else if(idAktivitas == "4" || idAktivitas == "5"){
		showSukuKata(idSiswaChoosen,idRangkaian,idAktivitas);

		//ini untuk code penambahan row pada tabel
	    var add_button      = $("#add_field_button"); //Add button ID
		var x = 1; //initlal text box count
		 
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment"<input class='form-control' name='kata[]' id='kata'/>"+
	            $("#input_fields_wrap").append('<div style="display:flex;"><input class="form-control" style="width:50%;" name="kata[]"/><a href="#" class="remove_field btn btn-default"><i class="glyphicon glyphicon-remove"></i></a></div>'); //add input box
	        }
	    });
	    
	    $("#input_fields_wrap").on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').remove(); x--;
	    });
	}
});


function showHuruf(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();

	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Huruf</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'><input type='text' class='form-control' id='mulaiHuruf' value="+data.paramAktivitas.mulaiHuruf+"></td>" + 
				 "</tr>");

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Akhir Huruf</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'><input type='text' class='form-control' id='akhirHuruf' value="+data.paramAktivitas.akhirHuruf+"></td>" + 
				 "</tr>");

			  	if(data.paramAktivitas.jenisTulisan == 'Arial'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Jenis Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='jenisTulisan' id='jenisTulisan'>"+
                            "<option value='Arial' selected>Arial</option>"+
                            "<option value='Times New Roman'>Times New Roman</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Jenis Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='jenisTulisan' id='jenisTulisan'>"+
                            "<option value='Arial'>Arial</option>"+
                            "<option value='Times New Roman' selected>Times New Roman</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasHurufBesar(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+path[n-1]+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+path[n-1]+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");

			  	$("#judulAktivitas").html(data.namaAktivitas);
}


function showAngka(siswa, rangkaian, aktivitas){
	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();

	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Mulai Angka</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'><input type='text' class='form-control' id='mulaiHuruf' value="+data.paramAktivitas.mulaiHuruf+"></td>" + 
				 "</tr>");

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Akhir Angka</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'><input type='text' class='form-control' id='akhirHuruf' value="+data.paramAktivitas.akhirHuruf+"></td>" + 
				 "</tr>");

			  	if(data.paramAktivitas.jenisTulisan == 'Arial'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Jenis Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='jenisTulisan' id='jenisTulisan'>"+
                            "<option value='Arial' selected>Arial</option>"+
                            "<option value='Times New Roman'>Times New Roman</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Jenis Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='jenisTulisan' id='jenisTulisan'>"+
                            "<option value='Arial'>Arial</option>"+
                            "<option value='Times New Roman' selected>Times New Roman</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasHurufBesar(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+path[n-1]+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+path[n-1]+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");

			  	$("#judulAktivitas").html(data.namaAktivitas);
}

function showSukuKata(siswa, rangkaian, aktivitas){

	/*ambil data rangkaian*/
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	data = dataLengkap.value();
	var n = dataLengkap.path().length;
	var path = dataLengkap.path();
	
	$("#tabelKonfigurasi").html("");
			$("#tabelKonfigurasi").html(
				"<tbody>"+
				"</tbody>"
				);

			  	if(data.paramAktivitas.jenisTulisan == 'Arial'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Jenis Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='jenisTulisan' id='jenisTulisan'>"+
                            "<option value='Arial' selected>Arial</option>"+
                            "<option value='Times New Roman'>Times New Roman</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Jenis Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='jenisTulisan' id='jenisTulisan'>"+
                            "<option value='Arial'>Arial</option>"+
                            "<option value='Times New Roman' selected>Times New Roman</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.ukuranTulisan == 'Kecil'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil' selected>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.ukuranTulisan == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Besar'>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Ukuran Tulisan</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='ukuranTulisan' id='ukuranTulisan'>"+
                            "<option value='Kecil'>Kecil</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Besar' selected>Besar</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.paramAktivitas.level == 'Mudah'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah' selected>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else if(data.paramAktivitas.level == 'Sedang'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang' selected>Sedang</option>"+
                            "<option value='Sulit'>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Level</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='level' id='level'>"+
                            "<option value='Mudah'>Mudah</option>"+
                            "<option value='Sedang'>Sedang</option>"+
                            "<option value='Sulit' selected>Sulit</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	if(data.isActive == 'Aktif' || data.isActive == 'Belum dikonfigurasi'){
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif' selected>Aktif</option>"+
                            "<option value='Tidak Aktif'>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}
			  	else{
			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Status</label></td>" +
				 "	<td>:</td>" + 
				 "	<td class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<select class='form-control' name='isActive' id='isActive'>"+
                            "<option value='Aktif'>Aktif</option>"+
                            "<option value='Tidak Aktif' selected>Tidak Aktif</option>"+
                         "</select>"+
				 "</td>" + 
				 "</tr>");	
			  	}

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
			 	 "	<td class='col-xs-4 col-sm-4 col-md-4'><label>Kata</label></td>" +
				 "	<td>:</td>" + 				 		
				 "  <td></td>" +
				 "</tr>");

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "	<td colspan='4' id='input_fields_wrap'>"+
				 		"<div style='display:flex;'>" +
				 		"<input class='form-control' name='kata[]' id='kata' style='width:50%;' value='"+data.paramAktivitas.kata[0]+"'>"+
				 		"<button type='button' class='btn btn-default addButton' id='add_field_button'>"+"<i class='glyphicon glyphicon-plus'></i></button>"+
				 		"<button onclick='getPhoto(pictureSource.PHOTOLIBRARY);'>Pilih Foto</button><br>"+
				 		"</div>"+
				 "</td>" + 
				 "</tr>");
			  	

			  	$("#tabelKonfigurasi tbody").append("<tr>"+
				 "<td colspan = '4' class='col-xs-8 col-sm-8 col-md-8'>"+
				 		"<button type='submit' onclick='editRangkaianAktivitasSukuKata(\""+data.idSiswa+ "\",\""+data.idAktivitas+ "\",\""+data.idRangkaian+ "\",\""+path[n-1]+ "\","+JSON.stringify(data)+")' class='btn btn-success teal pull-right'><span class='glyphicon glyphicon-floppy-saved' aria-hidden='true'></span> Simpan </button>" +
				 		"<button type='submit' onclick='deleteRangkaian(\""+data.idSiswa+ "\",\""+path[n-1]+ "\")' class='btn btn-danger pull-right' style='margin-right:6px;'><span class='glyphicon glyphicon-remove' aria-hidden='true'></span> Hapus Rangkaian</button>"+
				 "</td>" + 
				 "</tr>");


			  	nData = data.paramAktivitas.kata.length;
			  	console.log(nData);
			  	for(i=1; i<nData; i++){
					$("#input_fields_wrap").append('<div style="display:flex;"><input class="form-control" style="width:50%;" name="kata[]" value='+data.paramAktivitas.kata[i]+'><a href="#" class="remove_field btn btn-default">'+
					'<i class="glyphicon glyphicon-remove"></i></a></div>'); //add input box
			  	}

			  	$("#judulAktivitas").html(data.namaAktivitas);
}


var pictureSource;   // picture source
    var destinationType; // sets the format of returned value

    // Wait for device API libraries to load
    document.addEventListener("deviceready",onDeviceReady,false);

    // device APIs are available
    function onDeviceReady() {
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
    }

    // Called when a photo is successfully retrieved
    function onPhotoDataSuccess(imageData) {
      // Uncomment to view the base64-encoded image data
      // console.log(imageData);

      // Get image handle
      var smallImage = document.getElementById('smallImage');

      // Unhide image elements
      smallImage.style.display = 'block';

      // Show the captured photo
      // The in-line CSS rules are used to resize the image
      smallImage.src = "data:image/jpeg;base64," + imageData;
    }

    // Called when a photo is successfully retrieved
    function onPhotoURISuccess(imageURI) {
      // Uncomment to view the image file URI
      // console.log(imageURI);

      // Get image handle
      var largeImage = document.getElementById('largeImage');

      // Unhide image elements
      largeImage.style.display = 'block';

      // Show the captured photo
      // The in-line CSS rules are used to resize the image
      alert(imageURI);
      largeImage.src = imageURI;
    }

    // A button will call this function
    function capturePhoto() {
      // Take picture using device camera and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 50,
        destinationType: destinationType.DATA_URL });
    }

    // A button will call this function
    function capturePhotoEdit() {
      // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoDataSuccess, onFail, { quality: 20, allowEdit: true,
        destinationType: destinationType.DATA_URL });
    }

    // A button will call this function
    function getPhoto(source) {
    	alert("isi");
      // Retrieve image file location from specified source
      navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50,
        destinationType: destinationType.FILE_URI,
        sourceType: source });
    }

    // Called if something bad happens.
    function onFail(message) {
      alert('Failed because: ' + message);
    }
