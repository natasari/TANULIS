var letter = "";
var startLetter = "";
var endLetter = "";
var fontType = "";
var fontSize = null;
var level = "";
var startASCII = null;
var endASCII = null;
var arrayLetter = new Array();

var index = 0;
var idSiswaChoosen;
var idRangkaian;
var data;
var dataRangkaianSatuSiswa;
var hasilPresentasi;

// var canvasGambarTulisan = document.createElement('canvas');
// var contextGambarTulisan = canvasGambarTulisan.getContext('2d');

// var canvasGambarHuruf = document.createElement('canvas');
// var contextGambarHuruf = canvasGambarHuruf.getContext('2d');
var path, n;
/*ASCII*/
//var chr = String.fromCharCode(97 + n);
$(document).ready(function(){
	$(".footer").css({ top: $(window).height()-45});
	/*dapat aktivitas yang dikirimkan*/
	idSiswaChoosen = getUrlVars()["id"];
	idRangkaian = getUrlVars()["idRangkaian"];

	dataRangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/isActive == 'Aktif'][/idRangkaian =="+parseInt(idRangkaian)+"]");
	data = dataRangkaianSatuSiswa.value();
	console.log(data);
	path = dataRangkaianSatuSiswa.path();
	n = path.length;
	
	idAktivitas = data.idAktivitas;

	fontType = 'Calibri';

	if(data.paramAktivitas.ukuranTulisan == "Kecil"){
		fontSize = "225px";
	}
	else if(data.paramAktivitas.ukuranTulisan == "Sedang"){
		fontSize = "250px";
	}
	else if(data.paramAktivitas.ukuranTulisan == "Besar"){
		fontSize = "300px";
	}

	/*looping kata*/
	kata = data.paramAktivitas.kata;
	settingUp();
});

var ctx, ctxHuruf, color="rgb(0, 0, 5)";
var pixels = null;
var letterpixels = null;

function settingUp(){
	newCanvas(kata[index]);
	//setTimeout(function(){}, 1000);
	$(".palette").click(function(){
		$(".palette").css("border-color", "#777");
		$(".palette").css("border-style", "solid");
		$(this).css("border-color", "#fff");
		$(this).css("border-style", "dashed");
		color = $(this).css("background-color");
		ctx.beginPath();
		ctx.strokeStyle = color;
	});
}

function newCanvas(letter){
	//size of canvas	
	//setup canvas
	var canvas = document.getElementById("canvas");
	canvas.width = $(window).width();
	canvas.height = $(window).height()-45;
	ctx=canvas.getContext("2d");

	ctx.strokeStyle = color;
	ctx.lineWidth = 21;
	ctx.lineCap = 'round';

  	//setup ctxHuruf
  	var canvasHuruf = document.getElementById("canvasHuruf");
	canvasHuruf.width = $(window).width();
	canvasHuruf.height = $(window).height()-45;
  	ctxHuruf = canvasHuruf.getContext("2d");

  	ctxHuruf.font = 'bold ' + fontSize + " " + fontType ;
  	ctxHuruf.fillStyle = 'rgb(0, 150, 136)';
  	ctxHuruf.textBaseline = 'middle';
  	if(dataRangkaianSatuSiswa.idAktivitas == 5){

  	}
  	drawletter(letter);

	//setup for drawing
	$("#canvas").drawTouch();
	$("#canvas").drawPointer();
	$("#canvas").drawMouse();
}

function drawletter(letter) {
  var minus = ctxHuruf.measureText(letter).width / 2;
  var centerx = ($(window).width()/ 2)-minus;
  var centery = ($(window).height()-45) / 2;
  ctxHuruf.fillText(letter, centerx, centery);
};



$.fn.drawTouch = function(){
	var start = function(e){
		e = e.originalEvent;
		ctx.beginPath();
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.moveTo(x,y);
	};
	var move= function(e){
		e.preventDefault();
		e = e.originalEvent;
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
	};
	var end=function(e){
		//pixelthreshold();
	}
	$(this).on("touchstart",start);
	$(this).on("touchmove",move);
	$(this).on("touchend",end);
};


$.fn.drawMouse = function() {
	var clicked = 0;
	var start = function(e) {
		clicked = 1;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		if(clicked){
			x = e.pageX;
			y = e.pageY;
			ctx.lineTo(x,y);
			ctx.stroke();
		}
	};
	var stop = function(e) {
		clicked = 0;
		//pixelthreshold();
	};
	$(this).on("mousedown", start);
	$(this).on("mousemove", move);
	$(this).on("mouseup", stop);
};

// prototype to	start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
$.fn.drawPointer = function() {
	var start = function(e) {
        e = e.originalEvent;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		e.preventDefault();
        e = e.originalEvent;
		x = e.pageX;
		y = e.pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
    };
	$(this).on("MSPointerDown", start);
	$(this).on("MSPointerMove", move);
};  

function nilai(id){
	if(id == 1){
		var score = "0";
	}
	else{
		var score = prompt("Masukkan Nilai", 0);
	}

	index++;
	if(index == kata.length){
		editResultAktivitas(idSiswaChoosen, idAktivitas, idRangkaian, path[n-1], score, dataRangkaianSatuSiswa, kata[index-1]);
  		window.location.href = "../halaman-mulai-aktivitas.html?id=" + idSiswaChoosen;
	}
	else{	
		editResultAktivitas(idSiswaChoosen, idAktivitas, idRangkaian, path[n-1], score, dataRangkaianSatuSiswa, kata[index-1]);
		settingUp();
	}  	
}

function FindAllPermutations(str, index, buffer) {
    if (typeof str == "string")
        str = str.split("");
    if (typeof index == "undefined")
        index = 0;
    if (typeof buffer == "undefined")
        buffer = [];
    if (index >= str.length)
        return buffer;
    for (var i = index; i < str.length; i++)
        buffer.push(ToggleLetters(str, index, i));
    return FindAllPermutations(str, index + 1, buffer);
}

function ToggleLetters(str, index1, index2) {
    if (index1 != index2) {
        var temp = str[index1];
        str[index1] = str[index2];
        str[index2] = temp;
    }
    return str.join("");
}

$(document).on('click', "#nilai", function () {
	nilai(0);
});

$(document).on('click', "#skip", function () {
	nilai(1);
});