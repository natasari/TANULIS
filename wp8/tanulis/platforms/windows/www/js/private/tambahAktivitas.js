var idSiswaChoosen ;
$(document).ready(function(){
	idSiswaChoosen = getUrlVars()["id"];
    listSampleAktivitas(idSiswaChoosen);
    dbAktivitas = SpahQL.db(tbAktivitas);

    dataSiswa = getDataSiswa(idSiswaChoosen);
    showNama(dataSiswa);
    //console.log(dbAktivitas);

});	

function showNama(data){
	$("#namaSiswa").html("Rangkaian Aktivitas : <strong>"+data.namaSiswa+"</strong>");
}

function listSampleAktivitas(siswa){
	url="aktivitas/";	
	$("#daftarSampleAktivitas").html("");
		$("#daftarSampleAktivitas").html(
			"<thead>"+
			"	<tr>"+
			"	<th>No</th>"+
			"	<th>Jenis</th>"+
			"	<th>Nama Aktivitas</th>"+
			"	<th>Keterangan</th>"+
			"	<th>Lihat Demo</th>"+
			"	<th>Tambah</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);
		for(var i in tbAktivitas.aktivitas){
			//var restoredSession = JSON.parse(localStorage.getItem('session'));
			var cli = tbAktivitas.aktivitas[i];
		  	$("#daftarSampleAktivitas tbody").append("<tr>"+
									 	 "	<td>"+cli.idAktivitas+" </td>" +
										 "	<td>"+cli.jenisAktivitas+" </td>" + 
										 "	<td>"+cli.namaAktivitas+" </td>" + 
										 "	<td>"+cli.keterangan+" </td>" + 
										 "	<td class='center-block'><button type='submit' onclick='redirectPage(\""+url+ "\",\""+cli.pathAktivitas+ "\")' class='btn btn-warning center-block'><span class='glyphicon glyphicon-play-circle' aria-hidden='true'></span> Demo</button></td>" + 
										 "	<td><button type='submit' onclick='tambahAktivitaskeRangkaianSiswa(\""+siswa+ "\",\""+cli.idAktivitas+ "\")' class='btn btn-success center-block teal'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span> Tambah</button></td>" +
		  								 "</tr>");
		}
}

function tambahAktivitaskeRangkaianSiswa(siswa,aktivitas){
	AddtoRangkaianAktivitas(siswa,aktivitas);
}

function panelHeading(){
	url = "halaman-kelola-aktivitas.html?id="
	redirectPage(url,idSiswaChoosen);
		// $("#panelHeadingTambahAktivitas").html(
		// "<button onclick='redirectPage(\""+url+ "\",\""+idSiswaChoosen+ "\")' class='btn '><span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span></button>" + 
        // "<strong style='font-size:16px;'> Daftar Sample Aktivitas </strong>");	
		
}
