// localStorage.removeItem("tbRangkaianAktivitas");
var tbRangkaianAktivitas = localStorage.getItem("tbRangkaianAktivitas");//Retrieve the stored data

if(jQuery.isEmptyObject(tbRangkaianAktivitas) == true){
	var tbRangkaianAktivitas = {
	  'rangkaian': [],
	  'state' :  true
	};
}else{
	tbRangkaianAktivitas = JSON.parse(tbRangkaianAktivitas);
	console.log(tbRangkaianAktivitas);
}

var dbRangkaianAktivitas = SpahQL.db(tbRangkaianAktivitas);

function AddtoRangkaianAktivitas(siswa, aktivitas){
	/*get data siswa*/
	var dataSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(siswa)+"]");

	/*get data aktivitas*/
	var dataAktivitas = dbAktivitas.select("/aktivitas/*[/idAktivitas =="+parseInt(aktivitas)+"]");

	/*hitung jumlah rangkaian pada anak dengan id = siswa*/
	var jumlahRangkaian = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(siswa)+"]");
	
	if(jQuery.isEmptyObject(jumlahRangkaian) == true){
		id_rangkaian = 1;
	}
	else{
	    idRangkaianTerakhir = tbRangkaianAktivitas.rangkaian[jumlahRangkaian.length-1].idRangkaian;
		id_rangkaian = idRangkaianTerakhir + 1; 
	}
	
	console.log(dataAktivitas);
	var aktivitaskeRangkaian = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : id_rangkaian, //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataAktivitas.value().namaAktivitas,
			paramAktivitas : dataAktivitas.value().paramAktivitas,
			paramResult : dataAktivitas.value().paramResult,
			jenisAktivitas : dataAktivitas.value().jenisAktivitas,
			isActive : "Belum dikonfigurasi",
			isPass : "Belum"
	}); 
	tbRangkaianAktivitas.rangkaian.push(aktivitaskeRangkaian);
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	alert("Aktivitas Berhasil ditambahkan");
}


var Result;
function editResultAktivitas(siswa, aktivitas, rangkaian, index, kemiripan, dataRangkaian, parameterNilai){
	// alert("siswa: " + siswa + "aktivitas: "+ aktivitas + "rangkaian: " + rangkaian + " index: " + index + "dataRangkaian: " + dataRangkaian);
	var data = dataRangkaian.value();
	res = data.paramResult;
	console.log(res);

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	if(dd<10) { dd='0'+dd } 
	if(mm<10) { mm='0'+mm } 
	today = mm+'/'+dd+'/'+yyyy;

	
	Result = ({
		        "parameterYangDinilai" : parameterNilai,
				"nilai" : kemiripan,
				"waktu" : "0",
				"tanggal" : today
			});
	
	/*ini digunakan untuk */
    console.log(res);
	if(jQuery.isEmptyObject(res) == false){
		var nHasil = res.length;
			/*replace dong berarti*/
			/*cek dulu ada ga yang parameternya sama */
			dataNilai = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa == "+parseInt(siswa)+"][/idRangkaian == "+parseInt(idRangkaian)+"][/paramResult/*[/parameterYangDinilai=='"+parameterNilai+"']]/paramResult/*");
			pathNilai = dataNilai.path();
			
			if(jQuery.isEmptyObject(pathNilai) == false){
				panjangPathNilai = pathNilai.length;
				indeksUbah = pathNilai[panjangPathNilai-1];
				// alert(indeksUbah);
				// alert(today);
				if(res[indeksUbah].parameterYangDinilai == parameterNilai && res[indeksUbah].tanggal == today){
					alert("ada yang sama");
					res.splice(indeksUbah, 1);
				}
			}		
	}
	res.push(Result);
	 tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
		idSiswa : parseInt(siswa),
		idRangkaian    : parseInt(rangkaian),
		idAktivitas  : parseInt(aktivitas),
		namaAktivitas : data.namaAktivitas,
		paramAktivitas : ({
			ukuranTulisan : data.paramAktivitas.ukuranTulisan,
			mulaiHuruf : data.paramAktivitas.mulaiHuruf,
			akhirHuruf : data.paramAktivitas.akhirHuruf,
			jenisTulisan : data.paramAktivitas.jenisTulisan,
			level : data.paramAktivitas.level
		}),
		paramResult : res,
		jenisAktivitas : data.jenisAktivitas,
		isActive : data.isActive,
		isPass : data.isPass
	});
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	
	
}

function deleteRangkaian(siswa, index){
	tbRangkaianAktivitas.rangkaian.splice(index, 1);
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	if(jQuery.isEmptyObject(tbRangkaianAktivitas.rangkaian) == true){
		localStorage.removeItem("tbRangkaianAktivitas");
		id_rangkaian = 1;
	}
	alert("Data berhasil dihapus");
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return true;
}

function editRangkaianAktivitasHurufBesar(siswa, aktivitas, rangkaian, index, dataRangkaian){
	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : parseInt(rangkaian), //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataRangkaian.namaAktivitas,
			paramAktivitas : ({
				ukuranTulisan : $("#ukuranTulisan").val(),
				mulaiHuruf : $("#mulaiHuruf").val(),
				akhirHuruf : $("#akhirHuruf").val(),
				jenisTulisan : $("#jenisTulisan").val(),
				level : $("#level").val()
			}),
			paramResult : dataRangkaian.paramResult,
			jenisAktivitas : dataRangkaian.jenisAktivitas,
			isActive : $("#isActive").val(),
			isPass : dataRangkaian.isPass
		});
		localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
		dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
		console.log(dataLengkap.value());
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return true;
		
}

function editRangkaianAktivitasSukuKata(siswa, aktivitas, rangkaian, index, dataRangkaian){
	var values = $("input[name='kata[]']").map(function(){return $(this).val();}).get();
	// console.log(values);

	tbRangkaianAktivitas.rangkaian[parseInt(index)] = ({
			idSiswa : parseInt(siswa),
			idRangkaian    : parseInt(rangkaian), //ini cari jumlah aktivitas yg ada di user
			idAktivitas  : parseInt(aktivitas),
			namaAktivitas : dataRangkaian.namaAktivitas,
			paramAktivitas : ({
				kata : values,
				jenisTulisan : $("#jenisTulisan").val(),
				level : $("#level").val()
			}),
			paramResult : dataRangkaian.paramResult,
			jenisAktivitas : dataRangkaian.jenisAktivitas,
			isActive : $("#isActive").val(),
			isPass : dataRangkaian.isPass
		});
	console.log(tbRangkaianAktivitas.rangkaian[parseInt(index)]);
	localStorage.setItem("tbRangkaianAktivitas", JSON.stringify(tbRangkaianAktivitas));
	dataLengkap = dbRangkaianAktivitas.select("/rangkaian/*[/idRangkaian =="+parseInt(rangkaian)+"][/idSiswa =="+parseInt(siswa)+"]");
	console.log(dataLengkap.value());
	window.location.href = "halaman-kelola-aktivitas.html?id="+siswa;
	return true;	
}