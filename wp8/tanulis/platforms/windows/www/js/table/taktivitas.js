	localStorage.removeItem("tbAktivitas");

	var tbAktivitas;
	tbAktivitas = localStorage.getItem("tbAktivitas");//Retrieve the stored data
	 //Converts string to object

	if(jQuery.isEmptyObject(tbAktivitas) == true){
		tbAktivitas = {
		  'aktivitas': [],
		  'state' :  true
		};
		buildTableAktivitas();
	}else{
		tbAktivitas = JSON.parse(tbAktivitas);
	}

function buildTableAktivitas(){
	var aktivitasHurufKecil = ({
			idAktivitas : 1,
			namaAktivitas    : "Huruf Kecil",
			pathAktivitas  : "huruf.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulaiHuruf : "a",
				akhirHuruf : "z",
				jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis huruf kecil dari a - z"
	}); 
	tbAktivitas.aktivitas.push(aktivitasHurufKecil);

	var aktivitasHurufBesar = ({
			idAktivitas : 2,
			namaAktivitas    : "Huruf Besar",
			pathAktivitas  : "huruf-besar.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulaiHuruf : "A",
				akhirHuruf : "Z",
				jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis huruf besar dari A - Z"
	});
	tbAktivitas.aktivitas.push(aktivitasHurufBesar); 


	var aktivitasAngka = ({
			idAktivitas : 3,
			namaAktivitas    : "Angka",
			pathAktivitas  : "angka.html",
			paramAktivitas : ({
				ukuranTulisan : "Sedang",
				mulaiHuruf : "0",
				akhirHuruf : "9",
				jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Matematika",
			keterangan : "Menulis angka 0 - 9"
	});
	tbAktivitas.aktivitas.push(aktivitasAngka); 

	var aktivitasSukuKata = ({
			idAktivitas : 4,
			namaAktivitas    : "Suku Kata",
			pathAktivitas  : "suku-kata.html",
			paramAktivitas : ({
				kata 	   : ["", "", "", "", ""],
				jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis suku-kata"
	});
	tbAktivitas.aktivitas.push(aktivitasSukuKata); 

	var aktivitasKata = ({
			idAktivitas : 5,
			namaAktivitas    : "Kata",
			pathAktivitas  : "kata.html",
			paramAktivitas : ({
				kata 	   : ["", "", "", "", ""],
				jenisTulisan : "Arial",
				level : "Mudah"
			}),
			paramResult : [],
			jenisAktivitas : "Bahasa",
			keterangan : "Menulis kata"
	});
	tbAktivitas.aktivitas.push(aktivitasKata); 

	localStorage.setItem("tbAktivitas", JSON.stringify(tbAktivitas));
	console.log(tbAktivitas);
}

function redirectPage(url,id) {
		if(id==null)id = "";
		window.location.href=url+id;
		return false;
}
