var letter = "";
var startLetter = "";
var endLetter = "";
var fontType = "";
var fontSize = null;
var level = "";
var startASCII = null;
var endASCII = null;
var arrayLetter = new Array();

var index = 0;
var idSiswaChoosen;
var idRangkaian;
var data;
var dataRangkaianSatuSiswa;
var hasilPresentasi;

var canvasGambarTulisan = document.createElement('canvas');
var contextGambarTulisan = canvasGambarTulisan.getContext('2d');

var canvasGambarHuruf = document.createElement('canvas');
var contextGambarHuruf = canvasGambarHuruf.getContext('2d');
var path, n;
/*ASCII*/
//var chr = String.fromCharCode(97 + n);
$(document).ready(function(){
	$(".footer").css({ top: $(window).height()-45});
	/*dapat aktivitas yang dikirimkan*/
	idSiswaChoosen = getUrlVars()["id"];
	idRangkaian = getUrlVars()["idRangkaian"];

	dataRangkaianSatuSiswa = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+parseInt(idSiswaChoosen)+"][/isActive == 'Aktif'][/idRangkaian =="+parseInt(idRangkaian)+"]");
	data = dataRangkaianSatuSiswa.value();
	path = dataRangkaianSatuSiswa.path();
	n = path.length;
	
	idAktivitas = data.idAktivitas;

	fontType = 'Calibri';

	if(data.paramAktivitas.ukuranTulisan ="Kecil"){
		fontSize = "225px";
	}
	else if(data.paramAktivitas.ukuranTulisan = "Sedang"){
		fontSize = "250px";
	}
	else if(data.paramAktivitas.ukuranTulisan = "Besar"){
		fontSize = "300px";
	}

	/*looping huruf*/
	startLetter = data.paramAktivitas.mulaiHuruf;
	endLetter 	= data.paramAktivitas.akhirHuruf;
	var mulai = startLetter.charCodeAt(0);
	var selesai = endLetter.charCodeAt(0);

	for(var i=mulai; i<=selesai; i++){
		letter = String.fromCharCode(i);
		arrayLetter.push(letter);
	}
	settingUp();
});

var ctx, ctxHuruf, color="rgb(0, 0, 5)";
var pixels = null;
var letterpixels = null;

function settingUp(){
	newCanvas(arrayLetter[index]);
	//setTimeout(function(){}, 1000);
	$(".palette").click(function(){
		$(".palette").css("border-color", "#777");
		$(".palette").css("border-style", "solid");
		$(this).css("border-color", "#fff");
		$(this).css("border-style", "dashed");
		color = $(this).css("background-color");
		ctx.beginPath();
		ctx.strokeStyle = color;
	});
}

function newCanvas(letter){
	//size of canvas	
	//setup canvas
	var canvas = document.getElementById("canvas");
	canvas.width = $(window).width();
	canvas.height = $(window).height()-45;
	ctx=canvas.getContext("2d");

	ctx.strokeStyle = color;
	ctx.lineWidth = 21;
	ctx.lineCap = 'round';

  	//setup ctxHuruf
  	var canvasHuruf = document.getElementById("canvasHuruf");
	canvasHuruf.width = $(window).width();
	canvasHuruf.height = $(window).height()-45;
  	ctxHuruf = canvasHuruf.getContext("2d");

  	ctxHuruf.font = 'bold ' + fontSize + " " + fontType ;
  	ctxHuruf.fillStyle = 'rgb(0, 150, 136)';
  	ctxHuruf.textBaseline = 'middle';
  	drawletter(letter);
  	

  	pixels = ctx.getImageData(0, 0, $(window).width(), $(window).height());
  	letterpixels = getpixelamount(0, 150, 136);

	//setup for drawing
	$("#canvas").drawTouch();
	$("#canvas").drawPointer();
	$("#canvas").drawMouse();
}

function drawletter(letter) {
  var centerx = ($(window).width()-ctx.measureText(letter).width) / 2;
  var centery = ($(window).height()-45) / 2;
  ctxHuruf.fillText(letter, centerx, centery);
};

function pixelthreshold() {
  if (getpixelamountTulis(0, 0, 5) / letterpixels > 0.6) {
  	if(index < arrayLetter.length){
		index++;
	  	compare(index);
  	}   
  }
};

var resembleControl;
function compare(index){	
	var img2 = cropping(ctxHuruf, canvasHuruf);
	var img1 = cropping(ctx, canvas);
	
	/*change dimension of image1*/
	var dimension1 = getDimension(img1);
	var dimension2 = getDimension(img2);
	console.log(dimension2.w * dimension2.h);

	var allHuruf, allTulisan;
	resize(img1, dimension2, function(hasil){
		var dimensionR = getDimension(hasil);

		drawCanvas(1, img2, dimensionR, function(result){
			
			drawCanvas(2, hasil, dimensionR, function(result){
				var pixelsTulisan = contextGambarTulisan.getImageData(0, 0, dimensionR.w, dimensionR.h);
				var pixelsHuruf = contextGambarHuruf.getImageData(0, 0, dimensionR.w, dimensionR.h);

				allTulisan = pixelsTulisan.data.length;
				allHuruf = pixelsHuruf.data.length;
				console.log(pixelsHuruf);
				console.log(pixelsTulisan);

				console.log(canvasGambarHuruf.toDataURL());
				console.log(canvasGambarTulisan.toDataURL());

				console.log("allTulisan "+ allTulisan);
				console.log("allHuruf "+ allHuruf);

				var amountSame = 0;
				var amountSameWhite = 0;
				for (i = 0; i < allTulisan; i += 4) {
					var a=0, b=0, c=0, d=0;
					if (pixelsTulisan.data[i] === 0 && pixelsTulisan.data[i + 1] === 150 && pixelsTulisan.data[i + 2] === 136){
					  a=1;
					}
					if (pixelsHuruf.data[i] === 0 && pixelsHuruf.data[i + 1] === 0 && pixelsHuruf.data[i + 2] === 5){
					  b=1;
					}
					if(a==1 && b==1){
						amountSame++;
					}

					if (pixelsTulisan.data[i+3] < 0.5){
					  c=1;
					}
					if (pixelsHuruf.data[i+3] < 0.5 ){
					  d=1;
					}
					if(c==1 && d==1){
						amountSameWhite++;
					}
				}
				
				alert("sama huruf " + amountSame);
				alert("letterpixels " + letterpixels);
				alert("HurufAja" + (amountSame/letterpixels*100));
				persen = (amountSame/letterpixels*100);
			    
    			editResultAktivitas(idSiswaChoosen, idAktivitas, idRangkaian, path[n-1], persen, dataRangkaianSatuSiswa);

			    if(index == arrayLetter.length){
			  		window.location.href = "../halaman-mulai-aktivitas.html?id=" + idSiswaChoosen;
				}
				else{
					alert("akan masuk rangkaian aktivitas selanjutnya");	
					settingUp();
				}
			});
		});
		
	});
	
}

function drawCanvas(nomorGambar, gambar1, dimensi, callback){

	var gmb1 = new Image();
	gmb1.src = gambar1;

	if(nomorGambar == 1){
		canvasGambarTulisan.width = dimensi.w;
	    canvasGambarTulisan.height = dimensi.h;
		gmb1.onload=function(){
		     var ima = contextGambarTulisan.drawImage(gmb1, 0, 0, gmb1.width, gmb1.height, 0, 0, dimensi.w, dimensi.h);
		     callback(contextGambarTulisan);
		}
	}
	else{
		canvasGambarHuruf.width = dimensi.w;
		canvasGambarHuruf.height = dimensi.h;
		gmb1.onload=function(){
		     var ima = contextGambarHuruf.drawImage(gmb1, 0, 0, gmb1.width, gmb1.height, 0, 0, dimensi.w, dimensi.h);
		     callback(contextGambarHuruf);
		}
	}
}

function getDimension(image){
	var img = new Image();
	img.src = image;

	var imgSize = {
      w: img.width,
      h: img.height
	};
	return imgSize;
}

function cropping(ctx, canvas){
	var w = canvas.width,
	h = canvas.height,
	pix = {x:[], y:[]},
	imageData = ctx.getImageData(0,0,canvas.width,canvas.height),
	x, y, indexx;
	for (y = 0; y < h; y++) {
	    for (x = 0; x < w; x++) {
	        indexx= (y * w + x) * 4;
	        if (imageData.data[indexx+3] > 0) {

	            pix.x.push(x);
	            pix.y.push(y);

	        }   
	    }
	}
	pix.x.sort(function(a,b){return a-b});
	pix.y.sort(function(a,b){return a-b});
	var n = pix.x.length-1;
	
	w = pix.x[n] - pix.x[0];
	h = pix.y[n] - pix.y[0];

	var cut = ctx.getImageData(pix.x[0], pix.y[0], w, h);

	canvas.width = w;
	canvas.height = h;
	ctx.putImageData(cut, 0, 0);

	var image = canvas.toDataURL();
	//var win=window.open(image, '_blank');
	//win.focus();
	return image;
}

function onComplete(data){
	var time = Date.now();
	var diffImage = new Image();
	diffImage.src = data.getImageDataUrl();

	//$('#image-diff').html(diffImage);

	$(diffImage).click(function(){
	  window.open(diffImage.src, '_blank');
	});

	if(data.misMatchPercentage == 0){
	  $('#thesame').show();
	  $('#diff-results').hide();
	} 
	else {
	  $('#mismatch').text(data.misMatchPercentage);
	  if(!data.isSameDimensions)
	  {
	    $('#differentdimensions').show();
	  } 
	  else {
	    $('#differentdimensions').hide();
	  }
	  hasilPresentasi = data.misMatchPercentage;
	  $('#diff-results').show();
	  $('#thesame').hide();
	}
}
 

function resize(image, dimension, callback){
	var canvasResizing = document.createElement('canvas');
  	var contextResizing = canvasResizing.getContext('2d');

    var width = dimension.w;
    var height = dimension.h;
    canvasResizing.width = width;
    canvasResizing.height = height;

	var img=new Image();
	img.src=image;
	img.onload=function(){
	     var ima = contextResizing.drawImage(img, 0, 0, img.width, img.height, 0, 0, width, height);
	     callback(canvasResizing.toDataURL());
	}
}

function getpixelcolour(x, y) {
  var index = ((y * (pixels.width * 4)) + (x * 4));
  return {
    r: pixels.data[index],
    g: pixels.data[index + 1],
    b: pixels.data[index + 2],
    a: pixels.data[index + 3]
  };
}

function getpixelamount(r, g, b) {
  var pixels = ctxHuruf.getImageData(0, 0, $(window).width(), $(window).height());
  var all = pixels.data.length;
  var amount = 0;
  for (i = 0; i < all; i += 4) {
    if (pixels.data[i] === r &&
        pixels.data[i + 1] === g &&
        pixels.data[i + 2] === b) {
      amount++;
    }
  }
  return amount;
};

function getpixelamountTulis(r, g, b) {
  var pixels = ctx.getImageData(0, 0, $(window).width(), $(window).height());
  var all = pixels.data.length; //106579 248684
  var amount = 0;
  for (i = 0; i < all; i += 4) {
  	///console.log(pixels.data[i] +" " + pixels.data[i + 1] +" "+ pixels.data[i + 2]);
    if (pixels.data[i] === r && pixels.data[i + 1] === g && pixels.data[i + 2] === b) {
      amount++;
    }
  }
  return amount;
};

$.fn.drawTouch = function(){
	var start = function(e){
		e = e.originalEvent;
		ctx.beginPath();
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.moveTo(x,y);
	};
	var move= function(e){
		e.preventDefault();
		e = e.originalEvent;
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
	};
	var end=function(e){
		//pixelthreshold();
	}
	$(this).on("touchstart",start);
	$(this).on("touchmove",move);
	$(this).on("touchend",end);
};


$.fn.drawMouse = function() {
	var clicked = 0;
	var start = function(e) {
		clicked = 1;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		if(clicked){
			x = e.pageX;
			y = e.pageY;
			ctx.lineTo(x,y);
			ctx.stroke();
		}
	};
	var stop = function(e) {
		clicked = 0;
		//pixelthreshold();
	};
	$(this).on("mousedown", start);
	$(this).on("mousemove", move);
	$(this).on("mouseup", stop);
};

// prototype to	start drawing on pointer(microsoft ie) using canvas moveTo and lineTo
$.fn.drawPointer = function() {
	var start = function(e) {
        e = e.originalEvent;
		ctx.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx.moveTo(x,y);
	};
	var move = function(e) {
		e.preventDefault();
        e = e.originalEvent;
		x = e.pageX;
		y = e.pageY;
		ctx.lineTo(x,y);
		ctx.stroke();
    };
	$(this).on("MSPointerDown", start);
	$(this).on("MSPointerMove", move);
};  

$(document).on('click', "#hasil", function () {
	pixelthreshold();
});