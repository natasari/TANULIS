$(document).ready(function(){
	var idSiswaChoosen = getUrlVars()["id"];
	showTombolPlay(idSiswaChoosen);

	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	if(dd<10) { dd='0'+dd } 
	if(mm<10) { mm='0'+mm } 
	today = mm+'/'+dd+'/'+yyyy;
	/*dapetin hasil rangkaian khusus tanggal hari ini*/
	hasilData = dbRangkaianAktivitas.select("/rangkaian/*[/idSiswa =="+ parseInt(idSiswaChoosen) + "][/isActive == 'Aktif']*[/paramResult/*/tanggal == '"+today+"']");
	hasil = hasilData.values();
    console.log(hasil);
	$("#hasil").html("");
		$("#hasil").html(
			"<thead>"+
			"	<tr>"+
			"	<th>No</th>"+
			"	<th>Nama Aktivitas</th>"+
			"	<th>Hasil</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);

		for(i=0; i<hasilData.length; i++){
			n = hasil[i].paramResult.length;
			a = hasil[i].paramResult;
		  	$("#hasil tbody").append("<tr>"+
		  		 "	<td>"+(i+1)+" </td>" +
			 	 "	<td>"+hasil[i].namaAktivitas+" </td>" +
			 	 "  <td>"+looping(a, today)+
				 "  </td>" + 
					 "</tr>");
		}
		/*ambil nama dengan id siswa x*/
		var namaSiswa = dbSiswa.select("/siswa/*[/idSiswa =="+parseInt(idSiswaChoosen)+"]/namaSiswa");
		$("#namaSiswa").html("Hasil Aktivitas : <strong>"+namaSiswa+"</strong>");

});

function looping(a, today){
	console.log(a.length);
	var allHasil="";
	var ind = 0;
	for(j=0; j<a.length; j++){
		if(a[j].tanggal == today){
			if(ind == 0 ){
				allHasil = allHasil + a[j].tanggal + "<br>";
				ind = 1;
			}
			z= a[j];
			for (var property in z){
				if(property == 'parameterYangDinilai'){
					allHasil = allHasil + ("<b>Soal" +": " + z[property] + "</b> " );	
				}
				else if(property != 'tanggal'){
					allHasil = allHasil + (property +": " + z[property] + " " );
				}
			}
			allHasil = allHasil + "<br>";
		}
	}
	return allHasil;
}

function showTombolPlay(siswa){
	url = "halaman-mulai-aktivitas.html?id=";
	$("#buttonPlay").html("<button onclick='redirectPage(\""+url+ "\",\""+siswa+ "\")' type='button' class='btn btn-success' style='float:left;margin-left:5px;'>" +
                               "<span class='glyphicon glyphicon-triangle-right' aria-hidden='true'></span>Ulangi Aktivitas</a>");
}